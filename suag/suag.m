function varargout = suag(varargin)
% SUAG MATLAB code for suag.fig
%      SUAG, by itself, creates a new SUAG or raises the existing
%      singleton*.
%
%      H = SUAG returns the handle to a new SUAG or the handle to
%      the existing singleton*.
%
%      SUAG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SUAG.M with the given input arguments.
%
%      SUAG('Property','Value',...) creates a new SUAG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before suag_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to suag_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help suag

% Last Modified by GUIDE v2.5 10-Jul-2018 14:30:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @suag_OpeningFcn, ...
    'gui_OutputFcn',  @suag_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before suag is made visible.
function suag_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to suag (see VARARGIN)

% Choose default command line output for suag
handles.output = hObject;

% draw the grid on the axes
N = 5;%11;
M = 5;%11;
T = 0.01;
arraydims = [N M];
x = [repmat([0 M NaN]', [N+1 1]);reshape([(0:M)' (0:M)' NaN(M+1, 1)]', [], 1)] + .5;
y = [reshape([(0:N)' (0:N)' NaN(N+1, 1)]', [], 1);repmat([0 N NaN]', [M+1 1])] + .5;
imagesc(handles.axes_array, zeros(N, N));  % colorbar(handles.axes_array);
caxis(handles.axes_array, [0 3]);
hold(handles.axes_array, 'on');
plot(handles.axes_array, x, y, '-k');
hold(handles.axes_array, 'off');
xlim(handles.axes_array, [0 M]+.5);
ylim(handles.axes_array, [0 N]+.5);

stimmode = get(get(handles.uibuttongroup_simmode, 'SelectedObject'), 'String');
useslider = get(handles.checkbox_gain, 'Value');
sliderval = get(handles.slider_gain, 'Value');
arrayinds = 1:32;
arrayinds_ = mod(arrayinds - 1, prod(arraydims)) + 1;
% could use setappdata and getappdata rather than handles
% but buffer and timer are objects so I'll leave it this way
% handles.buffer = circVBuf(int64(102400), int64(length(arrayinds)), 0);

% TODO we will try using the UDP buffer rather than implementing our own buffer
handles.buffer = CircBuf(1024000, length(arrayinds)); % Don't make shorter thatn sr * T
handles.waitbarpatch = patch(handles.axes_waitbar, 'XData', [0 0 0 0], 'YData', [1 0 0 1], 'FaceColor', [1 0 0]);
handles.waitbartext = text(handles.axes_waitbar, 0.5, 0.5, '0.0%', 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center');

%Initialize UDP port to write SUAG data
% remoteport = 11002;
% localport = 11003;
% handles.udp = udp('localhost', 'RemotePort', remoteport, 'LocalPort', localport, ...
%         'InputBufferSize', 128*102400*8 + 128, 'OutputBufferSize', 128*102400*8 + 128, 'EnablePortSharing', 'on', ...
%         'ReadAsyncMode', 'continuous', ...
%         'BytesAvailableFcnCount', 5, 'BytesAvailableFcnMode', 'byte', ...  % 5
%         'BytesAvailableFcn', @(hObject, event) data_request(hObject, event, handles.buffer));  % data_request(hObject, eventdata, handles.buffer)
% fopen(handles.udp);

% handles.tcpipServer = tcpip('127.0.0.1',55000,'NetworkRole','Server', 'ReadAsyncMode', 'continuous',...
%      'BytesAvailableFcnCount', 5, 'BytesAvailableFcnMode', 'byte', ... 
%      'BytesAvailableFcn', @(hObject, event) data_request(hObject, event, handles.buffer)); 
% set(handles.tcpipServer,'OutputBufferSize',128*102400*8);
% pause(.1)
% % opening TCP port takes several minutes?!
% fopen(handles.tcpipServer);
% pause(.1);
% if ~strcmp(handles.tcpipServer.Status, 'open')
%     fprintf('could not open TCP object\n');
%     return
% end

%call TCP/UDP port
useTcpIpServer = true;
if useTcpIpServer
    suag('open_tcpip_server', hObject, eventdata, handles);
    % open_tcpip_server(hObject, eventdata, handles);
    handles = guidata(hObject);  % won't we lose handles.output if we do this here?
else
    handles.tcpipServer = struct('BytesAvailable', 0, 'Status', 'closed');
end
    
% handles.bufptr = 1;
handles.spkbuf = ObjectArray(cell(length(arrayinds), 7));  % I want this to be a handle
handles.mockarray = MockArray(15e3, T, stimmode, 'data/data.mat', useslider, sliderval, 1.0, arraydims, arrayinds_);
% setappdata(h, '', true);

% create callback functions when figure is clicked
set(handles.figure_suag, 'WindowButtonDownFcn', {@wbd});
set(handles.figure_suag, 'WindowButtonMotionFcn', {@wbm});
set(handles.figure_suag, 'WindowButtonUpFcn', {@wbu});
set(handles.figure_suag, 'DeleteFcn', {@wclose});

% update slider continuously
slidercallback = @(hObject, eventdata) suag('slider_gain_Callback', handles.slider_gain, eventdata, handles);
lh = addlistener(handles.slider_gain, 'Value', 'PreSet', slidercallback);
handles.slider_gain.DeleteFcn = @(~, ~) delete(lh);

% handles passed to bufferdata must is not updated by guidata
handles.timer = timer('BusyMode', 'drop', 'ExecutionMode', 'fixedRate', ...
    'Period', T, 'TimerFcn', {@bufferdata, handles});
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes suag wait for user response (see UIRESUME)
% uiwait(handles.figure_suag);

function bufferdata(hObject, event, handles)
if getappdata(handles.figure_suag, 'down')
    disp('');
end

try
    handles.mockarray.updatearray();  % items might get removed from the list
    [newdata, newspikes] = handles.mockarray.generatedata();
    % TODO create a buffer for new spikes (8785 max per channel might be set in config)
    % handles.buffer.append(newdata);
    
    % TODO might need to send the dimensions of the data first as two
    % uint16s
%     fwrite(handles.udp, typecast(newdata(:), 'uint8'));
%     fprintf('write new data\n')
    handles.buffer.write(newdata);
    pctfull = handles.buffer.available / handles.buffer.numsamples;
    set(handles.waitbarpatch, 'XData', [0 0 pctfull pctfull]);
    set(handles.waitbartext, 'String', sprintf('%2.1f', pctfull*100));
    
%     fprintf('check if data request\n')
%     currBytes = handles.tcpipServer.BytesAvailable;
    if handles.tcpipServer.BytesAvailable >= 5
%         disp(num2str(handles.tcpipServer.BytesAvailable))
%         fprintf('sending data\n')
        timeStamp = typecast(now(), 'single');
        handles = data_request(hObject, [], handles, handles.buffer, timeStamp);
    end
    
    % if get(handles.checkbox_record, 'Value')
    %     t = handles.mockarray.curtime + (0:size(newdata, 1) - 1) / handles.mockarray.sr;
    %     handles.mockarray.record_data([t' newdata]);
    % end
%     fwrite(handles.udp, typecast(newspikes(:), 'uint8'));
    
    for iuint = 1:size(newspikes, 2)
        for ichan = 1:size(newspikes, 1)
            handles.spkbuf.Value{ichan, iuint} = [handles.spkbuf.Value{ichan, iuint} newspikes{ichan, iuint}];
        end
    end
catch ex
    report = getReport(ex)
end

set(handles.text_curtime, 'String', sprintf('t = %f, available = %d', ...
    handles.mockarray.curtime, handles.buffer.available));
% validate the listbox entries
curseq = arrayfun(@(x) x.itemlabel, handles.mockarray.sequence, 'UniformOutput', false);
set(handles.listbox_sequence, 'String', curseq);

% if mod(handles.mockarray.curtime, .1) < 1e-4 || mod(handles.mockarray.curtime, .1) > 1 - 1e-4
try
    % update array at a potentially slower interval
    switch handles.mockarray.mode
        case 'Manual'
            him = findobj(handles.axes_array, 'Type', 'Image');
            temp = reshape(handles.mockarray.changains, handles.mockarray.arraydims(1), handles.mockarray.arraydims(2));
            temp(setdiff(1:prod(handles.mockarray.arraydims), handles.mockarray.arrayinds)) = 0;
            set(him, 'CData', temp);
        case 'Replay'
            % pass
    end
catch ex
    report = getReport(ex)
end

% drawnow;
% end


function handles = data_request(hObject, eventdata, handles, buffer, timeStamp)
%callback for data request, now packing and sending data
a = tic;
tcpipServer = handles.tcpipServer;
temp = fread(tcpipServer, [1 5], 'uint8');  % Start and Stop messages shouldn't be forced to be 5 bytes



% Is the first byte the type of message
% assert(isequal(temp(1), uint8(1)));
msgtype = temp(1);
SuagMessage.DATA_REQUEST = 1;
SuagMessage.START_PLAYBACK = 2;
SuagMessage.STOP_PLAYBACK = 3;

switch msgtype
    case SuagMessage.DATA_REQUEST
        numbytes = typecast(uint8(temp(2:5)), 'uint32');
        if numbytes == 0
            data = buffer.read();
        else
            data = buffer.read(numbytes);
        end

        [n, m] = size(data);
        dataCol = [single([n; m; reshape(data, [], 1)]); timeStamp'];
        % fprintf('In Callback...')
        fwrite(tcpipServer,dataCol,'single');

       
%         fprintf('...Data sent!\n')
    case SuagMessage.START_PLAYBACK
        handles.mockarray.curtime = 0;
        buffer.clear();
%         handles.mockarray.bufferStartTime = datevec(now,'mmmm dd, yyyy HH:MM:SS');
        
%          suag('pushbutton_start_Callback', handles.pushbutton_start, eventdata, handles);
    case SuagMessage.STOP_PLAYBACK
%         suag('pushbutton_stop_Callback', handles.pushbutton_stop, eventdata, handles);
end
 b = toc(a);

 
% fprintf('...Data sent! callback time: %f \n', b)
% figure;imagesc(data);
% fprintf('%f %f %f %f %f', dataCol(1),dataCol(2),dataCol(3),dataCol(4),dataCol(5))


% data = buffer.read();
% sz = size(data);
% fprintf('In Callback, data size: %d %d \n', sz);
% datapacket = [uint8(sz) typecast(reshape(data, 1, []), 'uint8')];
% disp(size(datapacket))
% fprintf('Write datapacket\n');
% fwrite(hObject, datapacket)

% GET_DATA = uint8(1);
% CHANGE_PARAMS = uint8(2);  % TBD
% 
% msg = fread(hObject, 1, 'uint8');
% switch msg
%     case GET_DATA        
%         numptstosend = fread(hObject, 1, 'uint32');
%         if numptstosend == 0
%             data = single(buffer.read());
%         else
%             data = single(buffer.read(numptstosend));
%         end
%         fprintf('SENDING DATA');
%         fwrite(hObject, [uint16(size(data)) typecast(single(data), 'uint16')], 'uint16');
%     case CHANGE_PARAMS
%         % pass




function wbd(h, evd)
% window button down function callback
% set the down state to true
setappdata(h, 'down', true);
handles = guidata(h);

onaxes = isonaxes(handles);

stimmode = get(get(handles.uibuttongroup_simmode, 'SelectedObject'), 'String');
if strcmp(stimmode, 'Manual') && onaxes
    tryadditem(handles, 'sequence', true);
end


function tryadditem(handles, dest, clicked)
% add item from the GUI settings
ax = handles.axes_array;
running = handles.timer.Running;

if strcmp(running, 'on')
    stop(handles.timer);
end

% get the first selection coordinate
if clicked
    cp = get(ax, 'CurrentPoint');
    x = cp(1, 1);
    y = cp(1, 2);
else
    x = str2double(get(handles.edit_x, 'String'));
    y = str2double(get(handles.edit_y, 'String'));
end

mfr = get(handles.checkbox_mfr, 'Value');
wpm = get(handles.checkbox_wpm, 'Value');
lambda = str2double(get(handles.edit_mfr, 'String'));
k = str2double(get(handles.edit_gain, 'String'));
freq1 = str2double(get(handles.edit_freq1, 'String'));
freq2 = str2double(get(handles.edit_freq2, 'String'));
sigma = str2double(get(handles.edit_sig, 'String'));
tau = str2double(get(handles.edit_tau, 'String'));

stimmode = get(get(handles.uibuttongroup_simmode, 'SelectedObject'), 'String');
if strcmp(stimmode, 'Manual')
    t0 = handles.mockarray.curtime;
elseif strcmp(stimmode, 'Replay')
    dt = str2double(get(handles.edit_dt, 'String'));
    if isempty(handles.mockarray.sequence)
        t0 = dt;
    else
        t0 = handles.mockarray.sequence(end).t0 + dt;
    end
end

label = get(handles.edit_settinglabel, 'String'); 
ramptime = str2double(get(handles.edit_ramptime, 'String'));
holdtime = str2double(get(handles.edit_holdtime, 'String'));

item = SimSettings(mfr, wpm, lambda, k, freq1, freq2, x, y, sigma, tau, t0, label, ramptime, holdtime);
if handles.mockarray.additem(item, dest)
    switch dest
        case 'sequence'
            listboxstrings = [get(handles.listbox_sequence, 'String'); ...
                handles.mockarray.sequence(end).itemlabel()];
            set(handles.listbox_sequence, 'String', listboxstrings);
        case 'stored'
            listboxstrings = [get(handles.popupmenu_settings, 'String'); ...
                handles.mockarray.stored(end).itemlabel()];
            set(handles.popupmenu_settings, 'String', listboxstrings);
            
            % Increment settings label value (not necessary unless labels have to be unique)
            % labelstring = get(handles.edit_settinglabel, 'String');
            % newlabel = ['Setting' num2str(str2double(labelstring(8:end)) + 1)];
            % set(handles.edit_settinglabel, 'String', newlabel);
    end
end

if strcmp(running, 'on')
    start(handles.timer);
end


function wbm(h, evd)
if getappdata(h, 'down')
    handles = guidata(h);
    
    onaxes = isonaxes(handles);
    
    stimmode = get(get(handles.uibuttongroup_simmode, 'SelectedObject'), 'String');
    if strcmp(stimmode, 'Manual') && onaxes
        tryadditem(handles, 'sequence', true);
    end
end


function onaxes = isonaxes(handles)
xl = xlim(handles.axes_array);
yl = ylim(handles.axes_array);
cp = get(handles.axes_array, 'CurrentPoint');
x = cp(1, 1);
y = cp(1, 2);
onaxes = x >= xl(1) && x <= xl(2) && y >= yl(1) && y <= yl(2);


function wbu(h, evd)
% window button up function callback
% set the down state to false
setappdata(h, 'down', false);


function wclose(h, evd)
% disable callbacks prior to deleting the figure
set(h, 'WindowButtonDownFcn', '');
set(h, 'WindowButtonMotionFcn', '');
set(h, 'WindowButtonUpFcn', '');
handles = guidata(h);
stop(handles.timer);
delete(handles.timer);
delete(handles.mockarray);
delete(handles.buffer);
close(h);


% --- Outputs from this function are returned to the command line.
function varargout = suag_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton_replay.
function pushbutton_replay_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_replay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton_manual.
function pushbutton_manual_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_manual (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton_start.
function pushbutton_start_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% if (~isfield(handles, 'tcpipServer') || ~isfield(handles, 'udp'))
%     error('Connect UDP or TCP/IP port first')
% end

if strcmp(handles.timer.Running, 'off')
    start(handles.timer)
    set(hObject, 'Enable', 'off');
    set(handles.pushbutton_stop, 'Enable', 'on');
end

% --- Executes on button press in checkbox_mfr.
function checkbox_mfr_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_mfr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_mfr


% --- Executes on button press in checkbox_wpm.
function checkbox_wpm_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_wpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_wpm



function edit_mfr_Callback(hObject, eventdata, handles)
% hObject    handle to edit_mfr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_mfr as text
%        str2double(get(hObject,'String')) returns contents of edit_mfr as a double


% --- Executes during object creation, after setting all properties.
function edit_mfr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_mfr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_settings.
function popupmenu_settings_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_settings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_settings contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_settings
% TODO populate settings the same way as from the listbox
% also allow items to be removed from both

% --- Executes during object creation, after setting all properties.
function popupmenu_settings_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_settings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_addsetting.
function pushbutton_addsetting_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_addsetting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tryadditem(handles, 'stored', false);

% --------------------------------------------------------------------
function mnu_file_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function mnu_help_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function mnu_about_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function mnu_new_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_new (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.popupmenu_settings, 'String', '');

% --------------------------------------------------------------------
function mnu_opensettings_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_opensettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filterspec = {'*.txt', 'Text File (*.txt)'; '*.*', 'All Files (*.*)'};
dialogtitle = 'Open Settings File';
defaultname = 'simsettings.txt';
[fn, pn] = uigetfile(filterspec, dialogtitle, defaultname);
filename = [pn fn];

% load and parse the file to construct SimSettings objects
fid = fopen(filename, 'r');
cleanupobj = onCleanup(@() fclose(fid));
while ~feof(fid)
    myline = fgetl(fid);
    if isempty(myline)
        break
    end
    namevalpairs = strsplit(myline, ', ');
    nvp2 = cellfun(@(x) strsplit(x, '='), namevalpairs, 'UniformOutput', false);
    clear mfr wpm lambda k freq1 freq2 x y sig tau t0 label
    for isetting = 1:length(nvp2)
        switch nvp2{isetting}{1}
            case 'mfr'
                mfr = nvp2{isetting}{2};
            case 'wpm'
                wpm = nvp2{isetting}{2};
            case 'lambda'
                lambda = nvp2{isetting}{2};
            case 'k'
                k = nvp2{isetting}{2};
            case 'freq1'
                freq1 = nvp2{isetting}{2};
            case 'freq2'
                freq2 = nvp2{isetting}{2};
            case 'x'
                x = nvp2{isetting}{2};
            case 'y'
                y = nvp2{isetting}{2};
            case 'sigma'
                sig = nvp2{isetting}{2};
            case 'tau'
                tau = nvp2{isetting}{2};
            case 't0'
                t0 = nvp2{isetting}{2};
            case 'label'
                label = nvp2{isetting}{2};
            case 'ramptime'
                ramptime = nvp2{isetting}{2};
            case 'holdtime'
                holdtime = nvp2{isetting}{2};
            otherwise
                warning('unrecognized setting %s\n', nvp2{isetting}{1});
        end
    end
    myvars = {'mfr' 'wpm' 'lambda' 'k' 'freq1' 'freq2' 'x' 'y' 'sig' 'tau' 't0' 'label'};
    if all(cellfun(@(x) exist(x, 'var'), myvars))    
        item = SimSettings(mfr, wpm, lambda, k, freq1, freq2, x, y, sig, tau, t0, label, ramptime, holdtime);
    end
    
    % add the item to popupmenu_settings
    if handles.mockarray.additem(item, 'stored')
        comboboxstrings = [get(handles.popupmenu_settings, 'String'); ...
            handles.mockarray.stored(end).itemlabel()];
        set(handles.popupmenu_settings, 'String', comboboxstrings);
    end
end
delete(cleanupobj);


% --------------------------------------------------------------------
function mnu_savesettings_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_savesettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.mockarray.write_settings_file(handles.mockarray.settingsfile);

% --------------------------------------------------------------------
function mnu_savesettingsas_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_savesettingsas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.mockarray.write_settings_file();


function edit_gain_Callback(hObject, eventdata, handles)
% hObject    handle to edit_gain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_gain as text
%        str2double(get(hObject,'String')) returns contents of edit_gain as a double


% --- Executes during object creation, after setting all properties.
function edit_gain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_gain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_dur_Callback(hObject, eventdata, handles)
% hObject    handle to edit_dur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_dur as text
%        str2double(get(hObject,'String')) returns contents of edit_dur as a double


% --- Executes during object creation, after setting all properties.
function edit_dur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_freq1_Callback(hObject, eventdata, handles)
% hObject    handle to edit_freq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_freq1 as text
%        str2double(get(hObject,'String')) returns contents of edit_freq1 as a double


% --- Executes during object creation, after setting all properties.
function edit_freq1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_freq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_freq2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_freq2 as text
%        str2double(get(hObject,'String')) returns contents of edit_freq2 as a double


% --- Executes during object creation, after setting all properties.
function edit_freq2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_stop.
function pushbutton_stop_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(handles.timer.Running, 'on')
    stop(handles.timer);
    set(hObject, 'Enable', 'off');
    set(handles.pushbutton_start, 'Enable', 'on');
end



function edit_x_Callback(hObject, eventdata, handles)
% hObject    handle to edit_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_x as text
%        str2double(get(hObject,'String')) returns contents of edit_x as a double


% --- Executes during object creation, after setting all properties.
function edit_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_y_Callback(hObject, eventdata, handles)
% hObject    handle to edit_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_y as text
%        str2double(get(hObject,'String')) returns contents of edit_y as a double


% --- Executes during object creation, after setting all properties.
function edit_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_sig_Callback(hObject, eventdata, handles)
% hObject    handle to edit_sig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_sig as text
%        str2double(get(hObject,'String')) returns contents of edit_sig as a double


% --- Executes during object creation, after setting all properties.
function edit_sig_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_sig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_tau_Callback(hObject, eventdata, handles)
% hObject    handle to edit_tau (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_tau as text
%        str2double(get(hObject,'String')) returns contents of edit_tau as a double


% --- Executes during object creation, after setting all properties.
function edit_tau_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_tau (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox_sequence.
function listbox_sequence_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_sequence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_sequence contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_sequence

ind = get(hObject, 'Value');
item = handles.mockarray.sequence(ind);
dts = diff([0 arrayfun(@(x) x.t0, handles.mockarray.sequence)]);

set(handles.checkbox_mfr, 'Value', item.mfr);
set(handles.checkbox_wpm, 'Value', item.wpm);
set(handles.edit_mfr, 'String', item.lambda);
set(handles.edit_gain, 'String', item.k);
set(handles.edit_freq1, 'String', item.freq1);
set(handles.edit_freq2, 'String', item.freq2);
set(handles.edit_x, 'String', item.x);
set(handles.edit_y, 'String', item.y);
set(handles.edit_sig, 'String', item.sigma);
set(handles.edit_tau, 'String', item.tau);
set(handles.edit_dt, 'String', dts(ind));

set(handles.edit_settinglabel, 'String', item.label);

% --- Executes during object creation, after setting all properties.
function listbox_sequence_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_sequence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_addtolist.
function pushbutton_addtolist_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_addtolist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tryadditem(handles, 'sequence', false);

% --- Executes on button press in pushbutton_getfromlist.
function pushbutton_getfromlist_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_getfromlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_settinglabel_Callback(hObject, eventdata, handles)
% hObject    handle to edit_settinglabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_settinglabel as text
%        str2double(get(hObject,'String')) returns contents of edit_settinglabel as a double


% --- Executes during object creation, after setting all properties.
function edit_settinglabel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_settinglabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_dt_Callback(hObject, eventdata, handles)
% hObject    handle to edit_dt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_dt as text
%        str2double(get(hObject,'String')) returns contents of edit_dt as a double


% --- Executes during object creation, after setting all properties.
function edit_dt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_record.
function checkbox_record_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_record (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_record


% --------------------------------------------------------------------
function mnu_saverecording_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_saverecording (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.mockarray.set_record_file();


% --- Executes on slider movement.
function slider_gain_Callback(hObject, eventdata, handles)
% hObject    handle to slider_gain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
val = get(hObject, 'Value');
handles.mockarray.sliderval = val;

% --- Executes during object creation, after setting all properties.
function slider_gain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_gain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in checkbox_gain.
function checkbox_gain_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_gain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_gain
stimmode = get(get(handles.uibuttongroup_simmode, 'SelectedObject'), 'String');
handles.mockarray.useslider = get(handles.checkbox_gain, 'Value') && strcmp(stimmode, 'Manual');

% --- Executes when selected object is changed in uibuttongroup_simmode.
function uibuttongroup_simmode_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uibuttongroup_simmode
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stimmode = get(get(handles.uibuttongroup_simmode, 'SelectedObject'), 'String');
handles.mockarray.mode = stimmode;
if strcmp(stimmode, 'Manual')
    set(handles.checkbox_gain, 'Enable', 'on');
else
    set(handles.checkbox_gain, 'Enable', 'off');
end
handles.mockarray.useslider = get(handles.checkbox_gain, 'Value') && strcmp(stimmode, 'Manual');

% --------------------------------------------------------------------
function mnu_edit_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function mnu_slider_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
minval = get(handles.slider_gain, 'Min');
maxval = get(handles.slider_gain, 'Max');
answer = inputdlg({'Slider Min:', 'Slider Max:'}, 'Slider Config', [1 10], {num2str(minval), num2str(maxval)});
if isempty(answer)
    % cancelled
    return
end
minval = str2double(answer{1});
maxval = str2double(answer{2});
if maxval <= minval
    % invalid range
    fprintf('Invalid range entered\n');
    return
end
set(handles.slider_gain, 'Max', maxval);
set(handles.slider_gain, 'Min', minval);

% --------------------------------------------------------------------
function mnu_exit_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function edit_ramptime_Callback(hObject, eventdata, handles)
% hObject    handle to edit_ramptime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_ramptime as text
%        str2double(get(hObject,'String')) returns contents of edit_ramptime as a double


% --- Executes during object creation, after setting all properties.
function edit_ramptime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_ramptime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton11.
function open_tcpip_server(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% set(hObject, 'Enable', 'off');

handles.tcpipServer = tcpip('127.0.0.1',4000,'NetworkRole','Server', 'ReadAsyncMode', 'continuous');
%      'BytesAvailableFcnCount', 5, 'BytesAvailableFcnMode', 'byte');
%      'BytesAvailableFcn', @(hObject, eventdata) data_request(hObject, eventdata, handles.buffer)); 
% set(handles.tcpipServer, 'BytesAvailableFcn', {'data_request', handles.buffer});
set(handles.tcpipServer,'OutputBufferSize',65536008+100);% 128*1000*4+10
set(handles.tcpipServer, 'Terminator', '')

pause(.05)
fopen(handles.tcpipServer);
if ~strcmp(handles.tcpipServer.Status, 'open')
    fprintf('could not open TCP object\n');
    return
elseif strcmp(handles.tcpipServer.Status, 'open')
    fprintf('TCP port opened\n')
else
    fprintf('port not opened\n')
end

% set(hObject, 'Enable', 'on');
guidata(hObject, handles)



function edit_holdtime_Callback(hObject, eventdata, handles)
% hObject    handle to edit_holdtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_holdtime as text
%        str2double(get(hObject,'String')) returns contents of edit_holdtime as a double


% --- Executes during object creation, after setting all properties.
function edit_holdtime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_holdtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



