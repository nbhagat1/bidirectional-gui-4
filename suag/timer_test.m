T = 0.1;
myTimer = timer('BusyMode', 'drop', 'ExecutionMode', 'fixedRate', 'Period', T, 'TimerFcn', {@timerfcn});
start(myTimer)

function timerfcn(hObject, event, handles)
    fprintf('in timerfcn\n');
    f2();
    fprintf('completed f2\n');
end

function f2
    fprintf('in f2\n');
    pause(1);    
end
