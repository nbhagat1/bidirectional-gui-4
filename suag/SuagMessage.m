classdef SuagMessage < uint8
    enumeration
        DATA_REQUEST (1)
        START_PLAYBACK (2)
        STOP_PLAYBACK (3)
    end
end