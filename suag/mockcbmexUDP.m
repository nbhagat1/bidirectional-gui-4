% mock cbmex ping

remoteport = 11003;
localport = 11002;
hudp = udp('localhost', 'RemotePort', remoteport, 'LocalPort', localport, ...
    'InputBufferSize', 128*102400*8 + 128, 'OutputBufferSize', 128*102400*8 + 128, 'EnablePortSharing', 'on', ...
    'ReadAsyncMode', 'continuous', 'Timeout', 3);
fopen(hudp);

%% easy reset

fclose(hudp)
clear
clc


%% request data
GET_DATA = uint8(1);
ALL_DATA = typecast(uint32(0), 'uint8');
fwrite(hudp, [GET_DATA ALL_DATA], 'uint8');
pause(0.5);

%read data
temp = fread(hudp, [1], 'uint8');  % sizeA doesn't work
sz = reshape(temp(1:2), 1, []);
data = reshape(temp(3:end), sz);
if ~isempty(sz)
    % datasz = uint8([sz(1) sz(2)+2]); %size of data is first two integers
    % disp(datasz)
    % data = fread(hudp, double(datasz), 'uint8');
    % disp(data)
else
    fprintf('Timed Out\n');
end



