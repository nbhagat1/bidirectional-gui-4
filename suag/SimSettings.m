classdef SimSettings < handle
    properties
        mfr
        wpm
        lambda
        k              
        freq1
        freq2
        x
        y
        sigma
        tau
        t0     % set based on dt control
        ramptime
        holdtime
        label
    end
    
    methods
        function obj = SimSettings(mfr, wpm, lambda, k, freq1, freq2, x, y, sig, tau, t0, label, ramptime, holdtime)
            obj.mfr = mfr;
            obj.wpm = wpm;
            obj.lambda = lambda;
            obj.k = k;
            obj.freq1 = freq1;
            obj.freq2 = freq2;
            obj.x = x;
            obj.y = y;
            obj.sigma = sig;
            obj.tau = tau;
            obj.t0 = t0;
            obj.label = label;
            obj.ramptime = ramptime;
            obj.holdtime = holdtime;
        end
        
        function itemlabel = itemlabel(obj)
            % generate a label for the item
            itemlabel = obj.label;
        end
        
        function entry = settingsfileentry(obj)
            props = properties(obj);
            mapobj = containers.Map({'logical', 'double', 'char'}, {'%d', '%f', '%s'});
            datatypes = cellfun(@(x) mapobj(class(obj.(x))), props, 'UniformOutput', false);            
            propsandvals = cellfun(@(x, y) sprintf(['%s=' y], x, obj.(x)), ...
                props, datatypes, 'UniformOutput', false);
            entry = strjoin(propsandvals, ', ');
        end
    end
end