classdef ObjectArray < handle
    % Object container for data
   properties
      Value
   end
   methods
      function obj = ObjectArray(F)
         if nargin ~= 0
            obj.Value = F;
         end         
      end
   end
end