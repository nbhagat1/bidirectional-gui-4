function data_request(hObject, event, buffer)
%callback for data request, now packing and sending data

temp = fread(hObject, [1 5], 'uint8');
assert(isequal(temp(1), uint8(1)));
numbytes = typecast(temp(2:5), 'uint32');
if numbytes == 0
    data = buffer.read();
else
    data = buffer.read(numbytes);
end

[n, m] = size(data);
dataCol = single([n; m; reshape(data, [], 1)]);
% fprintf('In Callback...')
fwrite(hObject,dataCol,'single');

fprintf('...Data sent!\n')