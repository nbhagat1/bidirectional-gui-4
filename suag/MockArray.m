classdef MockArray < handle
    properties
        sarray    % N x M grid of spike rates for each channel
        warray    % N x M grid of wavelet powers for each channel
        changains % N x M grid of gains for each channel
        curtime   % current time
        sequence  % list of settings that modify the array
        stored    % list of predefined settings
        sr        % data sampling rate
        T         % time between updates
        mode      % manual or replay
        useslider % 0 or 1
        thresh    % trunace the components after a certain decay
        arraydims % array dimensions
        arrayinds % array coordinates that map to electrodes        
        sliderval
        playback  % handle to mat file
        bufferStartTime %time buffer cleared from Realtime loop start request
        generateDataTime %time data gets generated and compared to ^ start time
        
        lfp
        spike
        Lo_D
        Hi_D
        Lo_R
        Hi_R
        
        settingsfile
        recordfile
        recordfid
    end
    
    properties (Dependent)
        wraptime  % reset the time at the end of the sequence
    end
    
    properties (Constant)
        maxlevels = 10;
    end
    
    methods  % cannot have block attributes
        function value = get.wraptime(obj)
            switch obj.mode
                case 'Manual'                
                    value = Inf;
                case 'Replay'                    
                    value = max(arrayfun(@(x) x.t0 + ...
                        obj.sequence(ii).tau * log(obj.sequence(ii).lambda / obj.thresh), ...
                        obj.sequence));
            end
        end
    end
    
    methods (Access = public)
        function obj = MockArray(sr, T, mode_, playbackmatfile, useslider, sliderval, thresh, arraydims, arrayinds, ipaddr, remoteport, localport)
            % constructor
            if ~exist('sr', 'var') || isempty(sr)
                sr = 30e3;
            end
            if ~exist('T', 'var') || isempty(T)
                T = 0.05;
            end
            if ~exist('mode_', 'var') || isempty(mode_)
                mode_ = 'Manual';
            end
            if ~exist('playbackmatfile', 'var') || isempty(playbackmatfile)
                playbackmatfile = '';
            end
            if ~exist('thresh', 'var') || isempty(thresh)
                thresh = 1;
            end
            if ~exist('arraydims', 'var') || isempty(arraydims)
                arraydims = [11 11];
            end
            if ~exist('arrayinds', 'var') || isempty(arrayinds)
                arrayinds = mod(0:127, prod(obj.arraydims)) + 1;  % setdiff(1:100, [1 10 91 100]);
            end
            if ~exist('usesilder', 'var') || isempty(useslider)
                useslider = false;
            end
            if ~exist('sliderval', 'var') || isempty(sliderval)
                sliderval = 1;
            end
            
            obj.thresh = thresh;
            obj.sr = sr;
            obj.T = T;
            obj.mode = mode_;
            obj.useslider = useslider;
            obj.sliderval = sliderval;
            obj.sarray = zeros(prod(obj.arraydims), 1);            
            obj.warray = ones(prod(obj.arraydims), round(obj.sr*obj.T));  % default to unity gain
            obj.changains = zeros(prod(obj.arraydims), 1);
            obj.curtime = 0;
            obj.sequence = [];
            obj.stored = [];
            obj.arraydims = arraydims;
            obj.arrayinds = arrayinds;
            
            load('spike.mat', 'template');
            load('lfp.mat', 'shuffled');
            obj.lfp = shuffled;
            obj.spike = template;
            [obj.Lo_D, obj.Hi_D] = wfilters('db3', 'd');
            [obj.Lo_R, obj.Hi_R] = wfilters('db3', 'r');
            if exist(playbackmatfile, 'file')
                obj.playback = matfile(playbackmatfile, 'Writable', false);
            else
                obj.playback = [];
            end
            % [obj.Lo_D, obj.Hi_D] = daubcqf(6, 'max');
        end
        
        function delete(obj)
            if ~isempty(obj.recordfid) && obj.recordfid > 0
                fclose(obj.recordfid);
            end
            if isobject(obj.playback) && isvalid(obj.playback)
                delete(obj.playback)
            end
            % if ~isempty(obj.udp) && obj.udp.isvalid && strcmp(handles.udp.Status, 'open')
            %     fclose(obj.udp);
            % end                
        end
        
        function success = additem(obj, item, dest)
            % add an item to the end of the sequence
            assert(isa(item, 'SimSettings'));
            if ~exist('dest', 'var') || isempty(dest)                
                dest = 'stored';
            end
            assert(ismember(dest, {'sequence', 'stored'}));
            try
                if strcmp(dest, 'stored') && ...
                        ismember({item.label}, arrayfun(@(x) x.label, obj.stored, 'UniformOutput', false))                    
                    % stored settings must have unique labels but the sequence can reuse them
                    warning('item labels must be unique');
                    success = false;
                    return                    
                end
            catch ex
                disp(ex);
            end
            success = true;
            
            switch dest
                case 'stored'
                    obj.stored = [obj.stored item];
                case 'sequence'            
                    obj.sequence = [obj.sequence item];
            end
        end
        
        function removeitem(obj, ind)
            % remove an item from the sequence
            delete(obj.sequence(ind));
            obj.sequence(ind) = [];
        end
        
        function updatearray(obj)
            switch obj.mode
                case 'Manual'
                    % update the spike rates and wavelet powers for each channel
                    [X, Y] = meshgrid(1:obj.arraydims(1), 1:obj.arraydims(2));
                    f = linspace(0, obj.sr, size(obj.warray, 2) + 1);
                    f = f(1:end - 1);
                    % scales = 1:obj.maxlevels + 1;

                    removelist = [];
                    sarray_ = zeros(numel(X), 1);
                    changains_ = zeros(numel(X), 1);
                    warray_ = ones(numel(X), size(obj.warray, 2));            
                    for ii = 1:length(obj.sequence)
                        % fprintf('debug: curtime = %f, sequence(%d).t0 = %f\n', obj.curtime, ii, obj.sequence(ii).t0)
                        if obj.curtime >= obj.sequence(ii).t0   % item started before current time                    
                            dt = obj.sequence(ii).tau * log(obj.sequence(ii).lambda / obj.thresh);
                            if obj.curtime - obj.sequence(ii).ramptime - obj.sequence(ii).holdtime <= obj.sequence(ii).t0 + dt || obj.useslider  % if item hasn't decayed close to zero yet
                                % only add components that are
                                mu = [obj.sequence(ii).x obj.sequence(ii).y];
                                s = mvnpdf([X(:) Y(:)], mu, obj.sequence(ii).sigma*eye(2));  % 100 x 1
                                norms = s / max(max(s));
                                changains_ = max(changains_, norms);  % I want do display this regardless of whether or not MFR of MWP is on
                                if obj.useslider
                                    changains_ = changains_ * obj.sliderval;
                                end

                                if obj.sequence(ii).mfr
                                    % use max over components rather than summation,
                                    % but not max over time or it won't decay
                                    % if obj.curtime - obj.T < obj.sequence(ii).t0
                                    sarray_ = max(sarray_, obj.sequence(ii).lambda * norms * ...
                                        exp(-(obj.curtime - obj.sequence(ii).t0)/obj.sequence(ii).tau));
                                end

                                if obj.sequence(ii).wpm
                                    % Wavelet power
                                    freqinds = f >= obj.sequence(ii).freq1 & f <= obj.sequence(ii).freq2;
                                    freqinds(2:end) = freqinds(2:end) | freqinds(end:-1:2);  % just keeping it real
                                    k = obj.sequence(ii).k * norms;
                                    if obj.useslider
                                        k = k * obj.sliderval;
                                    else                                
                                        t = obj.curtime - obj.sequence(ii).t0;                                
                                        % ramp = max(1, exp(1) * t / obj.sequence(ii).tau);                                
                                        % k = k * (1 + exp(-t / obj.sequence(ii).tau)) * ramp;  % scalar gain for each channel

                                        if t < obj.sequence(ii).ramptime
                                            k = k * t / obj.sequence(ii).ramptime;
                                        elseif t < obj.sequence(ii).ramptime + obj.sequence(ii).holdtime
                                            % pass
                                        else
                                            k = k * exp(-(t - obj.sequence(ii).ramptime - obj.sequence(ii).holdtime) / obj.sequence(ii).tau);
                                        end
                                    end
                                    % TODO temporarily remove variance
                                    k = k + 0*sqrt(k)*randn(1);  % add variance to gains as a function of time
                                    warray_(:, freqinds) = max(warray_(:, freqinds), k);  % apply scalar gain to frquency bands
                                end
                            end

                            if strcmp(obj.mode, 'Manual') && ~obj.useslider                        
                                if obj.curtime - obj.sequence(ii).ramptime - obj.sequence(ii).holdtime > obj.sequence(ii).t0 + dt
                                    % remove items when in manual mode
                                    removelist = [removelist ii];  %#ok
                                    % disp(removelist);
                                end
                            end                                        
                        end
                    end
                    obj.sarray = sarray_;
                    obj.warray = warray_;
                    obj.changains = changains_;

                    % remove items in the reverse order
                    for remind = length(removelist):-1:1
                        obj.removeitem(removelist(remind))
                    end
                case 'Replay'
                    % pass
            end
            
            obj.curtime = obj.curtime + obj.T;
            if obj.curtime >= obj.wraptime
                obj.curtime = 0;
            end
        end
        
        function [data, ts] = generatedata(obj)
            switch obj.mode
                case 'Manual'
                    % TODO can cbmex be configured to return analog data with
                    % spikes, or LPF data and spike timestamps and waveforms?
                    numchans = length(obj.sarray);
                    numsamples_ = round(obj.sr * obj.T);
                    % numsamples = numsamples_;  % 2^nextpow2(numsamples_);  % fast wavelet decomposition requires a power of 2 so buffer some data
                    % numbuf = numsamples - numsamples_;
                    % inds = mod(round(obj.curtime * obj.sr) - numbuf + (0:numsamples - 1), size(obj.lfp, 2)) + 1;
                    inds = mod(round(obj.curtime * obj.sr) + (0:numsamples_ - 1), size(obj.lfp, 2)) + 1;

                    % data = zeros(length(obj.arrayinds), numsamples_);

                    usewavelets = false;
                    if usewavelets            
                        % Rice Wavelet Toolbox crashed Matlab
                        % wavedec and waverec are too slow and making GUI nonresponsive
                        % Can I use FFT?
                        % for each channel
                        tic;
                        for ichan = 1:length(obj.arrayinds)
                            % get the data
                            x = obj.lfp(mod(ichan - 1, size(obj.lfp, 1)) + 1, inds)';
                            % perform the wavelet decomposition
                            [y, L] = wavedec(x, obj.maxlevels, obj.Lo_D, obj.Hi_D);
                            % y = mdwt(x, obj.Lo_D, obj.maxlevels);
                            % apply the gain for each level
                            % reps = arrayfun(@(x) length(y)/2^x, [obj.maxlevels obj.maxlevels:-1:1]);
                            % inds_ = cumsum([0 reps]);
                            inds_ = cumsum([0;L]);
                            y2 = cell2mat(arrayfun(@(x) obj.warray(mod(ichan - 1, size(obj.warray, 1)) + 1, x) * ...
                                y(inds_(x)+1:inds_(x+1)), 1:length(L) - 1, 'UniformOutput', false)');
                            % apply wavelet reconstruction
                            % x = midwt(y2, obj.Lo_D, obj.maxlevels);
                            x = waverec(y2, L, obj.Lo_R, obj.Hi_R);
                            data(ichan, :) = x(end - numsamples_ + 1:end);
                        end
                        toc;
                    else
                        chaninds = mod(obj.arrayinds, 10) + 1;            
                        fftdata = fft(obj.lfp(:, inds)');   % fft lfp data
                        fftdata = fftdata(:, chaninds);     % expand from 10 to 96 channels
                        arrayinds_ = mod(obj.arrayinds - 1, size(obj.warray, 1)) + 1;
                        data = ifft(fftdata .* obj.warray(arrayinds_, :)'); % apply frequency dependent gains
                        % add 32 additional channels
                        % data = [data zeros(size(data, 1), 32)];
                    end

                    % add spikes
                    maxspikes = ceil(2 * max(obj.sarray) * obj.T);
                    spiketimes = cumsum(bsxfun(@rdivide, -log(rand(numchans, maxspikes)), obj.sarray), 2);        
                    ts = cell(length(obj.arrayinds), 1);
                    for ichan = 1:length(obj.arrayinds)
                        mchan = mod(ichan - 1, size(spiketimes, 1)) + 1;
                        inds1 = spiketimes(mchan, :) < obj.T;
                        if ~isempty(inds1) && nnz(inds1) > 0  % TODO I updated this without testing
                            ts{ichan, 1} = obj.curtime + spiketimes(mchan, inds1);

                            % Don't superimpose the spikes on the LFP. Keep them separate.
                            % inds2 = ceil(obj.sr*spiketimes(mchan, inds1));                    
                            % deltas = zeros(numsamples_, 1);
                            % deltas(inds2) = 1;
                            % try
                            %     data(:, ichan) = data(:, ichan) + conv(deltas, obj.spike, 'same');
                            % catch ex
                            %     disp(ex);
                            % end
                        end
                    end
                case 'Replay'                    
                    ts = cell(length(obj.arrayinds), 1);  % Should the playback file include spike data?
                    numsamples_ = round(obj.sr * obj.T);
                    if isobject(obj.playback)
                        details = whos(obj.playback, 'data');
                        sz = details.size;  % size(hmat.data) is slow, use whos
                        inds = mod(round(obj.curtime * obj.sr) + (0:numsamples_ - 1), sz(2)) + 1;
                        
                        diffInds = diff(inds);
                        indsBreak = find(diffInds~=1);
                        if ~isempty(indsBreak)
                            %when data file gets to end and wraps around,
                            %must index into seperately
                            data1 = obj.playback.data(:, inds(1:indsBreak));
                            data2 = obj.playback.data(:, inds(indsBreak+1:end));
                            data = [data1 data2]';
                        else
                            data = obj.playback.data(:, inds)';
                        end
%                         fprintf('%d %d\n', inds(1), inds(end))
                        
                    else
                        % If no datafile provided output zeros
                        data = zeros(numsamples_, length(obj.arrayinds));
                    end
            end
            
            %comparator time for suag data alignment with real time system
            %   > through this into data somehow?
%              obj.generateDataTime = etime(obj.bufferStartTime, clock);
            
        end
        
        function write_settings_file(obj, settingsfile)
            if ~exist('settingsfile', 'var') || isempty(settingsfile) || isempty(obj.settingsfile)
                % Save As won't pass in settingsfile and first save
                % obj.settingsfile won't have been set
                filterspec = {'*.txt', 'Text File (*.txt)'; '*.*', 'All Files (*.*)'};
                dialogtitle = 'Save Settings File';
                defaultname = 'simsettings.txt';
                [fn, pn] = uiputfile(filterspec, dialogtitle, defaultname);
                obj.settingsfile = [pn fn];            
            end
            fid = fopen(obj.settingsfile, 'w');
            cleanupobj = onCleanup(@() fclose(fid));
            for isetting = 1:length(obj.sequence)
                fprintf(fid, '%s\n', obj.sequence(isetting).settingsfileentry);
            end
            delete(cleanupobj);
        end
        
        function set_record_file(obj)
            filterspec = {'*.bin', 'Binary File (*.bin)'; '*.*', 'All Files (*.*)'};
            dialogtitle = 'Save Recording File';
            defaultname = ['recording_' strrep(datestr(now), ':', '_') '.bin'];
            [fn, pn] = uiputfile(filterspec, dialogtitle, defaultname);
            obj.recordfile = [pn fn];
        end
        
        function record_data(obj, data)
            if isempty(obj.recordfid)
                obj.recordfid = fopen(obj.recordfile, 'w');
                % write header
                fwrite(obj.recordfid, uint64(size(data, 1)), 'unit64');
            end
            fwrite(obj.recordfid, single(reshape(data', [], 1)), 'single');
        end
    end  % end of methods
end  % end of class