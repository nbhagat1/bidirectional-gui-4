%mockcbmex tcp example

tcpipClient = tcpip('127.0.0.1',55000,'NetworkRole','Client');
set(tcpipClient,'InputBufferSize',128*102400*8);
set(tcpipClient,'Timeout',30);
fopen(tcpipClient);
rawData = fread(tcpipClient,104857600/8,'double');
thisData = reshape(rawData, 128,128);
figure;imagesc(thisData)