function [varargout] = mockcbmex(command, varargin)
% Create a function that mimics cbmex. Create a functor that is either this
% or cbmex in the code
global htcp hudp

GET_DATA = uint8(1);
ALL_DATA = typecast(uint32(2200), 'uint8'); % 128 * 1000 matrix request)
START_DATA = uint8(2);
STOP_DATA = uint8(3);
BLANK_REQUEST = typecast(uint32(0), 'uint8');

switch command
    case 'open'
        % I can ignore additional inputs
        % open the simulated Utah array GUI
        connection = [];
        instrument = [];
%         if isempty(htcp) || ~htcp.isvalid()
            
         htcp = tcpip('127.0.0.1',4000,'NetworkRole','Client', 'ReadAsyncMode', 'continuous');
            set(htcp,'InputBufferSize',65536008+100);%102400 128*1000*4+10
            set(htcp,'Timeout',0.3);
            fopen(htcp);
            
            if ~strcmp(htcp.Status, 'open')
                fprintf('could not open UDP object\n');
                return
            end
            
            connection = 1;
            instrument = 1;
%         end
        
        varargout{1} = connection;
        varargout{2} = instrument;
       

    case 'trialdata'
        % gets data from the buffer
        if isempty(htcp)
            fprintf('call mockcbmex(''open'')\n');
            varargout{1} = [];
            varargout{2} = [];
            varargout{3} = [];
            return
        end
        
        
        % default inputs
        instance = [];
        
        active = varargin{1};
        if nargin > 2, instance = varargin{2};end
        
        % get the data
        % data = handles.buffer.raw(handles.bufptr:handles.buffer.lst, :);   % fst only gets reset on clear
        %         GET_DATA = uint8(1);
        %         ALL_DATA = typecast(uint32(0), 'uint8');
        %         fwrite(hudp, [GET_DATA ALL_DATA], 'uint8');
        %         waitfordata = true;
        
        
        %aquiring data over TCP/IP
        %request data
        a = tic;        
%         fprintf('sending data request\n')
        fwrite(htcp, [GET_DATA ALL_DATA], 'uint8');
%         pause(0.002);
        
        %read data
        loopctr = 0;
        temp = [];
        timeStamp = [];        
        while htcp.BytesAvailable > 0            
            sizeD = fread(htcp, 2, 'single');
            numSinglesToRead = (sizeD(1)*sizeD(2));
            
            temp = [temp; reshape(fread(htcp, numSinglesToRead,'single'), sizeD')];
            timeStamp = [typecast(single(fread(htcp, 2, 'single')'), 'double')];
            
            loopctr = loopctr + 1;
        end
        timeStampLocal = now();
                
%         fprintf('%d times through loop\n', loopctr);
                
        dtSec = (timeStampLocal - timeStamp) * 86400;
%         fprintf('got data back\n')
%             b = toc(a);
%             fprintf('Data received: time %f \n', b)
        %reformat data
        sr = 30e3;
        if ~isempty(temp)
            data = temp;  % reshape(temp, sizeD(1),sizeD(2));
            numChan = 32;
            countinuous_cell_array(1:numChan, 1:2) = num2cell([(1:numChan)' sr.*ones(numChan,1)]);
            countinuous_cell_array(:,3) = arrayfun(@(x) data(:,x), (1:numChan), 'UniformOutput', false);
            varargout{2} = dtSec;
            varargout{3} = countinuous_cell_array;
            
%             b = toc(a);
%             fprintf('...Data received!\n')
%             fprintf('Data received: time %f \n', b)
        else
            fprintf('no data received\n')
            varargout{2} = 0;
            varargout{3} =  -1;
        end
        
        
        
        % TODO since we won't get GUI parameters directly, we read from the data
        % buffer
        %         handles = guidata(htcp);
        
        % outputs
        %         maxunits = 6;
        %         timestamps_cell_array = cell(152, maxunits + 1);  % TODO up to 6 units per channel
        %         timestamps_cell_array(:, 1) = [arrayfun(@(x) sprintf('chan%d', x), 1:128, 'UniformOutput', false)'; ...
        %             arrayfun(@(x) sprintf('ainp%d', x), 1:16, 'UniformOutput', false)'; ...
        %             arrayfun(@(x) sprintf('aout%d', x), 1:4, 'UniformOutput', false)'; ...
        %             arrayfun(@(x) sprintf('audio%d', x), 1:2, 'UniformOutput', false)'; ...
        %             {'digin1'}; {'serial1'}];
        % time = handles.mockarray.curtime;
        
        % TODO rather than create a buffer in SUAG, SUAG should always send
        % the data so it gets stored in the network buffer
        % continuous_cell_array = cell(handles.buffer.vecSz, 3);
        % continuous_cell_array = cell(handles.buffer.numchans, 3);
        
        % running = handles.timer.Running;
        % if strcmp(running, 'on')
        %     stop(handles.timer)
        % end
        % data = handles.buffer.read();
        % if strcmp(running, 'on')
        %     start(handles.timer);
        % end
        
        % handles.bufptr = handles.buffer.lst + 1;
        % chans = (1:handles.buffer.vecSz)';
        % chans = (1:handles.buffer.numchans)';
        % sr = handles.mockarray.sr;
        % srs = sr * ones(handles.buffer.vecSz, 1);
        % srs = sr * ones(handles.buffer.numchans, 1);
        % continuous_cell_array(1:handles.buffer.vecSz, 1:2) = ...
        % continuous_cell_array(1:handles.buffer.numchans, 1:2) = ...
        %     num2cell([chans srs]);  % channum sr values
        % continuous_cell_array(:, 3) = arrayfun(@(x) data(:, x), ...
        %     1:handles.buffer.numchans, ...  % handles.buffer.vecSz
        %     'UniformOutput', false);
        
        % timestamps_cell_array(1:size(handles.spkbuf.Value, 1), (1:size(handles.spkbuf.Value, 2)) + 1) = handles.spkbuf.Value;
        
        % if active == 1
        % flushes the data cache and starts buffering data
        %     handles.buffer.clear();
        % end
        % TODO am I interpreting active correctly? I want to clear the
        % spike buffer every time. The continuous buffer isn't cleared
        % unless active is true but the pointer is updated so I don't get
        % the previous data.
        % handles.spkbuf.Value = cell(size(handles.spkbuf.Value));
        
        % guidata(hudp, handles);
        
        % timeout = 5;
        % t0 = clock;
        % while waitfordata && etime(clock, t0) < timeout
        %         sz = fread(hudp, [1 2], 'uint16');
        %         if ~isempty(sz)
        %             disp(sz);
        %             data = fread(hudp, sz, 'single');
        %         else
        %             fprintf('Timed Out\n');
        %         end
        
        % pause(.001);
        % end
        
        %         guidata(hudp, handles);
        
        % varargout{1} = timestamps_cell_array;
        % varargout{2} = time;
        % varargout{3} = continuous_cell_array;
        
    case 'trialconfig'
        fwrite(htcp, [START_DATA BLANK_REQUEST], 'uint8');
        
        %         % control data buffering
        %         config_vector_in = [];
        %         instance = [];
        %         dbl = [];
        %         absolute = [];
        %         noevent = [];
        %         nocontinuous = [];
        %         continuous = 102400;
        %         event = 2097152;
        %         comment = [];
        %         tracking = [];
        %
        %         active = varargin{1};
        %         if nargin > 2, config_vector_in = varargin{2};end
        %         if nargin > 3, instance = varargin{3};end
        %         if nargin > 4, dbl = varargin{4};end
        %         if nargin > 5, absolute = varargin{5};end
        %         if nargin > 6, noevent = varargin{6};end
        %         if nargin > 7, nocontinuous = varargin{7};end
        %         if nargin > 8, continuous = varargin{8};end
        %         if nargin > 9, event = varargin{9};end
        %         if nargin > 10, comment = varargin{10};end
        %         if nargin > 11, tracking = varargin{11};end
        %
        %         active_state = [];
        %         config_vector_out = [];
        %
        %         % TODO ____ won't exist so can't get handles
        %         % since we won't change GUI parameters directly, we need to send a
        %         % packet to SUAG
        %         %         handles = guidata(hudp);
        %         if active == 1
        %             % flushes the data cache and starts buffering data
        %             handles.buffer.clear();
        %             if strcmp(handles.timer.Running, 'off')
        %                 start(handles.timer);
        %                 active_state = 0;
        %             else
        %                 active_state = 1;
        %             end
        %         else
        %             % stops buffering
        %             if strcmp(handles.timer.Running, 'on')
        %                 stop(handles.timer);
        %                 active_state = 1;
        %             else
        %                 active_state = 0;
        %             end
        %         end
        %         %         guidata(hudp, handles);
        %
        %         varargout{1} = active_state;
        %         varargout{2} = config_vector_out;
        
    case 'fileconfig'
        
    case 'config'
        
    case 'time'
        
    case 'digitalout'
        
    case 'chanlabel'
        
    case 'mask'
        
    case 'comment'
        
    case 'analogout'
        
    case 'trialcomment'
        
    case 'trialtracking'
        
    case 'ccf'
        
    case 'system'
        
    case 'close'
        if ~isempty(htcp) && htcp.isvalid()
            %close socket
            fclose(htcp);
            fclose(instrfindall);
            delete(htcp);
        end
        if ~isempty(hudp) && hudp.isvalid()
            %close socket
            fclose(hudp);
            fclose(instrfindall);
            delete(hudp);
        end
        
        case 'openUDP'
        %Initialize UDP port for listener
        remoteport = 11003;
        localport = 11002;
        hudp = udp('localhost', 'RemotePort', remoteport, 'LocalPort', localport, ...
            'InputBufferSize', 128*102400*4, 'OutputBufferSize', 128*102400*4, 'EnablePortSharing', 'on', ...
            'ReadAsyncMode', 'continuous', 'Timeout', 2);
        set(hudp, 'DatagramTerminateMode', 'off')
        fopen(hudp);
        
    %not in use bc using TCP/IP protocol    
    case 'trialdataUDP'
        
        if isempty(hudp)
            fprintf('call mockcbmex(''open'')\n');
            varargout{1} = [];
            varargout{2} = [];
            varargout{3} = [];
            return
        end
        
        %aquiring data over UDP
        %request data
        
        fwrite(hudp, [GET_DATA ALL_DATA], 'uint8');
        pause(0.005);
        
        %read data
        temp = fread(hudp, 512000/4,'single');
        sr = 30e3;
        %reformat data
        if ~isempty(temp)
            data = reshape(temp, 1000,128);
            countinuous_cell_array(1:128, 1:2) = num2cell([(1:128)' sr.*ones(128,1)]);
            countinuous_cell_array(:,3) = arrayfun(@(x) data(:,x), (1:128), 'UniformOutput', false);
            varargout{2} = clock;
            varargout{3} = countinuous_cell_array;
        else
            fprintf('no data received\n')
            varargout{2} = clock;
            varargout{3} =  -1;
        end
            
    otherwise
        
end


% function mycallback(hObject, eventdata)
% global waitfordata rcvdata
% sz = fread(hObject, [1 2], 'uint16');
% rcvdata = fread(hObject, sz, 'single');
% waitfordata = false;