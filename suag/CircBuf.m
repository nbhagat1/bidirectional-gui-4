classdef CircBuf < handle
    % I downloaded a class called circvBuf but I didn't understand what it was doing so I'm making my own here
    properties (SetAccess = private, GetAccess = public)
        raw
        rdptr
        wtptr
        available
        overflowflag
        mutex
    end
    
    properties (Dependent)
        numchans
        numsamples
    end
    
    methods
        function val = get.numchans(obj)
            val = size(obj.raw, 2);
        end
        
        function val = get.numsamples(obj)
            val = size(obj.raw, 1);
        end
    end
    
    methods (Access = public)
        function obj = CircBuf(numsamples, numchans)
            obj.raw = NaN(numsamples, numchans);
            obj.rdptr = 1;
            obj.wtptr = 1;
            obj.available = 0;
            obj.overflowflag = false;
            obj.mutex = false;
        end
        
        function write(obj, data)
%             ct = 0;
%             while obj.mutex
%                 pause(.01);  % This allows additional timer writes to occur
%                 ct = ct + 1;
%                 if ct > 200
%                     warning('stuck in mutex loop (write)');
%                     break
%                 end
%             end
            obj.mutex = true;
%             fprintf('start: %d\n', obj.wtptr);
            
            inds = mod(obj.wtptr + (0:size(data, 1) - 1) - 1, obj.numsamples) + 1;
            obj.raw(inds, :) = data;
            obj.wtptr = mod(inds(end), obj.numsamples) + 1;
            obj.available = obj.available + size(data, 1);
            if obj.available > obj.numsamples 
                if ~obj.overflowflag
                    % only once consecutively
                    warning('buffer overflowed');
                    obj.overflowflag = true;
                end
                obj.rdptr = obj.wtptr;
                obj.available = obj.numsamples;                
            else
                obj.overflowflag = false;
            end
            
%             fprintf('end: %d, (%f)\n', obj.wtptr, obj.raw(inds(1), 1));
            obj.mutex = false;
        end
           
        function data = read(obj, n)
            ct = 0;
            while obj.mutex
%                 fprintf('mutex\n');
                pause(.01);
                ct = ct + 1;
                if ct > 200
                    warning('stuck in mutex loop (read)');
                    break
                end
            end            
            
            if ~exist('n', 'var') || isempty(n)
                n = obj.available;
            end
            if n > obj.available
                warning('n = %d > available = %d. Setting n = available', n, obj.available);
                n = obj.available;
            end
            if n == 0
                data = zeros(0, obj.numchans);
                return
            end
            
            inds = mod(obj.rdptr + (0:n - 1) - 1, obj.numsamples) + 1;
            
%             fprintf('read before %d (%f)\n', obj.rdptr, obj.raw(inds(1), 1));
            data = obj.raw(inds, :);
            obj.rdptr = mod(inds(end), obj.numsamples) + 1;
%             fprintf('update %d\n', obj.rdptr);
            obj.available = obj.available - n;
        end
        
        function clear(obj)
%             ct = 0;
%             while obj.mutex
%                 pause(.01);
%                 ct = ct + 1;
%                 if ct > 200
%                     warning('stuck in mutex loop (clear)');
%                     break
%                 end
%             end
%             obj.mutex = true;
            
            obj.raw = NaN(obj.numsamples, obj.numchans);
            obj.rdptr = 1;
            obj.wtptr = 1;
            obj.available = 0;
            
%             obj.mutex = false;
        end
    end
end