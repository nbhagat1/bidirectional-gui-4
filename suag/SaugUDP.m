classdef SaugUDP < handle
    properties
        handles
    end
    
    methods
        function obj = SaugUDP()            
            ipaddr = 'localhost';
            remoteport = 11002;
            localport = 11003;
            obj.handles.udp = udp(ipaddr, 'RemotePort', remoteport, 'LocalPort', localport, ...
                'InputBufferSize', 65535, 'OutputBufferSize', 65535, 'EnablePortSharing', 'on', ...
                'ReadAsyncMode', 'continuous', ...
                'BytesAvailableFcnCount', 5, 'BytesAvailableFcnMode', 'byte', ...  % 5
                'BytesAvailableFcn', @(hObject, eventdata) obj.data_request(hObject, eventdata));  % data_request(hObject, eventdata, handles.buffer)
            fopen(obj.handles.udp);
            % hcleanup = onCleanup(fclose(handles.udp));
            if ~strcmp(obj.handles.udp.Status, 'open')
                fprintf('could not open UDP object\n');
                return
            end
            
            %% easy reset
            %
            % fclose(handles.udp)
            % clear
            % clc
        end
    end
    
    methods (Static)
        function data_request(hObject, eventdata)
            %callback for data request, now sending data
            fprintf('In Callback\n');
            buffer = fread(hObject);
            sz = size(buffer);
            datapacket = uint8([sz reshape(buffer, 1, [])]);
            disp(datapacket)
            
            fwrite(hObject, datapacket)
        end
    end
end

%%



% msg = 1%fread(hObject, 1, 'uint8');
% switch msg
%     case GET_DATA        
%         numptstosend = fread(hObject, 1, 'uint32');
%         if numptstosend == 0
%             data = single(buffer.read());
%         else
%             data = single(buffer.read(numptstosend));
%         end
%         fprintf('SENDING DATA');
%         fwrite(hObject, [uint16(size(data)) typecast(single(data), 'uint16')], 'uint16');
%     case CHANGE_PARAMS
%         % pass
% end
