data = Block_1_results;
chans = [1,2];
dt = 1/30000;
totalTime_sec = sum(data.whileLoopTime);
 
A = data.neuralData';   % padded with zeros at end
A = A(sum(A')~=0, :);  % remove padding
 
[nr nc] = size(A);
t = 0:dt:(dt*(nr-1)); 
 
cues = data.labels;   % 'padded' with NaNs
cues = cues(~isnan(cues))';  % remove 'padding'
 
figure
s1 = subplot(2,1,1);
plot(t,cues)
s2 = subplot(2,1,2);
plot(t,A(:,chans))
linkaxes([s1,s2],'x')
axis tight