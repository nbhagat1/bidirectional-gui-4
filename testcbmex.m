% testcbmex
dataSavePath = 'C:\Users\NB\Data';
filename = datestr(clock, 'dd-mmm-yyyy' );

% Initialize
[connection] = cbmex('open', 1);

[active_state, config_vector_out] = cbmex('trialconfig', 1 , 'double');

temp = cbmex('trialdata',1);

cbmex('fileconfig', [dataSavePath '\' filename], '', 1)

pause(5)

[spike_data, buffer_start_time_sec, contdata] = cbmex('trialdata',1);

cbmex('fileconfig',[dataSavePath '\' filename], '', 0)

figure; plot(contdata{[1:4],3})