function [results, handles] = main_RealTimeLoop(handles, hObject)
%{
This is the main real time function that will do everything necessary
within our time bin (i.e. .refreshRate -- set in opening fcn of GUI)

Input: Note > GUI call to start main loop

Output: results from while loop test


By Johnny Ciancibello, Sept. 29, 2017
%}

%% set up
% persistent cueTimes cueLabel u
global cueNum numitems runRTS regressionTask currLabel regressLabel cueTimes cueLabel reggLabel reggLabels u UpdateAnimationTimer

%timing testing
% profile on

%Simulation mode or calling cbmex?
getInputData = handles.functor;
if isequal(getInputData, @mockcbmex)
    dt = 0.1;% 5 Hz 
    handles.refreshRatert = 10;%Hz
    numSUAGChan = 32;
    numTotalChan = 128;
    upsampleChannels = true;
else    
    dt = handles.refreshRateSec;% 0.1sec or  10 Hz
    numTotalChan = 128 + 8; % Added 8 analog input channels - Nikunj 6-8-19    
    upsampleChannels = false;
end

%extract block info for Real Time System
if handles.blockInfo.closedLoop
    %these are the decoder parameters
    closedLoop = true;
    decoderMdl = handles.filterInfo.sFilters{handles.blockInfo.decoderSelectedNumber};
else
    closedLoop = false;
end
if handles.blockInfo.nmStim
    nmstim = true;
else
    nmstim = false;
end
if handles.blockInfo.sensoryStim
    encoder = handles.blockInfo.encoderSelected;
    sensoryStim = true;
end
if handles.blockInfo.artifactRejection
    artifactRejection = true;
    artifactRejectionFilter = handles.blockInfo.artifactRejectionSelected;
else 
    artifactRejection = false;
end

%Set up Cue Label information
if isfield(handles.blockInfo, 'regressionLabel')
    regressLabel = [str2num(handles.blockInfo.regressionLabel)];
    regressionTask = true;
else
    regressionTask = false;
end
cueTimes = [0; str2num(handles.blockInfo.cueTime)];
cueLabel = handles.blockInfo.cue;
numitems = length(get(handles.taskDisplayBox, 'String'));

%Initialize UDP port for cues to be sent to Unity
UDP_IP = "127.0.0.1";
UDP_PORT = 11000;
u = udp(UDP_IP, UDP_PORT, 'InputBufferSize', 1024, 'OutputBufferSize', 1024, 'EnablePortSharing', 'on', ...
    'ReadAsyncMode', 'continuous', 'BytesAvailableFcn', @mycallback, 'BytesAvailableFcnMode', 'byte');
fopen(u);
if ~strcmp(u.Status, 'open')
    fprintf('could not open UDP object\n');
    return
end
hcleanup = onCleanup(@() fclose(u));


%Initialize data saving matrices that are populated below:
%  estimate the block duration based on task length
sr = handles.sr;
n = ceil(sr*(sum(cueTimes)+4)); %Added +4 - Nikunj, 6-10-19
n10hz = ceil(handles.refreshRate*(sum(cueTimes)+4)); %Added +4 - Nikunj, 6-10-19
% n1000hz = ceil(1000*(sum(cueTimes)+4)); %Added - Nikunj, 8-5-19, AnalogIn from NSP is sampled at 1kHz
neuraldata    = zeros(numTotalChan, n);
% analogIndata  = zeros(16, n1000hz); 
waveletdata   = zeros(numTotalChan, n10hz);
benddata = zeros(n10hz,23); 
labelWav      = nan(1,n10hz);
labels        = zeros(1,n);
reggLabels    = zeros(1,n);
beginIndex    = 1;
beginIndex2    = 1;
% beginIndexFeat = 1;
indexTracker = zeros(n10hz,2);
runTime = sum(cueTimes); %seconds
spc = sr*dt; %samples per cycle
table_entry = 0;
results.dtSec = zeros(n10hz,1);
results.loopTime = zeros(n10hz,1);
results.loopTime2 = zeros(n10hz,1);
results.whileLoopTime = zeros(n10hz,1);
results.whileLoopTime2 = zeros(n10hz,1);

%low pass filter before downsampling to removing aliasing affect
fs = 10000; % initial sampling frequency
%[n,wn] = buttord(1/(fs/2), 2500/(fs/2), 3, 50);
%[b,a] = butter(2, 2500/(fs/2), 'low');

% BoxCAR variables
numseconds = 1; % 'numseconds' Moving average
% memoryvar = numseconds*sr; % number of samples for moving average
memoryvar = numseconds*(handles.refreshRate);

%take start time of loop
handles.sessionInfo.startTime = clock;
handles.sessionInfo.loopStartTime = tic;

iterNum = 1;
currTime = clock;
cueNum   = 1;
set(handles.taskDisplayBox, 'Enable', 'inactive', 'Value', cueNum);
currLabel = 0; % Initialize - Nikunj 6-10-19
runRTS = true;

% Animation update timer
UpdateAnimationTimer = timer('TimerFcn',{@UpdateAnimation_Callback, hObject},'Period', handles.AnimationUpdateRate, 'ExecutionMode','fixedRate');
start(UpdateAnimationTimer); 

profile off;
profile on -history;
if handles.nspConnected
    % start library collecting data to return as a double
    getInputData('trialconfig', 1, 'double');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% Real Time System %%%%%%%%
while runRTS
    
    startTimeLoop = tic; %loop time to be compared after computations are done
    startLoopTime = now();
    
    %First, check if we pressed stop on GUI to exit loop
    %       OR if we have sent all the trials are complete
%     running = get(handles.startButton, 'UserData'); 
%     if (~running || ~runRTS) %cueNum > (size(cueTimes,1)+1)
%         fprintf('leaving real time environment\n')
%         break;
%     end
%     
    %Second, check if need to send hand cue to Unity
%     if etime(clock, currTime) >= cueTimes(cueNum)  || cueNum == 1
%         %generate udp message
%         destination = DestinationHand.CUE; % always cue hand here
%         labelToSend = (cueLabel{cueNum});
%         
%         if ~regressionTask
%             currLabel = GenerateClassificationLabels(labelToSend, cueLabel, cueNum, destination, u);
%             if handles.bendConnected
%                 fprintf(handles.serialBend,'%c',currLabel);
%             end
%         else
%             [currLabel, reggLabel] = GenerateRegressionLabels(labelToSend, cueLabel, regressLabel, cueNum, destination, u);
%         end
%             
%         %increment index for next cue presentation
%         set(handles.taskDisplayBox, 'Value', cueNum);
%         cueNum2 = mod(cueNum, numitems);  % The last item should wrap around        
%         cueNum = cueNum2 + 1;
%         
%         %When done with last cue, break from real time system
%         if cueNum == 1 && cueNum2 == 0
%             runRTS = false;
%         end
%         
%         currTime = clock;
%     end
    %% Get raw neural data    
    profile resume;
    if handles.nspConnected
        [~, dtSec, contData] = getInputData('trialdata', 1);            
    else
        dtSec = iterNum/handles.refreshRate;
        contData = zeros(numTotalChan,1000); % assuming 10Khz sampling rate
    end
    results.dtSec(iterNum) = dtSec;

    if iscell(contData)
        %we got data!
        if upsampleChannels
            %replicate data across total 128 channels (to speed up SUAG)

            suag_neuralData = [contData{1:numSUAGChan,3}]';
            raw_neuralData  = repmat(suag_neuralData, numTotalChan/numSUAGChan ,1);
            disp('upsamplin')
        else
%             raw_neuralData = [contData{1:numTotalChan,3}]';
            raw_neuralData = double(cell2mat(contData(1:numTotalChan,3)'))';
%             raw_analogData = double(cell2mat(contData(numTotalChan+1:end,3)'))';
        end

    elseif contData == -1
        %dropped SUAG packet
        results.loopTime(iterNum) = 0;
        iterNum = iterNum+1; %track # packets dropped
        continue;
    else
        raw_neuralData = contData;
%         raw_analogData = zeros(16,1000);
        % raw_neuralData = [contData{1:numChan,3}]';
        % TODO: define numchan (= numtotalchan?)
    end

    % ---- Visualize some of the data in real time ---- %
%     if ishandle(99) && ~isempty(findobj(99, 'Type', 'Line'))
%         pl = findobj(99, 'Type', 'Line');
%         set(pl, 'XData', 1:size(raw_neuralData, 2), 'YData', raw_neuralData(1, :));
%     else
%         figure(99);plot(raw_neuralData(1, :));title(num2str(t));
%     end

    %  Get raw Sensor DATA --------------
    %     raw_sensorData = [contData{129:end,3}]';

%     [row, numPoints] = size(raw_neuralData);


    % Low pass filter the signal before downsampling
%     lpf_neuralData = filtfilt(b,a,raw_neuralData);
% %     lpf_neuralData = raw_neuralData; % Commented by Nikunj 8-5-19

    %Downsample 30K data to 15K
%     if isequal(getInputData, @mockcbmex)
%         %don't downsample for mockcbmex/SUAG
%         downsampledND = lpf_neuralData';
%     else
%         downsampledND = downsample(lpf_neuralData', 2);
%     end
% %     downsampledND = lpf_neuralData'; % Commented by Nikunj 8-5-19

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%artifact rejection for blanking %%%
    if artifactRejection 
       downsampledND = getARneuralSignal(downsampledND, artifactRejectionFilter, handles);
    end

    %% Get hand sensor data - Added by Nikunj 6-3-2019    
    if handles.bendConnected 
        fprintf(handles.serialBend, '%c', 'r'); 
        BendSensorString = fgetl(handles.serialBend); 
        profile off
%         [BendVector, Count] = sscanf(BendSensorString, '%f, %f,%f, %f, %f, %f, %f, %f, %f');
        [BendVector, Count] = sscanf(BendSensorString, '%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f');
        benddata(iterNum,:) = BendVector';
    else
        profile off
    end
    
    %% Preprocess neural data
% %     processedND = getPreprocessNeuralSignal(downsampledND', handles); % Commented by Nikunj 8-5-19
    
    %populate data matrix
%     [~, colDS] = size(processedND.final);
%     numPoints = min(colDS, spc);
%     endIndex =  beginIndex + numPoints - 1;
%     if numPoints > (spc-1)
%         neuraldata(:,beginIndex:endIndex) = processedND.final(:, colDS-(spc-1):colDS);
%         labels(beginIndex:endIndex) = ones(1,(endIndex - beginIndex +1)).* currLabel;
%     else
%         neuraldata(:,beginIndex:endIndex) = processedND.final(:, :);
%         labels(beginIndex:endIndex) = ones(1,(endIndex - beginIndex +1)).* currLabel;
%     end
%     beginIndex = beginIndex + numPoints;
%     indexTracker(iterNum,:) = [beginIndex endIndex];

% %     [~, numPoints] = size(processedND.final); % Commented by Nikunj 8-5-19
    [~, numPoints] = size(raw_neuralData);   
    endIndex =  beginIndex + numPoints - 1;
% %     neuraldata(:,beginIndex:endIndex) = processedND.final; % Commented by Nikunj 8-5-19
    neuraldata(:,beginIndex:endIndex) = raw_neuralData;
    
%     [~, numPoints2] = size(raw_analogData);   
%     endIndex2 =  beginIndex2 + numPoints2 - 1;
%     analogIndata(:,beginIndex2:endIndex2) = raw_analogData;
    
    labels(beginIndex:endIndex) = ones(1,numPoints).* currLabel;
    if regressionTask
        reggLabels(beginIndex:endIndex) = ones(1,numPoints).* reggLabel;
    end
    indexTracker(iterNum,:) = [beginIndex endIndex];
            
    % TODO -- mess with this

    %check for suag data sending
%     if (beginIndex > 1)
%         if ~(neuraldata(1, beginIndex-1) ==  neuraldata(1, beginIndex)-1)
%             keyboard
%         end
%     end

    beginIndex = endIndex + 1;  
%     beginIndex2 = endIndex2 + 1;

    %% Neural Feature Extraction % Commented by Nikunj 8-5-19
% %     featTic = tic;
% %     neuralFeatures = getNeuralFeatures(processedND.final, handles);   %%%%%%%%%%%%% 
% %     timeFeat = toc(featTic);
% % 
% %     %populate feature matrix
% %     [~, colDS] = size(neuralFeatures.final);
% %     numPointsFeat = min(colDS, spc);
% %     %     endIndexFeat =  beginIndexFeat + numPointsFeat - 1;
% % 
% %     if numPointsFeat > (spc-1)
% %         waveletdata(:,iterNum) = neuralFeatures.final(:, colDS-(spc-1):colDS);
% %         labelWav(iterNum) = mode(labels(beginIndex:endIndex));
% %     else
% %         waveletdata(:,iterNum) = neuralFeatures.final(:, :);
% %         labelWav(iterNum) = mode(labels(beginIndex:endIndex));
% %     end
% %     if regressionTask
% %         reggLabelWav(iterNum) = mode(reggLabels(beginIndex:endIndex)); % Pseudo-discrete/continuous coding scheme
% %     %         reggLabelWav(iterNum) = mode(reggLabels(beginIndex:endIndex)); % Half-sine waves
% %     end
% %     %     indexTrackerWav(iterNum,:) = [beginIndexFeat endIndexFeat];

    %% Decode Neural Data
    if handles.blockInfo.closedLoop

        if any(strcmpi('BoxCAR', handles.pipeline))
            % Create buffers that look back in time and compute moving average at
            % every iteration of the loop
            minwinsize = min(iterNum, memoryvar); % endIndexFeat is the most latest point. Subtract 'memoryvar' samples from this number.
            if iterNum < minwinsize
                moving_avg = nanmean(waveletdata(:, 1:iterNum),2);
            else
                moving_avg = nanmean(waveletdata(:, iterNum-minwinsize+1:iterNum),2);
            end
            neuralFeatures.movavg = moving_avg;
            waveletdata_movavg{iterNum} = neuralFeatures.movavg; 
        end

        %Decode here
        if regressionTask
            decodedOut = RegressNeuralSignal(neuralFeatures, decoderMdl, handles);
            pcadata{iterNum} = decodedOut.pcadata;
            normWaveletdata{iterNum} = decodedOut.normalizedWavelets;
        else
            decodedOut = ClassifyNeuralSignal(neuralFeatures, decoderMdl, handles);
            pcadata{iterNum} = decodedOut.pcadata;
            normWaveletdata{iterNum} = decodedOut.normalizedWavelets;
        end
    end

    %% Postprocessing
    %take decode output and map to stimulation pattern, send to AO system
    %for stim output

    if closedLoop
        if regressionTask
            visualOut = postprocessUnityFeedbackRegress(decodedOut, u, currLabel);
            results.decodePrediction(iterNum) = decodedOut.predicted;
        else
            visualOut = postprocessUnityFeedback(decodedOut, u);
            results.decodePrediction(iterNum) = decodedOut.predicted;
        end
    end

    %     if sensoryStim
    %         s1out = postprocessSensoryStim(decodedOut, raw_sensorData);
    %     end
    %
    %send FES output to AO last due to delay?
    if nmstim
        nmstimOut = postprocessFESstim(decodedOut, handles);
        results.nmstim(iterNum) = nmstimOut;
    end
        
    %% Store loop Results

    %Save output variables
%     results.wavTime(iterNum) = neuralFeatures.timeWav;


%     beginIndexFeat = beginIndexFeat + numPointsFeat;
    iterNum = iterNum+1;
%     clear contData

    %clear buffer
%     [~, ~, ~] = getInputData('trialdata', 1);

    %Maintain consistent sampling rate
    endTimeLoop = toc(startTimeLoop);
    results.loopTime(iterNum) = endTimeLoop;    
    endLoopTime = (now() - startLoopTime) * 86400;
    results.loopTime2(iterNum) = endLoopTime;
    if endLoopTime > dt
        %already over on time so no pause
%         pause(dt)
    else
%         results.desireddelay(iterNum) = dt-endLoopTime;          
%         profile on;                   
        pause(dt-endLoopTime);
%         p_struct = profile('info');
        
%         if table_entry == 0
%             table_entry = 1;
%             for i = 1:length(p_struct.FunctionTable)
%                 if strcmp(p_struct.FunctionTable(i).FunctionName,'main_RealTimeLoop')
%                     table_entry = i;
%                     break;
%                 end
%             end            
%         end
%         display(p_struct.FunctionTable(table_entry).FunctionName);
%         if strcmp(p_struct.FunctionTable(table_entry).FunctionName,'main_RealTimeLoop')            
%             results.execTime(iterNum) = p_struct.FunctionTable(table_entry).TotalTime;
%         else
%             results.execTime(iterNum) = -1; % No match
%         end       
        
    end
    results.whileLoopTime(iterNum) = toc(startTimeLoop);
    results.whileLoopTime2(iterNum) = (now() - startLoopTime) * 86400;
    
end
set(handles.taskDisplayBox, 'Enable', 'on');

%% Store Results & relevant variables here

handles.sessionInfo.duration = toc(handles.sessionInfo.loopStartTime);
handles.blockInfo.resultsName = ['Block_' num2str(handles.sessionInfo.currentRow) '_results'];

%Save output variables of interest
results.neuralData      = neuraldata;
results.benddata        = benddata;
results.waveletData     = waveletdata;
results.labels          = labels;
results.waveltLabels    = labelWav;
results.indexTrackerRaw = indexTracker;
% results.indexTrackerWav = indexTrackerWav;
results.handles         = handles;

if regressionTask
    results.regressionLabels = reggLabels;
%     results.regressionLabelsWav = reggLabelWav; %Commented by Nikunj,
%     8-5-19
end


if closedLoop
    results.pcadata         = pcadata;
    results.waveletdata_mov = waveletdata_movavg;
    results.normWavelet     = normWaveletdata;
end


%% Clean up
%reset hand positions
resetmsg = generate_force_message(DestinationHand.CUE, single(0), Finger.NONE);           
fwrite(u, resetmsg)
resetmsg = generate_force_message(DestinationHand.RIGGED, single(0), Finger.NONE);  
fwrite(u, resetmsg)

%close socket
delete(hcleanup);
delete(u);

if handles.nspConnected
%stop buffering NSP data
getInputData('trialconfig', 0);
end
profile viewer

end

function UpdateAnimation_Callback(timer_object,eventdata,hObject)

    handles = guidata(hObject);
    global cueTimes cueLabel regressLabel cueNum u numitems runRTS regressionTask currLabel reggLabel UpdateAnimationTimer
    persistent NumCycles
    
    if isempty(NumCycles)
        NumCycles = 0;
    end
    
    if ((NumCycles >= cueTimes(cueNum)/handles.AnimationUpdateRate)  || cueNum == 1)
        NumCycles = 0;
        %generate udp message
        destination = DestinationHand.CUE; % always cue hand here
        labelToSend = (cueLabel{cueNum});

        if ~regressionTask
            currLabel = GenerateClassificationLabels(labelToSend, cueLabel, cueNum, destination, u);
            if handles.bendConnected
                fprintf(handles.serialBend,'%c',currLabel);
            end
            if handles.FocalStimConnected
                if (currLabel == 0)
                    fprintf(handles.serialFocalStim, '%s', [int2str(10) ' ' int2str(0)]); %Channel 1 with intensity 
                elseif (currLabel == 1)
                    fprintf(handles.serialFocalStim, '%s', [int2str(1) ' ' int2str(100)]); %Channel 1 with intensity 
                elseif (currLabel == 3)
                    fprintf(handles.serialFocalStim, '%s', [int2str(2) ' ' int2str(60)]); %Channel 2 with intensity 
                end
            end
        else
            [currLabel, reggLabel] = GenerateRegressionLabels(labelToSend, cueLabel, regressLabel, cueNum, destination, u);
            if handles.bendConnected
                fprintf(handles.serialBend,'%c',currLabel);
            end
            if handles.FocalStimConnected
                if (currLabel == 0)
                    fprintf(handles.serialFocalStim, '%s', [int2str(10) ' ' int2str(0)]); %Channel 1 with intensity 
                elseif (currLabel == 1)
                    fprintf(handles.serialFocalStim, '%s', [int2str(1) ' ' int2str(100)]); %Channel 1 with intensity 
                elseif (currLabel == 3)
                    stimlevel = int16(min(abs(regressLabel(cueNum))*100,100));
                    fprintf(handles.serialFocalStim, '%s', [int2str(1) ' ' int2str(stimlevel)]); %Channel 2 with intensity 
                end
            end
        end

        %increment index for next cue presentation
        set(handles.taskDisplayBox, 'Value', cueNum);
        cueNum2 = mod(cueNum, numitems);  % The last item should wrap around        
        cueNum = cueNum2 + 1;

        %When done with last cue, break from real time system
        if cueNum == 1 && cueNum2 == 0
            runRTS = false;
            stop(UpdateAnimationTimer);
        end

%         currTime = clock;
    else
        NumCycles = NumCycles + 1;
    end

    guidata(hObject, handles);
end
