function ARneuraldata = getARneuralSignal(neuraldata, artifactRejectionFilter, handles , testmode)
%% Placeholder for Artifact rejection algorithms

if nargin > 3
        handles.filterParameters.artifactRejection.extraction.channels = randperm(128, 26);
        handles.filterParameters.artifactRejection.extraction.threshold = 1000; % = 1 mV
end

switch lower(artifactRejectionFilter)
    case 'extraction'
        selectedChannels = handles.filterParameters.artifactRejection.extraction.channels;
        threshold = handles.filterParameters.artifactRejection.extraction.threshold ;
        
        channelsToTest      = neuraldata(selectedChannels, :);  % get selected channels (~20%)
        channelsAboveThresh = (channelsToTest > threshold | channelsToTest < -threshold);     % find where data is above threshold & below threshold

        
        %TODO: placeholder to ignore broken channels, > instead have a list
        %of broken channels and don't look at those!
        totalIndex4Channels = sum(channelsAboveThresh,2) ;

        brokenChannels = (totalIndex4Channels ./ size(neuraldata,2)) > .25 ; %channel is above threshold 25% of time, its broken
        channelsAboveThresh_adjusted = channelsAboveThresh( ~brokenChannels,: ); %good channels above threshold
        indexAboveThresh    =  sum(channelsAboveThresh_adjusted,1) >=1 ; % count # of indices above threshold 
        
        %remove those indices above threshold / keep data below threshold and reconnect data stream
        if any(indexAboveThresh)
            ARneuraldata = neuraldata(:,~indexAboveThresh); 
        end
        
        
    otherwise 
        fprintf('no Artifact Rejection Method found\n')
end



end