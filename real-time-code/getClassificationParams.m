function [minleafsize,numtrees] = getClassificationParams(KTrainF,KTrainL,cost)
% This function returns the optimal parameters for the classification
% ensemble.
% leafsize - optimal number of instances in a leaf node of each tree
% numtrees - optimal number of trees in the ensemble
% KTrainF, KTrainL - output parameters returned by function "getPNIdata"
% Ensemble information: This function trains a bagging classification
% ensemble. 

leaf = [5 10 50 100];
weight = ones(1,length(KTrainL));
weight(KTrainL == 1 | KTrainL == 2) = 10;
clc,sprintf('Estimating optimal parameters for the classification ensemble')
if nargin < 3
    cost = [];
end
CVmdl = zeros(length(leaf),100);
for kk=1:numel(leaf)
    t = templateTree('MinLeafSize',leaf(kk));
    Mdl = fitensemble(KTrainF,KTrainL,'Bag',100,t,'type','classification','Prior','Empirical','kfold',3,'Cost',cost);
    CVmdl(kk,:) = kfoldLoss(Mdl,'mode','cumulative');
end

% Get minimum CV error
CVmdl(isnan(CVmdl)) = 1;
serr = std(CVmdl(~isnan(CVmdl)))/sqrt(numel(CVmdl(~isnan(CVmdl))));
[minloss, ~] = min(CVmdl(~isnan(CVmdl)));
% One-standard error rule
onestderr = minloss + serr;
minmat = abs(CVmdl-onestderr);
[~, onestdind] = min(minmat(:));
% Optimal leaf size and no. of trees
[r,numtrees] = ind2sub(size(CVmdl),onestdind);
minleafsize = leaf(r);
