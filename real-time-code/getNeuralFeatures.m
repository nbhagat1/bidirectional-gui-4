function [results] = getNeuralFeatures(neuralData, handles)
%{
This function extracts the neural features from the processed signal.

Input: processed neural signal

Output: neural features

By: Johnny Ciancibello 10.13.2017
%}


[row, col] = size(neuralData);

%% Neural Feature Extraction Here 
%various wavelet transform techniques 
wavname = 'db3';
level = 6;

% Todd's wavedec_fast
% [Lo_D,Hi_D] = wfilters(wavname,'d');
% l = get_wavedec_l(neuralData(1,:), levelA, Lo_D, Hi_D);
% temp = wavedec_fast(neuralData(1,:), levelA, Lo_D,Hi_D,l); 
% cA = zeros(row, sum(l(1:end-1))); 
% 
% parfor i=1:row
%     %level A for wavelet decomp
%     cA(i,:) = wavedec_fast(neuralData(i,:), levelA,  Lo_D,Hi_D,l);
%     
% end

% Utilizing Rice wavelet toolbox
[h, ~] = daubcqf(4,'min');

%Must ensure length of neural data is divisible by 2^level
% colvec = 1:col;
% DivisibleColVev     = rem(colvec , 2^level);
% lastIndDivisible    = find(DivisibleColVev==0,1, 'last');
lastIndDivisible = col - mod(col, 2^level);
newNeuralData    = neuralData(:,1:lastIndDivisible);
% [cB, cC] = deal(zeros(row, lastIndDivisible));
cB = zeros(row, lastIndDivisible);
if isempty(lastIndDivisible)
    fprintf('Didn''t find divisible length of neural data for wavelet level: Wavelet values are all ZERO \n')
end

tStart = tic;
for i = 1:row
    cB(i, :) = mdwt(newNeuralData(i, :), h, level);
    % [cB(i, :), L] = mdwt(newNeuralData(i, :), h, level);
    % [cC(i, :), L] = mdwt(newNeuralData(i, :), h, 3);
end
% [cA, cC] = deal(cB);
% cA(:, lastIndDivisible / 2^3 + 1:end) = 0;
% cC(:, 1:lastIndDivisible / 2^3) = 0;
% cA = cB - cC; % compute levels 4-6 (and inverts the signal)

% Why does cB contain NaN's?
% The code after this function assumes that cA is the same size as the data
% cA = reshape(repmat(permute(cB(:, 1:lastIndDivisible / 2^3), [3 2 1]), [2^3 1]), [], size(cB, 1))';
cA = sum(cB(:, lastIndDivisible / 2^6 + 1:lastIndDivisible / 2^4), 2);
% lev4 = sum(cB(:, lastIndDivisible / 2^4 + 1:lastIndDivisible / 2^3 - 1), 2);
tt1 = toc(tStart);


% power within the bandpass filtered signal
%MUA = sum(sqrt(neuralData.^2),2); % this is the proper format, but so
%labels and data align keep original size
% MUA = sqrt(neuralData.^2);



%%  store results
results.timeWav = tt1;
% results.wavdecompL4to6 = cA;
% results.wavdecompL1to3 = cC;
% results.wavedecompL1to6 = cB;
results.final = cA;

