function [ND, reg_cues, classif_cues,baseline, featrank] = genDecodingData(X,Y,numfeats,task)
% This function can be used to generate the training and testing data for
% neural bypass decoding algorithms
% Inputs:
% X - Neural data
% Y - cue_refresh_shifted
% numfeats - Number of features as ranked by Mutual Information
% Maximization
% Outputs:
% ND - Neural data
% reg_cues - target regression labels
% classif_cues - target classification labels
% Subash Padmanaban 09/14/17

%% Generate regression labels
switch task
    case 'regression'
        % Replace square waves with sine waves for regression cues
        idx_cue = find(diff(Y) > 10) + 1;
        reg_cues = Y'; % Create a dummy matrix to modify square waves
        % to sine waves
        t = 0:1/24:1;
        for jj=1:numel(idx_cue)
            reg_cues(idx_cue(jj):idx_cue(jj)+24) = 98*sin(2*pi*0.5*t) + 3; % Account for
            % offset of 3 and increase the amplitude to match 101.
        end
        reg_cues(reg_cues == 3 | reg_cues == 2 | reg_cues == 5) = 0;
        % Rank features for regression
        % Feature selection for regression task
        % Obtain neural activity during movement only periods
        % Subtract rest period neural activity from other periods
        classif_cues = Y';
        classif_cues(classif_cues == 2 | classif_cues == 3 | classif_cues == 5)= 0; % Rest cues
        classif_cues(classif_cues == 101) = 1; % Hand open
        classif_cues(classif_cues == 102) = 2; % Hand close
        
        baseline = median(X(classif_cues == 0,:)); % Get baseline activity for all channels
        numactive = sum(classif_cues ~= 0);
        X(classif_cues ~= 0,:) = X(classif_cues ~=0,:) - repmat(baseline,[numactive 1]); % Subtract neural activity during rest period 
        % prior to feature selection
        featrank = feast('mim',96,X,reg_cues);
        ND = X(:,featrank(1:numfeats));
%         baseline = sum(X(classif_cues == 0,:)); % Get baseline activity for all channels
%         X(classif_cues ~= 0,:) = X(classif_cues ~=0,:) - repmat(baseline,[numactive 1]); % Subtract neural activity during rest period 
%         ND = X(:,regress_rank(1:numfeats));
        
        %% Generate classification labels
    case 'classification'
        classif_cues = Y';
        classif_cues(classif_cues == 2 | classif_cues == 3 | classif_cues == 5)= 0; % Rest cues
        classif_cues(classif_cues == 101) = 1; % Hand open
        classif_cues(classif_cues == 102) = 2; % Hand close
        baseline = median(X(classif_cues == 0,:)); % Get baseline activity for all channels
        numactive = sum(classif_cues ~= 0);
        X(classif_cues ~= 0,:) = X(classif_cues ~=0,:) - repmat(baseline,[numactive 1]); % Subtract neural activity during rest period 
        
        % Feature selection for classification task
        % Obtain neural activity during movement only periods
        % Subtract rest period neural activity from other periods
        
        featrank = feast('mim',96,X,classif_cues);
        
        ND = X(:,featrank(1:numfeats)); 
        
        reg_cues = [];
end
