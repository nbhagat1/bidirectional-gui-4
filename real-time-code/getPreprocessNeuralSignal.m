function processedND = getPreprocessNeuralSignal(raw_neuralData, handles)
%{
This is the preprocessing step of the downsamples voltage in the neural signal pipeline.

Input: raw Neural Signal, 

Output: processed Neural data as a structure

By Johnny Ciancibello, Sept. 29, 2017
%}

[row, col] = size(raw_neuralData);

validMethods = lower({'CAR', 'movingMean', 'Zscore', 'IIRfilter'}); %order matters for FOR loop below
signalProcessingChain = lower(handles.pipeline);

%% Main Math
theseMethods = validMethods(ismember(validMethods, signalProcessingChain));

outputPrepro = [];
for i = 1:length(theseMethods)
    thisMethod = theseMethods{i};
    switch lower(thisMethod)
        case 'car'
            %simple CAR: NOTE: get gloabl mu & std offline when training
            mu = 0;
            std = 1;
            outputPrepro = (raw_neuralData - mu) ./std;
            processedND.CAR_ND = outputPrepro;
        case 'zscore'
            % Z score w.r.t. each channel
            
            outputPrepro = zscore(raw_neuralData, 1);
            
        case 'iirfilter'
            %IIR filter (non-causal) NOTE > This won't work unless track and store
            %history of neural signal
            N = 6; %num time steps
            b = ones(1,N)./N;
            a = 1;
            outputPrepro = filtfilt(a,b,raw_neuralData);
            
            processedND.filtered_ND = outputPrepro;
        case 'movingmean'
            %Sliding window
            bkwds = 5;
            fwd = 0;
            if row > 1
                outputPrepro = movmean(raw_neuralData, [bkwds fwd], 2);
            else
                outputPrepro = movmean(raw_neuralData, [bkwds fwd]);
            end
            processedND.movingAvg_ND = outputPrepro;
    end
    
    raw_neuralData = outputPrepro;
end

%% Store results

if isempty(outputPrepro)
    processedND.final = raw_neuralData;
else
    processedND.final = outputPrepro;
end

end