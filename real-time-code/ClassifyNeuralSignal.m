function decodedOut = ClassifyNeuralSignal(neuralstruct, decoderMdl, handles)
%{
Here are all the main decoding functions that will be called from the main
loop

Input: neuralSignal - input neural signal
       labels - Discrete values for class labels. Use this information to
       subtract baseline activity from movement period
       Encoding of labels: 
    0 = rest
    1 = thumb
    2 = index
    3 = middle
    4 = ring
    5 = pinky
    6 = open
    7 = close
    8 = pinch
    9 = power 
       decoderMdl - Type of decoder

Output: decoded predictions & other saved results

by Johnny Ciancibello, Sept. 29, 2017
%}

%% Decode

neuraldata = neuralstruct.final;

rawdata = neuralstruct.movavg;
X_train = rawdata; %nanmean(rawdata,2);

% Use offline values to subtract baseline mean
basemean = handles.filtInfo.meanbaseline{handles.blockInfo.decoderSelectedNumber};
X_train = X_train - basemean;

% Scale data based on values learned offline
minval = handles.filtInfo.minval{handles.blockInfo.decoderSelectedNumber};
datarange = handles.filtInfo.range{handles.blockInfo.decoderSelectedNumber};

ndata = X_train - minval;
ndata = ndata/datarange;
ndata = ndata + 0;

% pcadata = handles.filtInfo.pcatransformation' * ndata;
pcadata = ndata;

switch lower(handles.filtInfo.filterName)
    case 'baggedensemble'
         decodedOut.predicted = predict(decoderMdl, pcadata');
    case 'boostedregressiontree'
         decodedOut.predicted = predict(decoderMdl, pcadata');
    case 'svm'
         decodedOut.predicted = predict(decoderMdl, pcadata');
    case 'volterra'
          ;
end

%% Store results
decodedOut.neuraldata = neuraldata;
decodedOut.pcadata = pcadata;
decodedOut.normalizedWavelets = ndata;
% decodedOut.movement = 1;
% decodedOut.force    = 'medium';