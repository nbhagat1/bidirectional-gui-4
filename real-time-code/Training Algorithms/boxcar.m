function out = boxcar(data, N)
% Boxcar filter of N (points) width
% C. Bouton

out = filter(ones(1,N)/N, 1 ,data);

