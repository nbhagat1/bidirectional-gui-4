function [finalMdl, filtInfo]  = trainSVM(trainingData, handles)
% Train classification SVM for neural decoding
% Unpack structure to concatenate neural data
[neuralData, labels, waveletData, wavlabels] = deal([]);
for jj=1:size(trainingData,2)
    neuralData = horzcat(neuralData, trainingData(jj).selblocks.neuralData);
    labels = horzcat(labels, trainingData(jj).selblocks.labels);
    waveletData = horzcat(waveletData, trainingData(jj).selblocks.waveletData);
    wavlabels = horzcat(wavlabels, trainingData(jj).selblocks.waveltLabels);
end

% Truncate neural data
nanlabels = find(isnan(labels));
labels(nanlabels) = [];
neuralData(:,nanlabels) = [];
labels = labels(1:size(neuralData,2));

% Truncate wavelet data
nanwavlabels = find(isnan(wavlabels));
wavlabels(nanwavlabels) = [];
waveletData(:, nanwavlabels) = [];
wavlabels = wavlabels(1:size(waveletData,2));

%account for reaction time
reactionTime = 0.6;% sec
handles.refreshRateSec = .2;
K = ceil(reactionTime / handles.refreshRateSec);
% wavlabels_shifted = circshift(wavlabels,K);
wavlabels_shifted = [zeros(1,K) wavlabels(1:end-K)];

% Wavelet transform (This should be done in the real-time loop, not here)
% [results] = getNeuralFeatures(neuralData, handles);

% Boxcar average the processed neural data
% refrate = 10;
% Nbox = handles.sr/refrate; % 1500 because 100ms at 15k is 1500 samples. 
% nearestmultiple = floor(size(waveletData, 2)/ Nbox);
% X_train = squeeze(nanmean(reshape(waveletData(:,1:nearestmultiple*Nbox), [size(waveletData,1) Nbox nearestmultiple]), 2)); 
% X_train = waveletData(:,1:nearestmultiple);
X_train = movmean(waveletData, 5);

% Pre-process labels
% Y_train = labels(1:Nbox:nearestmultiple*Nbox);
% Y_train = labels(1:Nbox:nearestmultiple*Nbox);
Y_train = wavlabels_shifted;

% Get "movement only" neural features (subtract baseline activity)
filtInfo.meanbaseline = nanmean(X_train(:, Y_train == 0),2);
filtInfo.stdbaseline = nanstd(X_train(:, Y_train == 0),[],2);

% Subtract baseline from neural data
X_train = X_train - filtInfo.meanbaseline;

% preprocess raw neural data
filtInfo.minval = nanmin(X_train(:));
filtInfo.range = prctile(X_train(:), 90) - prctile(X_train(:), 10);%range(X_train(:));% 
neuraldata = scaledata(X_train, 0, 1, filtInfo.range, filtInfo.minval);
% [coeff, pcadata] = pca(neuraldata');
pcadata = neuraldata';

% Find optimal boxconstraint and rbf-sigma
posvals = [0.001 0.01 0.1 1 10 100];
boxc = [0.001 0.01 0.1 1 10 100];
for ii=1:numel(posvals)
    for jj=1:numel(posvals)
        t = templateSVM('BoxConstraint',boxc(ii),'KernelFunction','rbf',...
            'KernelScale',posvals(jj));
        Mdl = fitcecoc(pcadata, Y_train', 'Learners',t);
        CVMdl = crossval(Mdl);
        oosloss(ii,jj) = kfoldLoss(CVMdl);
    end
end

% Find optimal values of boxc and sigma
[~,minind] = min(oosloss(:));
% [optboxc, optsigma] = ind2sub([6,6], minind); 
optboxc = 10;
optsigma = 10;
finalt = templateSVM('BoxConstraint',optboxc,'KernelFunction','rbf',...
            'KernelScale',optsigma);
wtvec = ones(1,length(Y_train));
wtvec(Y_train == 0) = 1.25;
finalMdl = fitcecoc(pcadata,Y_train', 'Learners',finalt,'Weights',wtvec);
        
figure;
subplot(2,1,1)
imagesc(pcadata')
subplot(2,1,2)
plot(wavlabels_shifted)

filtInfo.pcadata    = pcadata;
filtInfo.labels     = Y_train;
filtInfo.filterName = handles.filterBuild.filterChoice;
filtInfo.filterType = 'decoder'; 
filtInfo.trainingBlocks = handles.filterBuild.trainingBlocks;
filtInfo.featureCount = 128;
filtInfo.sensorFeedback = false;
filtInfo.startTime = clock; 
filtInfo.decoderName = 'svm';
% filtInfo.pcatransformation = coeff;