function [numtrees, lambda] = optimizeRegressBoostEnsemble(pcadata,Y_train)
% Find optimal hyperparameters for number of trees and regularization
% factor 'lambda'

clc,disp('Estimating optimal parameters for the regression ensemble')
Mdl = fitensemble(pcadata, Y_train','LSBoost',100,'Tree','kfold',3);
CVerr = kfoldLoss(Mdl,'mode','cumulative');

% Optimal number of trees
serr = std(CVerr(~isnan(CVerr)))/sqrt(numel(CVerr(~isnan(CVerr))));
[minloss, ~] = min(CVerr(~isnan(CVerr)));
onestderr = minloss + serr;
[~, numtrees] = min(abs(CVerr-onestderr));
Mdl = fitensemble(pcadata,Y_train','LSBoost',numtrees,'Tree');

% Optimal lambda
lambda_max = 0.5;
lambdas = [0 logspace(log10(lambda_max/1000),log10(lambda_max),9)];
ls = regularize(Mdl,'lambda',lambdas);
[mse,~] = cvshrink(ls,'Lambda',ls.Regularization.Lambda,'KFold',5);
mse(isnan(mse)) = 1;
serr = std(mse(~isnan(mse)))/sqrt(10);
[minloss, ~] = min(mse(~isnan(mse)));
onestderr = minloss + serr;
[~, lambda] = min(abs(mse-onestderr));

