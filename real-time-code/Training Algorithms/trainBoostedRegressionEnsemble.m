
function  [regMdl, filtInfo]  = trainBoostedRegressionEnsemble(trainingData, handles)


%% Train Bagged Ensemble Decoder 
[neuralData, labels, waveletData, wavlabels, regressionLabels, regressionWavLabels] = deal([]);
for jj=1:size(trainingData,2)
    neuralData = horzcat(neuralData, trainingData(jj).selblocks.neuralData);
    labels = horzcat(labels, trainingData(jj).selblocks.labels);
    regressionLabels = horzcat(regressionLabels, trainingData(jj).selblocks.regressionLabels);
    
    waveletData = horzcat(waveletData, trainingData(jj).selblocks.waveletData);
    wavlabels = horzcat(wavlabels, trainingData(jj).selblocks.waveltLabels);
    regressionWavLabels = horzcat(regressionWavLabels, trainingData(jj).selblocks.regressionLabelsWav);
end
  
% Truncate neural data
nanlabels = find(isnan(labels));
labels(nanlabels) = [];
nanreglabels = find(isnan(regressionLabels));
regressionLabels(nanreglabels) = [];
neuralData(:,nanlabels) = [];
labels = labels(1:size(neuralData,2));
regressionLabels = regressionLabels(1:size(neuralData,2));

% Truncate wavelet data
nanwavlabels = find(isnan(wavlabels));
wavlabels(nanwavlabels) = [];
waveletData(:, nanwavlabels) = [];

nanregwavlabels = find(isnan(regressionWavLabels));
regressionWavLabels(nanregwavlabels) = [];
wavlabels = wavlabels(1:size(waveletData,2));
regressionWavLabels = regressionWavLabels(1:size(waveletData,2));

%account for reaction time
reactionTime = 0.6;% sec
handles.refreshRateSec = .2;
K = ceil(reactionTime / handles.refreshRateSec);
% wavlabels_shifted = circshift(wavlabels,K);
wavlabels_shifted = [zeros(1,K) regressionWavLabels(1:end-K)];

% Wavelet transform (This should be done in the real-time loop, not here)
% [results] = getNeuralFeatures(neuralData, handles);

% Boxcar average the processed neural data
% refrate = 10;
% Nbox = handles.sr/refrate; % 1500 because 100ms at 15k is 1500 samples. 
% nearestmultiple = floor(size(waveletData, 2)/ Nbox);
% X_train = squeeze(nanmean(reshape(waveletData(:,1:nearestmultiple*Nbox), [size(waveletData,1) Nbox nearestmultiple]), 2)); 
% X_train = waveletData(:,1:nearestmultiple);
X_train = movmean(waveletData, 5);

% Pre-process labels
% Y_train = labels(1:Nbox:nearestmultiple*Nbox);
% Y_train = labels(1:Nbox:nearestmultiple*Nbox);
Y_train = wavlabels_shifted;

% Get "movement only" neural features (subtract baseline activity)
filtInfo.meanbaseline = nanmean(X_train(:, Y_train == 0),2);
filtInfo.stdbaseline = nanstd(X_train(:, Y_train == 0),[],2);

% Subtract baseline from neural data
% X_train = X_train - filtInfo.meanbaseline;

% preprocess raw neural data
filtInfo.minval = nanmin(X_train(:));
filtInfo.range = prctile(X_train(:), 90) - prctile(X_train(:), 10);%range(X_train(:));% 
neuraldata = scaledata(X_train, 0, 1, filtInfo.range, filtInfo.minval);
% [coeff, pcadata] = pca(neuraldata');
pcadata = neuraldata';

% Post-process the training cues to ramp/sine function from step function
labeltransitions = diff([0 wavlabels]);
labelonset = find(labeltransitions > 0)+1;
labeloffset = find(labeltransitions < 0);
sequences(1:2:(numel(labelonset)*2)) = labelonset;
sequences(2:2:(numel(labeloffset)*2)) = labeloffset;
sequences2 = diff([0, sequences, numel(wavlabels)]);

sequenceslabels = mat2cell(wavlabels, 1, sequences2);
sequencesforces = mat2cell(Y_train, 1, sequences2);
for jj=1:numel(sequenceslabels)
    if round(mode(sequenceslabels{1,jj})) == 0
       xs2{jj} = sequencesforces{1,jj};
       
    else
       maxamp = max(sequencesforces{1,jj});
       
       x = linspace(0,pi, numel(sequencesforces{1,jj}));
       xs2{jj} = maxamp.*sin(x);
       
    end
end
sin_Y_train = horzcat(xs2{:});
% 
% figure;hold on;
% plot(wavlabels)
% plot(labelonset, wavlabels(labelonset), 'r*')
% plot(labeloffset, wavlabels(labeloffset), 'b*')
% legend('label', 'onset', 'offset')

% 
% figure; hold on;
% plot(sin_Y_train, 'b')
% plot(sin_Y_train2, 'r')
% plot(Y_train, 'k')

% Build the training model
Mdl = fitensemble(pcadata, sin_Y_train','LSBoost',100,'Tree','kfold',3);
CVerr = kfoldLoss(Mdl,'mode','cumulative');

% Optimal number of trees
serr = std(CVerr(~isnan(CVerr)))/sqrt(numel(CVerr(~isnan(CVerr))));
[minloss, ~] = min(CVerr(~isnan(CVerr)));
onestderr = minloss + serr;
[~, numtrees] = min(abs(CVerr-onestderr));
Mdl = fitensemble(pcadata, sin_Y_train','LSBoost',numtrees,'Tree');

% Optimal lambda
lambda_max = 0.5;
lambdas = [0 logspace(log10(lambda_max/1000),log10(lambda_max),9)];
ls = regularize(Mdl,'lambda',lambdas);
[mse,~] = cvshrink(ls,'Lambda',ls.Regularization.Lambda,'KFold',5);
mse(isnan(mse)) = 1;
serr = std(mse(~isnan(mse)))/sqrt(10);
[minloss, ~] = min(mse(~isnan(mse)));
onestderr = minloss + serr;
[~, lambda] = min(abs(mse-onestderr));

Mdl = fitensemble(pcadata, sin_Y_train','LSBoost',numtrees,'Tree');
lambda_max = 0.5;
lambdas = [0 logspace(log10(lambda_max/1000),log10(lambda_max),9)];
ls = regularize(Mdl,'lambda',lambdas);
regMdl = shrink(ls,'weightcolumn',lambda);

figure;
h(1) = subplot(2,1,1);
imagesc(pcadata')
h(2) = subplot(2,1,2); hold on;
plot(wavlabels_shifted)
plot(sin_Y_train, 'r')
linkaxes(h, 'x')

figure;imagesc(diff(neuralData))

% Final training parameters for GUI filter table
filtInfo.filterName = handles.filterBuild.filterChoice;
filtInfo.filterType = 'regression'; 
filtInfo.trainingBlocks = handles.filterBuild.trainingBlocks;
filtInfo.featureCount = 22;
filtInfo.sensorFeedback = false;
filtInfo.startTime = clock; 
filtInfo.decoderName = 'boostedregressionensemble';
% filtInfo.pcatransformation = coeff;


