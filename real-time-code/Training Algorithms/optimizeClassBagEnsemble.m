function [minleafsize,numtrees] = optimizeClassBagEnsemble(neuraldata,labels)
% Optimize hyperparameters for classification bagged ensemble

leaf = [5 10 50 100];
% sprintf('Estimating optimal parameters for the classification ensemble')
CVmdl = zeros(length(leaf),100);
for kk=1:numel(leaf)
    t = templateTree('MinLeafSize',leaf(kk));
    Mdl = fitensemble(neuraldata',labels','Bag',100,t,'type','classification','Prior','Empirical','kfold',3);
    CVmdl(kk,:) = kfoldLoss(Mdl,'mode','cumulative');
end

% Get minimum CV error
CVmdl(isnan(CVmdl)) = 1;
serr = std(CVmdl(~isnan(CVmdl)))/sqrt(numel(CVmdl(~isnan(CVmdl))));
[minloss, ~] = min(CVmdl(~isnan(CVmdl)));
% One-standard error rule
onestderr = minloss + serr;
minmat = abs(CVmdl-onestderr);
[~, onestdind] = min(minmat(:));
% Optimal leaf size and no. of trees
[r,numtrees] = ind2sub(size(CVmdl),onestdind);
minleafsize = leaf(r);
