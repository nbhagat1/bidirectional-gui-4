function output = meansqerr(X,y)
% If X and y are multidimensional, M x N :
% M - number of kinematic variables (RU and FE)
% N - number of data points

output = sum((y-X).^2,2)./size(X,2);
