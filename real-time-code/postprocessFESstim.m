function nmStimOut = postprocessFESstim(decodedOut, handles)
%{
Translate decoded out to FES patterns. This function references the loaded
outcomes in the Bidirectional GUI from our stimmapping GUI.

TODO: This function must be robust to regression and classification

Classification Movement Labels:
0 = rest
1 = thumb
2 = index
3 = middle
4 = ring
5 = pinky
6 = open
7 = close
8 = pinch
9 = power

Created by JGC 3.22.18
%}

%% Declare Variables
%FES stim set up
FirstPhaseDelay_mS  = 0;   %the delay of the first phase
FirstPhaseWidth_mS  = 0.5; %the width of the first phase
SecondPhaseDelay_mS = 0; %the delay of the second phase
SecondPhaseWidth_mS = FirstPhaseWidth_mS; %the width of the second phase
Freq_hZ             = 50;  %the frequency of the stimulation
duration            = 1;
returnChannel      = -1;
baseline_amp       = 1;

%get info from GUI Handles
movementLabels = handles.FESinfo.movementLabels;
FEStemplate    = handles.FESinfo.template;


%translate decode out to required stimulation pattern
switch decodedOut.predicted
    case 0
        %stop all channels upon rest decode
        stimulationChannel = [FEStemplate{:,4}] +10255;
        reciprocalChannel  = [FEStemplate{:,5}] +10255;
        stimAMP            = 0;
        
    case 1
        %find desired movement
        thisMovement = strcmpi('thumb flexion',  movementLabels);
        stimulationChannel = [FEStemplate{thisMovement,4}] +10255;
        reciprocalChannel  = [FEStemplate{thisMovement,5}] +10255;
        stimAMP            = [FEStemplate{thisMovement,2}]; %assume movement threshold for now
        
    case 2
        %find desired movement
        thisMovement = strcmpi('index flexion',  movementLabels);
        stimulationChannel = [FEStemplate{thisMovement,4}] +10255;
        reciprocalChannel  = [FEStemplate{thisMovement,5}] +10255;
        stimAMP            = [FEStemplate{thisMovement,2}]; %assume movement threshold for now
    case 3
        %find desired movement
        thisMovement = strcmpi('middle flexion',  movementLabels);
        stimulationChannel = [FEStemplate{thisMovement,4}] +10255;
        reciprocalChannel  = [FEStemplate{thisMovement,5}] +10255;
        stimAMP            = [FEStemplate{thisMovement,2}]; %assume movement threshold for now
        
    case 4
        %find desired movement
        thisMovement = strcmpi('ring flexion',  movementLabels);
        stimulationChannel = [FEStemplate{thisMovement,4}] +10255;
        reciprocalChannel  = [FEStemplate{thisMovement,5}] +10255;
        stimAMP            = [FEStemplate{thisMovement,2}]; %assume movement threshold for now
        
    case 5
        %find desired movement
        thisMovement = strcmpi('pinky flexion',  movementLabels);
        stimulationChannel = [FEStemplate{thisMovement,4}] +10255;
        reciprocalChannel  = [FEStemplate{thisMovement,5}] +10255;
        stimAMP            = [FEStemplate{thisMovement,2}]; %assume movement threshold for now
        
    case 6
        %find desired movement
        thisMovement = strcmpi('open',  movementLabels);
        stimulationChannel = [FEStemplate{thisMovement,4}] +10255;
        reciprocalChannel  = [FEStemplate{thisMovement,5}] +10255;
        stimAMP            = [FEStemplate{thisMovement,2}]; %assume movement threshold for now
        
    case 7
        %find desired movement
        thisMovement = strcmpi('close',  movementLabels);
        stimulationChannel = [FEStemplate{thisMovement,4}] +10255;
        reciprocalChannel  = [FEStemplate{thisMovement,5}] +10255;
        stimAMP            = [FEStemplate{thisMovement,2}]; %assume movement threshold for now
        
    case 8
        %find desired movement
        thisMovement = strcmpi('pinch',  movementLabels);
        stimulationChannel = [FEStemplate{thisMovement,4}] +10255;
        reciprocalChannel  = [FEStemplate{thisMovement,5}] +10255;
        stimAMP            = [FEStemplate{thisMovement,2}]; %assume movement threshold for now
        
    case 9
        %find desired movement
        thisMovement = strcmpi('power',  movementLabels);
        stimulationChannel = [FEStemplate{thisMovement,4}] +10255;
        reciprocalChannel  = [FEStemplate{thisMovement,5}] +10255;
        stimAMP            = [FEStemplate{thisMovement,2}]; %assume movement threshold for now
        
    otherwise
        %is this necessary bc rest is case 0...
end

%prepare to send parameters to AO system
if decodedOut.predicted ~= 0
    allStimChannels    = [stimulationChannel reciprocalChannel];
    
    if isempty(allStimChannels)
        fprintf('decoded movement does not have a matched FES movement: NO Stim sent to AO \n')
        nmStimOut = false;
    else
        % send stim params & stimulate!
        
        AO_StartDigitalStimulation(stimulationChannel, FirstPhaseDelay_mS, -stimAMP, FirstPhaseWidth_mS, SecondPhaseDelay_mS, stimAMP, SecondPhaseWidth_mS, Freq_hZ, duration, returnChannel)
        AO_StartDigitalStimulation(reciprocalChannel, FirstPhaseDelay_mS, stimAMP, FirstPhaseWidth_mS, SecondPhaseDelay_mS, -stimAMP, SecondPhaseWidth_mS, Freq_hZ, duration, returnChannel)
        
        nmStimOut = true;
        
        %          %old AO command for reference
        %         [~]   = AO_SetStimulationParameters(FirstPhaseDelay_mS,   -stimAMP, FirstPhaseWidth_mS,...
        %             SecondPhaseDelay_mS,  stimAMP, SecondPhaseWidth_mS,...
        %             Freq_hZ, duration,  returnChannel, stimulationChannel);
        %         pause(.007)
        %         [~]   = AO_SetStimulationParameters(FirstPhaseDelay_mS,   stimAMP, FirstPhaseWidth_mS,...
        %             SecondPhaseDelay_mS,  -stimAMP, SecondPhaseWidth_mS,...
        %             Freq_hZ, duration,  returnChannel, reciprocalChannel);
        %         pause(.007)
        %
        %         %stim!
        %         for jj = 1:length(allStimChannels)
        %             [~] = AO_StartStimulation(allStimChannels(jj));
        %         end
        
    end
else
    
    allStimChannels    = [stimulationChannel reciprocalChannel];
    
    for jj = 1:length(allStimChannels)
        [~] = AO_StopStimulation(allStimChannels(jj));
    end
    
    nmStimOut = true;
    
end




