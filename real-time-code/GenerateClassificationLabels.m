function  currLabel = GenerateClassificationLabels(labelToSend, cueLabel, cueNum, destination, u)


        switch lower(labelToSend)
            %all functional movements here
            case 'delay'
                force = single(0);
                grasp = Finger.(upper(labelToSend));
                isFingerLabel = false;
            case {'key' , 'pinch'} %TODO: these should be different
                force = single(-1);
                grasp = Finger.(upper(labelToSend));
                isFingerLabel = false;
            case 'power'
                force = single(-1);
                grasp = Finger.(upper(labelToSend));
                isFingerLabel = false;
            case 'open'
                force = single(1);
                grasp = Finger.(upper(labelToSend));
                isFingerLabel = false;
            case 'close'
                force = single(-1);
                grasp = Finger.(upper(labelToSend));
                isFingerLabel = false;
                
            case 'wristflexion'
                force = single(-1);
                grasp = Finger.WRIST;
                isFingerLabel = false;
                
            case 'wristextension'
                force = single(1);
                grasp = Finger.WRIST;
                isFingerLabel = false;
                
            case 'wristpronation'       %palm down
                force = single(1);
                grasp = Finger.FOREARM;
                isFingerLabel = false;
                
            case 'wristsupination'      %palm up
                force = single(-1);
                grasp = Finger.FOREARM;
                isFingerLabel = false;
                
            otherwise
                %all finger movement direction determined here
                isFingerLabel = true;
                flxInd = strfind(labelToSend, 'Flexion');
                extInd = strfind(labelToSend, 'Extension');
                if any(flxInd) && isempty(extInd)
                    force = single(-1);
                    fingerLabel = labelToSend(1:flxInd-1);
                    grasp = Finger.(upper(fingerLabel));
                elseif any(extInd) && isempty(flxInd)
                    force = single(1);
                    fingerLabel = labelToSend(1:extInd-1);
                    grasp = Finger.(upper(fingerLabel));
                else
                    %TODO: place holder for wrist movements
                    fprintf('cue %s not recognized\n', labelToSend)
                end
        end
        
        fullmsg = generate_force_message(destination, force, grasp);
        
        %send a udp msg
        fwrite(u, fullmsg)
        
%         %print to confirm
% %         fprintf('sending grasp %s\n', grasp)
        
        %set label to align with neural data
        if isFingerLabel
            switch lower(fingerLabel)
                case 'thumb'
                    currLabel = 1;
                case 'index'
                    currLabel = 2;
                case 'middle'
                    currLabel = 3;
                case 'ring'
                    currLabel = 4;
                case 'pinky'
                    currLabel = 5;
                otherwise
                    fprintf('no label set\n')
                    currLabel = nan;
            end
        else
            switch lower(cueLabel{cueNum})
                case 'delay'
                    currLabel = 0;
                case 'open'
                    currLabel = 6;
                case 'close'
                    currLabel = 7;
                case {'pinch', 'key'}
                    currLabel = 8;
                case 'power'
                    currLabel = 9;
                case 'wristflexion'
                    currLabel = 10;
                case 'wristextension'
                    currLabel = 11;
                case 'wristpronation'
                    currLabel = 12;
                case 'wristsupination'
                    currLabel = 13;
                    
                otherwise
                    fprintf('no label set\n')
                    currLabel = nan;
            end
        end
end
        