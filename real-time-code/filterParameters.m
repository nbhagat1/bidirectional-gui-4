function struct = filterParameters(string)
%a parameter file to maintain & declare filter variables
% uses: 1. initialize gui filter names by calling 'init'
%       2. pass in decoder name to retreive signal processing pipeline
%       required for real time system 
%
% Johnny Ciancibello 1.10.2018


%place decoder params here
if nargin < 1
    string = 'init';
end

switch lower(string)
    case 'init'
        %% All training decoder names go here to populate "Decoder build" GUI tab
        struct.decoder.SVM = [];
        struct.decoder.baggedEnsemble = [];
        struct.decoder.boostedRegressionEnsemble = [];
        
        struct.decoder.regressiontree = [];
        
        %% All Encoders 
        struct.encoder.frequencyBased = [];
        struct.encoder.linearAmplitude = [];
        
        
        %% ALL Artifact Rejection
        struct.artifactRejection.extraction = [];
        struct.artifactRejection.stopNhold = [];
        struct.artifactRejection.arPCA     = [];
        
        %AR parameters
        struct.artifactRejection.extraction.channels = randperm(128, 26);
        struct.artifactRejection.extraction.threshold = 1000; % = 1 mV
        
        
    case 'openloop'
        struct = {'CAR', 'Wavelet'};
        
    case 'baggedensemble'
        struct = {'BoxCAR', 'Wavelet', 'baggedEnsemble'};
        
    case 'boostedregressionensemble'
        struct = {'BoxCAR', 'Wavelet', 'BoostedRegressionEnsemble'};
        
    case 'boostedregressionensemble'
        struct = {'BoxCAR', 'Wavelet', 'BoostedRegressionEnsemble'};
        
    case 'regressiontree'
        struct = {'BoxCAR', 'Wavelet', 'RegressionTree'};
        
    case 'svm'
        struct = {'BoxCAR', 'Wavelet', 'svm'};
        
    case 'artifactRejection'
        struct = {'artifactRejection', 'BoxCAR', 'Wavelet'};
            
    otherwise
        warning('Filter parameters have NOT been set correctly')
end

end














