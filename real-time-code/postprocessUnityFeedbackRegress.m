function visualOut = postprocessUnityFeedbackRegress(decodedOut, u, currLabel)

%{
This function send the decoded output to the decoded hand in the unity environment
First, construct the message
Second, send the message over UDP


Classification Movement Labels:
0 = rest
1 = thumb
2 = index
3 = middle
4 = ring
5 = pinky
6 = open
7 = close
8 = pinch
9 = power

JGC 4.4.18
%}

destination = DestinationHand.RIGGED; %always send decoded output to rigged hand
force = single(-decodedOut.predicted);

switch currLabel
    case 0
        force = single(0);
        grasp = Finger.NONE; %rest
    case 1
        grasp = Finger.THUMB;
    case 2
        grasp = Finger.INDEX;
    case 3
        grasp = Finger.MIDDLE;
    case 4
        grasp = Finger.RING;
    case 5
        grasp = Finger.PINKY;
    case 6
        force = single(1);
        grasp = Finger.OPEN;
    case 7
        grasp = Finger.CLOSE;
    case 8
        grasp = Finger.PINCH;
    case 9
        grasp = Finger.POWER;
    otherwise
        fprintf('decoded grasp wasn''t send to unity\n')
end

%         fprintf('decoded:%d \n',decodedOut.predicted)

fullmsg = generate_force_message(destination, force, grasp);

try
    %send a udp msg
    fwrite(u, fullmsg)
    visualOut = true;
catch
    visualOut = false;
end