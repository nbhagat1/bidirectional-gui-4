function outcomes = testingGUI_realtimeloop(handles)
%this is testing the call back function of the gui start and stop button
%

%extract block info for RTloop
if handles.blockInfo.closedLoop
    %these are the learned decoder parameters
    decoderMdl = handles.filterInfo.sFilters{handles.blockInfo.decoderSelectedNumber};
end
if handles.blockInfo.nmStim
    nmstim = true;
end
if handles.blockInfo.normalize
    normalizeMethod = handles.blockInfo.normalizeSelected;
end
if handles.blockInfo.sensoryStim
    encoder = handles.blockInfo.encoderSelected;
    sensoryStim = true;
end
if handles.blockInfo.artifactRejection 
    artifactRejection = handles.blockInfo.artifactRejectionSelected;
end


%take start time of loop
handles.sessionInfo.startTime = clock;
handles.sessionInfo.loopStartTime = tic;

while true
    
    %Check if we pressed stop on GUI to exit loop
    running = get(handles.startButton, 'UserData');
    if ~running %~isempty(running) &&
        % assert and send any variables to workspace for GUI updating,
        % filter building, data saving
        %          (handles, parameters) = stop_RealTimeLoop(handles, parameters);
        
        
        handles.sessionInfo.duration = toc(handles.sessionInfo.loopStartTime);
%         assignin('base', 'handlesRT',handles)
        outcomes = handles;
        
        fprintf('stopping the loop now\n')
        break;
    end
    fprintf('pausing for 100 ms..\n')
    
    pause(.1)
    
end



end