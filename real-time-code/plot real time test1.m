
% plot real time framework timing results
% Johnny Ciancibello 10.17.17

%% general plot
figure;
plot(resultsWav10.wavTime)
ylabel('time (seconds)')
xlabel('loop interation #')
title('Computation time for Wavedec level 3')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot various wavlet level's timing results
figure;hold on;
plot(resultsWav3.wavTime)
plot(resultsWav5.wavTime)
plot(resultsWav8.wavTime)
plot(resultsWav10.wavTime)
plot(resultsWav12.wavTime)
ylabel('time (seconds)')
xlabel('loop interation #')
title('Computation time for several Wavelet Levels')
legend('Level 3', 'Level 5', 'Level 8','Level 10','Level 12')   
set(findall(gcf,'-property','FontSize'),'FontSize',18)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);


%% plot timing results on 11.10.17
load('timingResultFINAL.mat')
% load('timingResultFOR.mat')
% load('timingResultPARFOR.mat')
% load('timingResultMEX2.mat')

featMex     = resultsMEX4.featTime;
featParFor  = resultsPARFOR4.featTime;
featFor     = resultsFOR.featTime;
featTODD    = resultsTODD2.featTime;

figure; hold on;
plot(featFor)
plot(featParFor)
plot(featMex)
plot(featTODD)
legend('standard wavedec', 'parallel for', 'Mex with parallel for loop', 'parallel for with stripped down wavedec')
ylabel('time (seconds)')
xlabel('loop interation #')
title('Comparing computation time for Wavedec function calls')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% first plottting loop: organize results within seperate structures into a vector
for ii = 1:99
    preprocessTime(ii) = results.processedND{1,ii}.time;
    wavDecTime(ii) = results.neuralFeatures{1,ii}.timeWav;
%     muaTime(ii) = results.neuralFeatures{1,ii}.timeMUA;
%     decodeTime(ii) = results.decodedND{1,ii}.time;
end

overallLoopTime = results.looptime;

% plot results
figure; hold on;
plot(preprocessTime)
plot(wavDecTime)
% plot(muaTime)
% plot(decodeTime)
plot(overallLoopTime)

legend('\Delta t: Preprocessing','\Delta t: Wavelet Decomposition','Total Loop Time')


%% plottting MEX timing 
% organize results within seperate structures into a vector
for ii = 1:99
    preprocessTime(ii) = results.processedND{1,ii}.time;
%     muaTime(ii) = results.neuralFeatures{1,ii}.timeMUA;
%     decodeTime(ii) = results.decodedND{1,ii}.time;
end

overallLoopTime = results.looptime;
FeatTime = results.featTime;

% plot results
figure; hold on;
plot(preprocessTime)
plot(FeatTime)
% plot(muaTime)
% plot(decodeTime)
plot(overallLoopTime)

legend('\Delta t: Preprocessing','\Delta t: Wavelet Decomposition','Total Loop Time')




%% first RTF timing test
load('realtimetest1.mat')
figure; hold on;
ms = 1000;
plot(results1002.looptime.*ms)
plot(results402.looptime.*ms)
plot(results202.looptime.*ms)
legend('100x192 matrix', '40x192 matrix', '20x192 matrix')
ylabel('milliseconds')