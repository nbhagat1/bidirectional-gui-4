function s = bendsetupSerial(comPort)
% Initialize the serial port communication between Arduino and MATLAB
% WE ensure that the arduino is also communicating with MATLAB at this
% time. A predefined code on the Arduino acknowledges this.
% If setup is complete then the value of setup is returned as 1 else 0.

%% Variables to play with
pause1 = 2;
pause2 = 2;

%% Main
s = serial(comPort);
set(s, 'DataBits', 8 );
set(s, 'StopBits', 1 );
set(s, 'BaudRate', 1000000 ); % default 9600
set(s, 'Parity', 'none' );
fopen(s) ; % connects the serialport to the Arduino, open the serial object file 's'

try
    % Not entirely sure why we need the pauses here. Probably has to do
    % with the Arduino/IMU's boot up routine. Errors if set to 0.
    pause(pause1)
    fprintf(s, '%c', '1'); %write over the serial port the charecter '1'
    pause(pause2)
    %fscanf(s, '%c'); % read unsigned base ten integer format over serial port
    fgetl(s); %read a line of ASCII character and discard terminator
catch
    if ~isempty(instrfind)
        fclose(instrfind);
        delete(instrfind);
        clear
    end
    error('failed to initiate communications')
end


