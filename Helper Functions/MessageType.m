classdef MessageType < uint16
    enumeration
        SEQUENCE (0)
        FORCE (1)
    end
end