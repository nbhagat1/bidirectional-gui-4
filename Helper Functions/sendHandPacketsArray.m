% """
% Header
% 1 byte for message type (enum sequence = 0, enum force = 1)
% 1 byte for destination hand (enum cue hand = 0, enum rigged hand = 1)
% 2 bytes for the length of the message
% 
% Sequence message
% message type 1
% 4 bytes number of commands
% {
%     4 bytes force (0 - 10)
%     2 bytes movement (enum of grasp types)
%     4 bytes delay (single ms)
% }
% 
% Force message
% message type 2
% 4 bytes force
% 2 bytes movement
% 
% Example
% 0x02 0x00 0x01 0x0A 0x00 0x00 0x00 0x05 0x00 0x01
% """
% import socket
% import time
% from enum import IntEnum
% import struct
% from threading import Thread

UDP_IP = "127.0.0.1";
UDP_PORT = 11000;

%fullmsg = generate_example_sequence();
% This matches the message from Python
fullmsg = uint8([  0,   3,  72,   1,  32,   0,   0,   0, 102, 102, 102,  63,   1, ...
         0,   0,   0, 122,  68,   0,   0,   0,   0,   0,   0,   0,   0, ...
       122,  68, 102, 102, 102, 191,   1,   0,   0,   0, 122,  68,   0, ...
         0,   0,   0,   0,   0,   0,   0, 122,  68, 102, 102, 102,  63, ...
         2,   0,   0,   0, 122,  68,   0,   0,   0,   0,   0,   0,   0, ...
         0, 122,  68, 102, 102, 102, 191,   2,   0,   0,   0, 122,  68, ...
         0,   0,   0,   0,   0,   0,   0,   0, 122,  68, 102, 102, 102, ...
        63,   4,   0,   0,   0, 122,  68,   0,   0,   0,   0,   0,   0, ...
         0,   0, 122,  68, 102, 102, 102, 191,   4,   0,   0,   0, 122, ...
        68,   0,   0,   0,   0,   0,   0,   0,   0, 122,  68, 102, 102, ...
       102,  63,   8,   0,   0,   0, 122,  68,   0,   0,   0,   0,   0, ...
         0,   0,   0, 122,  68, 102, 102, 102, 191,   8,   0,   0,   0, ...
       122,  68,   0,   0,   0,   0,   0,   0,   0,   0, 122,  68, 102, ...
       102, 102,  63,  16,   0,   0,   0, 122,  68,   0,   0,   0,   0, ...
         0,   0,   0,   0, 122,  68, 102, 102, 102, 191,  16,   0,   0, ...
         0, 122,  68,   0,   0,   0,   0,   0,   0,   0,   0, 122,  68, ...
       102, 102, 102,  63,  31,   0,   0,   0, 122,  68,   0,   0,   0, ...
         0,   0,   0,   0,   0, 122,  68, 102, 102, 102, 191,  31,   0, ...
         0,   0, 122,  68,   0,   0,   0,   0,   0,   0,   0,   0, 122, ...
        68, 102, 102, 102,  63,   3,   0,   0,   0, 122,  68,   0,   0, ...
         0,   0,   0,   0,   0,   0, 122,  68, 102, 102, 102, 191,   3, ...
         0,   0,   0, 122,  68,   0,   0,   0,   0,   0,   0,   0,   0, ...
       122,  68, 102, 102, 102,  63,  12,   0,   0,   0, 122,  68,   0, ...
         0,   0,   0,   0,   0,   0,   0, 122,  68, 102, 102, 102, 191, ...
        12,   0,   0,   0, 122,  68,   0,   0,   0,   0,   0,   0,   0, ...
         0, 122,  68]);


% This was for testing
% echoudp('off');
% echoudp('on', UDP_PORT);

% open the UDP port
u = udp(UDP_IP, UDP_PORT, 'InputBufferSize', 1024, 'OutputBufferSize', 1024, 'EnablePortSharing', 'on', ...
    'ReadAsyncMode', 'continuous', 'BytesAvailableFcn', @mycallback, 'BytesAvailableFcnMode', 'byte');
fopen(u);
if ~strcmp(u.Status, 'open')
    fprintf('could not open UDP object\n');
    return
end
hcleanup = onCleanup(@() fclose(u));

fwrite(u, fullmsg);

done = false;
ct = 10;
while ~done
    if u.BytesAvailable > 0
        A = fread(u, [1 1024], 'uint8');
        % TODO do I need to send back finger states?
        disp(A);
        ct = ct - 1;        
    end
    if ct <= 0
        done = true;
    end
    pause(.1);
end
disp('done');

pause(1);
delete(hcleanup);
delete(u);

%% Helper functions
function fullmsg = generate_example_sequence()
    grasps = [Finger.THUMB, Finger.NONE, Finger.THUMB, Finger.NONE, ...
              Finger.POINTER, Finger.NONE, Finger.POINTER, Finger.NONE, ...
              Finger.MIDDLE, Finger.NONE, Finger.MIDDLE, Finger.NONE, ...
              Finger.RING, Finger.NONE, Finger.RING, Finger.NONE, ...
              Finger.PINKY, Finger.NONE, Finger.PINKY, Finger.NONE, ...
              Finger.THUMB | Finger.POINTER | Finger.MIDDLE | Finger.RING | Finger.PINKY, Finger.NONE, ...
              Finger.THUMB | Finger.POINTER | Finger.MIDDLE | Finger.RING | Finger.PINKY, Finger.NONE, ...
              Finger.THUMB | Finger.POINTER, Finger.NONE, Finger.THUMB | Finger.POINTER, Finger.NONE, ...
              Finger.MIDDLE | Finger.RING, Finger.NONE, Finger.MIDDLE | Finger.RING, Finger.NONE];
    numcommands = uint32(length(grasps));
    delays = single(1000.0) * ones(1, numcommands, 'single');
    forces = repmat(single([0.9, 0.0, -0.9, 0.0]), [1 floor(numcommands / 4)]);
    msglen = uint16(4 + 4 + numcommands * 10);
    header = [typecast(uint8([MessageType.SEQUENCE, DestinationHand.RIGGED | DestinationHand.CUE]), 'uint8') ...
        typecast(msglen, 'uint8')];
    msg = typecast(numcommands, 'uint8');
    for ii = 1:length(grasps)    
        msg = [msg typecast(forces(ii), 'uint8') typecast(uint32(grasps(ii)), 'uint8') typecast(delays(ii), 'uint8')];        
    end
    fullmsg = [header msg];
end

function mycallback(obj, event)
    % disp(obj.BytesAvailable);
end