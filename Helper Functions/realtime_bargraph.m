% realtime1.m
% C. Bouton
% 2019-02-18
% Plots spectra in real-time
 
clear all
close all
 
updateTime_sec = 0.1;  
totalTime_sec = 10000;
fsa = 1000; 
 
figure
 
% Starts out slow on first pass or two (adding 0.25 to 0.5 sec), but then
% extremely accurate after that
 
% Microphone record
% myrec = audiorecorder(fsa, 16, 2);  % fs, bits, 2=stereo

% Initialize Cbmex
[connection] = cbmex('open', 1);
[active_state, config_vector_out] = cbmex('trialconfig', 1 , 'double');
for chan = 1:128
    cbmex('config', chan, 'smpgroup', 2); % smpgroup 5 = 30kHz, 4 = 10kHz
end
temp = cbmex('trialdata',1);
pause(0.1)

time(1) = 0;
tic; % start timer
%record(myrec);  % start
 
for i = 2:(1+floor(totalTime_sec / updateTime_sec))
    
    [spike_data, buffer_start_time_sec, contdata] = cbmex('trialdata',1);
    
    while((toc-time(i-1))<(updateTime_sec/2))
    end
    
    y = double(cell2mat(contdata(1:64,3)'));
     
    [p, f] = pspectrum(y,fsa);
     
    auc = sum(p((f>30) & (f<300),:));  % integrate power from 30-300Hz
     
    %plot(f,20*log10(p))
     
    bar(120 + 20*log10(auc)); ylim([100,350]); xticks([0:2:64])
    drawnow;
    
    while((toc-time(i-1))<updateTime_sec)
    end
    time(i) = toc;
end
      
figure
plot(diff(time))