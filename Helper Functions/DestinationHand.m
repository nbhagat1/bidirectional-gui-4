classdef DestinationHand < uint16
    enumeration
        CUE (1)
        RIGGED (2)
    end
end