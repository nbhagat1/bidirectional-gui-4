function fullmsg = generate_force_message(destination, force, grasp)
% Modified by Nikunj to switch to individual fingers and wrist control using bend sensors
% 7-19-2019
% assert(isa(destination, DestinationHand));
% assert(isa(grasp, Finger));
assert(isa(force, 'single'));

if size(force,2) > 1
    % send individual joint angles (or equivalent force) information
    msglen = uint16(30); %num bits (force will be 24 bytes)        
else
    msglen = uint16(10); %num bits (force will be 4 bytes)
end

header = [uint8(MessageType.FORCE), uint8(destination), ...
    typecast(msglen, 'uint8')];

msg = [typecast(force, 'uint8') typecast(uint16(grasp), 'uint8')];

fullmsg = [header msg];
end