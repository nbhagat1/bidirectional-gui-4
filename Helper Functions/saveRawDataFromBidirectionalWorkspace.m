function saveRawDataFromBidirectionalWorkspace(blocknum)


%{
Get raw data from desired block of bidirectional GUI workspace,
save raw data as a .mat file for suag to replay and read from

7.9.18 JGC
%}

%save data from the workspace
BaseWorkspace = evalin('base','whos');
BaseWorkspaceCell = struct2cell(BaseWorkspace);

BaseWorkspaceNames = {BaseWorkspaceCell{1,:}};
check1 = contains(BaseWorkspaceNames, 'Block');
check2 = contains(BaseWorkspaceNames, 'results');
subsetnames = {BaseWorkspaceNames{check1 & check2}};
availableblocks = cell2mat(cellfun(@str2double, (regexp(subsetnames,'\d*','Match')), 'un',0));

%recreate block names
for jj=1:numel(availableblocks)
    blockname{1,jj} = strcat('Block_',num2str(availableblocks(jj)),'_results');
end

%get desired block data
Data = evalin('base',blockname{1,blocknum});

%remove unnecessary datapoints
indToRemove = find(Data.neuralData(1,:) == 0);
Data.neuralData(:,indToRemove) = [];
data = Data.neuralData;

%where to save
[FileName,PathName]  = uiputfile('*.mat');

%save!
save([PathName, FileName], 'data')
