function [data, labels] = getNeuralDataFromRegressionLabels(labels)



indToRemove = isnan(labels);
labels(:,indToRemove) = []; 


% calculate fft
load('lfp.mat');
fftdata = fft(shuffled);
Fs = 15e3;
T = 1/Fs;             % Sampling period       
L = length(shuffled);             % Length of signal
t = (0:L-1)*T;        % Time vector
f = Fs*(0:(L/2))/L;

% calculate single sided spectrum
P2 = abs(fftdata/L);
P1 = P2(:,1:L/2+1);
P1(:,2:end-1) = 2*P1(:,2:end-1);

plotvar = false;
if plotvar
    plot(f,P1)
    title('Single-Sided Amplitude Spectrum of S(t)')
    xlabel('f (Hz)')
    ylabel('|P1(f)|')
end

numlabels = unique(labels);
numlevels = linspace(800,1500, length(numlabels)+1);
ndata = [];
nraw_new = [];
gain = 10;

for i = 1:length(labels)
    label_level = find(numlabels == labels(i));
    %     nblock = P1;
    somegain = 100*randperm(10, 1);
    nblock = P1;
    
    if label_level ~=1
        nblock(:, f > numlevels(label_level) & f < numlevels(label_level+1)) = gain*somegain*P1(:, f > numlevels(label_level) & f < numlevels(label_level+1));
        nraw = ifft(nblock);
        randblock = find( f > numlevels(label_level)& f < numlevels(label_level+1)) ;
        nraw_new = nraw(:, randblock(1:1500));
      
    else
        %ignore modulating rest periods
        nraw_new = randi([0 100],10,1500)./1000;
        
    end
    
    ndata = horzcat(ndata, nraw_new);
end

%match 32 channels being sent in suag
indsToMatch = mod(1:32 , 10);
indsToMatch(indsToMatch == 0) = 10;
data = ndata(indsToMatch, :);

%where to save
[FileName,PathName]  = uiputfile('*.mat');

%save!
save([PathName, FileName], 'data')