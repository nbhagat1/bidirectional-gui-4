classdef Finger < uint16
    enumeration
        %text labels have flexion/ext. after how do we distinguish between the
        %two movement directions here?
        DELAY (0)
        NONE (0)
        THUMB (1)
        INDEX (2)
        MIDDLE (4)
        RING (8)
        PINKY (16)
        %ADDING FUNCTIONAL MOVEMENTS
        OPEN (31)
        CLOSE (31)
        POWER (30)
        PINCH (3)
        KEY (3)
        WRIST (32)
        FOREARM (64)
        HAND (63)
    end
end