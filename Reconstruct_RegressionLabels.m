function [regressLabels] = Reconstruct_RegressionLabels(block_results)
% Code for reconstructing Regression Labels vector based on previously
% recorded bi-directional_gui data
force_levels = str2num(block_results.handles.blockInfo.regressionLabel);
force_levels(force_levels == 0) = [];
cuelabels = block_results.labels;
regressLabels = zeros(size(cuelabels));

Indexes = find(cuelabels > 0);
Unique_StartIndexes = [Indexes(1), Indexes(find(diff(Indexes) > 1) + 1)];
Unique_StopIndexes = [Indexes((diff(Indexes) > 1)), Indexes(end)];

for i = 1:length(Unique_StartIndexes)
    regressLabels(Unique_StartIndexes(i):Unique_StopIndexes(i)) = 10*force_levels(i);
end

figure; plot(cuelabels); 
hold on; plot(regressLabels); 
legend('Cue Labels', 'Regress Labels');

