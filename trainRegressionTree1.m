function  [regMdl, filtInfo]  = trainRegressionTree(trainingData, handles)


[neuralData, labels, waveletData, wavlabels, regressionLabels, regressionWavLabels] = deal([]);
for jj=1:size(trainingData,2)
    neuralData = horzcat(neuralData, trainingData(jj).selblocks.neuralData);
    labels = horzcat(labels, trainingData(jj).selblocks.labels);
    regressionLabels = horzcat(regressionLabels, trainingData(jj).selblocks.regressionLabels);
    
    waveletData = horzcat(waveletData, trainingData(jj).selblocks.waveletData);
    wavlabels = horzcat(wavlabels, trainingData(jj).selblocks.waveltLabels);
    regressionWavLabels = horzcat(regressionWavLabels, trainingData(jj).selblocks.regressionLabelsWav);
end
  
% Truncate neural data
nanlabels = find(isnan(labels));
labels(nanlabels) = [];
nanreglabels = find(isnan(regressionLabels));
regressionLabels(nanreglabels) = [];
neuralData(:,nanlabels) = [];
labels = labels(1:size(neuralData,2));
regressionLabels = regressionLabels(1:size(neuralData,2));

% Truncate wavelet data
nanwavlabels = find(isnan(wavlabels));
wavlabels(nanwavlabels) = [];
waveletData(:, nanwavlabels) = [];

nanregwavlabels = find(isnan(regressionWavLabels));
regressionWavLabels(nanregwavlabels) = [];
wavlabels = wavlabels(1:size(waveletData,2));
regressionWavLabels = regressionWavLabels(1:size(waveletData,2));

%account for reaction time
reactionTime = 0.6;% sec
handles.refreshRateSec = .2;
K = ceil(reactionTime / handles.refreshRateSec);
% wavlabels_shifted = circshift(wavlabels,K);
wavlabels_shifted = [zeros(1,K) regressionWavLabels(1:end-K)];



X_train = movmean(waveletData, 5);
Y_train = wavlabels_shifted;



% Get "movement only" neural features (subtract baseline activity)
filtInfo.meanbaseline = nanmean(X_train(:, Y_train == 0),2);
filtInfo.stdbaseline = nanstd(X_train(:, Y_train == 0),[],2);


% preprocess raw neural data
filtInfo.minval = nanmin(X_train(:));
filtInfo.range = prctile(X_train(:), 90) - prctile(X_train(:), 10);%range(X_train(:));% 
neuraldata = scaledata(X_train, 0, 1, filtInfo.range, filtInfo.minval);
% [coeff, pcadata] = pca(neuraldata');
pcadata = neuraldata';


% Post-process the training cues to ramp/sine function from step function
labeltransitions = diff([0 wavlabels]);
labelonset = find(labeltransitions > 0)+1;
labeloffset = find(labeltransitions < 0);
sequences(1:2:(numel(labelonset)*2)) = labelonset;
sequences(2:2:(numel(labeloffset)*2)) = labeloffset;
sequences2 = diff([0, sequences, numel(wavlabels)]);

sequenceslabels = mat2cell(wavlabels, 1, sequences2);
sequencesforces = mat2cell(Y_train, 1, sequences2);
for jj=1:numel(sequenceslabels)
    if round(mode(sequenceslabels{1,jj})) == 0
       xs2{jj} = sequencesforces{1,jj};
       
    else
       maxamp = max(sequencesforces{1,jj});
       
       x = linspace(0,pi, numel(sequencesforces{1,jj}));
       xs2{jj} = maxamp.*sin(x);
       
    end
end
sin_Y_train = horzcat(xs2{:});



%train regression tree
groupedTrainData = [wavlabels_shifted; pcadata'];
[regMdl, ~] = trainRegressionModel_mediumTree(groupedTrainData');



figure;
h(1) = subplot(2,1,1);
imagesc(pcadata')
h(2) = subplot(2,1,2); hold on;
plot(wavlabels_shifted)
plot(sin_Y_train, 'r')
linkaxes(h, 'x')

figure;imagesc(diff(neuralData))

% Final training parameters for GUI filter table
filtInfo.filterName = handles.filterBuild.filterChoice;
filtInfo.filterType = 'regression'; 
filtInfo.trainingBlocks = handles.filterBuild.trainingBlocks;
filtInfo.featureCount = 22;
filtInfo.sensorFeedback = false;
filtInfo.startTime = clock; 
filtInfo.decoderName = 'regressiontree';
% filtInfo.pcatransformation = coeff;






