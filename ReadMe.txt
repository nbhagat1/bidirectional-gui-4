ReadMe.txt

_____________________________________
TODO List (Must be finished before trial):
-validate artifact extraction method
-able to upload S1 GUI outputs and parse sensory stimulation parameters, must compute psycometric curves (tip: https://www.mathworks.com/matlabcentral/fileexchange/42641-sigm_fit)
-integrate cerestim commands into encoding of main_real_time_loop.m
-copy calibrate sensor code from neuromuscular GUI into Sensor calibrate button
-integrate IMU rPi code into main_real_time_loop.m
-integrate NI DAQ to record cerestim monitoring port



_____________________________________
TO run closed-loop testing with SUAG

1. open 2 instances of matlab
2. On first instance, run BidirectionalGUI.m to view GUI
3. On second instance, navigate to SUAG folder and run suag.m (Note: Nothing will pop up, TCP Server listening)
4. On first instance, select File > Simulation Mode  (Note: this opens mockcbmex and initiates TCP Client)
5. SUAG GUI should pop up now, press Start, and it is ready to use with real-time system



_____________________________________
TO add new algorithms to the real time framework & Decoder training tab

1. Navigate to FilterParameters.m > 
1a. Under case 'init' declare decoder name (ex. struct.decoder.LinearRegression = [];)
1b. Now create a new case for the algorithm and declare real time signal processing chain 
    (This should replicate how the algorithm is trained offline)
    (ex. case 'linearregression' ; struct = {'BoxCAR', 'Wavelet', 'linearregression'} )

2. Within "Training Algorithms" Folder, create a new function that will train the algorithm

3. Navigate to BidirectionalGUI.m > search "trainFilterPushButton" > add another switch case that calls your new training algorithm

4. Navigate to DecoderNeuralSignal.m to implement the real-time decoding for new algorithm