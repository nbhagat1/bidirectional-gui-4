function varargout = BidirectionalGUI(varargin)
% BIDIRECTIONALGUI MATLAB code for BidirectionalGUI.fig
%      BIDIRECTIONALGUI, by itself, creates a new BIDIRECTIONALGUI or raises the existing
%      singleton*.
%
%      H = BIDIRECTIONALGUI  returns the handle to a new BIDIRECTIONALGUI or the handle to
%      the existing singleton*.
%
%      BIDIRECTIONALGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BIDIRECTIONALGUI.M with the given input arguments.
%
%      BIDIRECTIONALGUI('Property','Value',...) creates a new BIDIRECTIONALGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BidirectionalGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BidirectionalGUI_OpeningFcn via varargmydrivein.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES
%
% By Johnny Ciancibello 1.10.2018

% Edit the above text to modify the response to help BidirectionalGUI

% Last Modified by GUIDE v2.5 20-May-2019 14:07:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @BidirectionalGUI_OpeningFcn, ...
    'gui_OutputFcn',  @BidirectionalGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BidirectionalGUI is made visible.
function BidirectionalGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BidirectionalGUI (see VARARGIN)

% Choose default command line output for BidirectionalGUI
handles.output = hObject;


%add proper folders to path upon start up
mydir  = pwd;
addpath(genpath([mydir, '\real-time-code']))
addpath(genpath([mydir, '\suag']))
addpath([mydir, '\Helper Functions'])

addpath(genpath('C:\Program Files (x86)\AlphaOmega\AO_MatlabTool\matlabDirectory'))
addpath(genpath('C:\Program Files (x86)\Blackrock Microsystems\NeuroPort Windows Suite'))

% Initialise tabs
handles.tabManager = TabManager( hObject );


%Set-up a selection changed function on the create tab groups
tabGroups = handles.tabManager.TabGroups;
for tgi=1:length(tabGroups)
    set(tabGroups(tgi),'SelectionChangedFcn',@tabChangedCB)
end

%Delcare global variables to be seen from GUI and realtime software
global timeMonitor
timeMonitor = tic; %TODO: how is this used again?

% Initialize all data to be shared across GUI and Real Time System
handles.nspConnected = false;
handles.nsp2Connected = false;
handles.bendConnected = false;
handles.FocalStimConnected = false;
handles.serialBend = [];
handles.AOConnected  = false;
handles.dataSavePath = 'C:\Users\NBhagat1\Documents\MATLAB'; %'C:\Users\NB\Data';
handles.testmode = false;
handles.sr       = 10e3;
handles.refreshRate = 10; %50; %10;%Hz     %%%%%% LOOP RATE %%%%%%
handles.refreshRateSec = 0.100; %0.02; %0.100;%seconds
handles.AnimationUpdateRate = 0.2;

handles.functor = '';%@cbmex;%toggles btwn cbmex and mock simulation
handles.mockcbmex.open = false;
set(handles.connectNSP,'Checked','off');
set(handles.connectAO,'Checked','off');
set(handles.simulationMode,'Checked','off');

%task design parameters
handles.tasksetup.numreps   = 0;
handles.tasksetup.taskname  = '';
handles.tasksetup.cueDelay  = 0;   % 'inter-cue delay (REST) 
handles.tasksetup.prepostrestdur  = 5;   % at start of task and end of task
handles.tasksetup.cueTime   = 0;
handles.tasksetup.regressionTask = 0;
handles.taskcues.pinch     = 0;
handles.taskcues.power     = 0;
handles.taskcues.key       = 0;
handles.taskcues.close     = 0;
handles.taskcues.open      = 0;
handles.taskcues.indexFlexion       = 0;
handles.taskcues.indexExtension     = 0;
handles.taskcues.middleFlexion      = 0;
handles.taskcues.middleExtension    = 0;
handles.taskcues.ringFlexion        = 0;
handles.taskcues.ringExtension      = 0;
handles.taskcues.pinkyFlexion       = 0;
handles.taskcues.pinkyExtension     = 0;
handles.taskcues.thumbFlexion       = 0;
handles.taskcues.thumbExtension     = 0;
handles.taskcues.wristFlexion       = 0;
handles.taskcues.wristExtension     = 0;
handles.taskcues.wristPronation     = 0;
handles.taskcues.wristSupination    = 0;
handles.taskcues.ulnarDeviation     = 0;
handles.taskcues.radialDeviation    = 0;

%block design params
handles.blockInfo.openLoop = 0;
handles.blockInfo.closedLoop = 0;
handles.blockInfo.nmStim = 0;
handles.blockInfo.visualFeedback = 0;
handles.blockInfo.sensoryStim = 0;
handles.blockInfo.decoder = 0;
handles.blockInfo.decoderSelected = 0;
handles.blockInfo.decoderSelectedNumber = 0;
handles.blockInfo.encoder = 0;
handles.blockInfo.encoderSelected = 0;
handles.blockInfo.artifactRejection = 0;
handles.blockInfo.artifactRejectionSelected = 0;
handles.blockInfo.feedbackString = '';
handles.blockInfo.loadedtask = 0;
handles.blockInfo.stimMonitor = 0;
handles.blockInfo.task = '';
handles.blockInfo.cue = '';
handles.blockInfo.cueTime = '';

%Session/Filter Table variables
handles.sessionInfo.currentRow = 1;
handles.filterInfo.currentRow = 1;


%filter parameters
handles.params = filterParameters('init');
decoderNames ={};
encoderNames = {};
arNames = {};
sFilt = handles.filterTable.Data;


%set decoder options in listbox
if isfield(handles.params, 'decoder')
    decoderNames = fieldnames(handles.params.decoder);
    [decoderBuilds, decoderIndx] = intersect(sFilt(:,1),decoderNames, 'stable');
    if ~exist(decoderBuilds, 'var') || isempty(decoderBuilds)
        decoderBuilds = '...';
    end
    set(handles.decoderDropDown,'String',decoderBuilds);
end
%set training decoders listbox in the "decoder build" tab
set(handles.filterListbox, 'String', decoderNames)

%set encoder options in listbox
if isfield(handles.params, 'encoder')
    encoderNames = cellstr(fieldnames(handles.params.encoder));
    set(handles.encoderDropDown,'String',encoderNames);
end

%set artifact rejection options in listbox
if isfield(handles.params, 'artifactRejection')
    arNames = cellstr(fieldnames(handles.params.artifactRejection));
    set(handles.artifactRejectionDropDown,'String', arNames);
end



% Update handles structure
guidata(hObject, handles);

% UIWAIT makes BidirectionalGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% Called when a user clicks on a tab
function tabChangedCB(src, eventdata)
% commented out to minimize printing @ command line
% disp(['Changing tab from ' eventdata.OldValue.Title ' to ' eventdata.NewValue.Title ] );


% --- Outputs from this function are returned to the command line.
function varargout = BidirectionalGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in startButton.
function startButton_Callback(hObject, eventdata, handles)
% hObject    handle to startButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject, 'UserData', true);
run = true;

%CHECK important variables before going online
if isempty(handles.blockInfo.task)
    uiwait(errordlg('Please upload a task before starting the block.'))
    run = false;
end
if ~handles.blockInfo.openLoop && ~handles.blockInfo.closedLoop
    uiwait(errordlg('Please select a task paradigm (OL or CL)'));
    run = false;
end
if handles.blockInfo.openLoop
    %check to make sure no filters are selected?
    handles.pipeline = filterParameters('OpenLoop');
    handles.blockInfo.closedLoop = false;
end
if handles.blockInfo.closedLoop
    %check to make sure specific filters are turned on
    if ~handles.blockInfo.decoder
        uiwait(errordlg('Please choose a Decoder for closed-loop feedback'));
        run = false;
    else
        %get signal processing pipeline for real time system to reference
        handles.pipeline = filterParameters(handles.blockInfo.decoderSelected(5:end)); %TODO: is the decoder name saved anywhere else?
        handles.filtInfo.filterName = handles.blockInfo.decoderSelected(5:end);
    end
    
    if handles.blockInfo.sensoryStim
        if ~handles.blockInfo.encoder
            uiwait(errordlg('Please choose an Encoder for sensory stimulation feedback'));
            run = false;
        end
    end
end


if run
    set(handles.startButton, 'enable','off'); % ensure start button not pressed twice
    
    if handles.blockInfo.stimMonitor
        fprintf('collecting Stim monitor port data in background')
        %TODO: udp call to initiate stim monitor  matlab to begin colleting
        %DAQ data in background
    end
    
    %Begin to save neural data to file here
    if handles.nspConnected
        filename = datestr(clock, 'dd-mmm-yyyy' );
        cbmex('fileconfig', [handles.dataSavePath '\' filename], '', 1)
    end
    
    %Begin to save neural data for NSP2 to file here
    if handles.nsp2Connected
        filename = datestr(clock, 'dd-mmm-yyyy' );
        cbmex('fileconfig', [handles.dataSavePath '\' filename '_nsp2'], '', 1, 'instance',1)
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%% The real magic happens here %%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if handles.testmode %TODO: this is temporary until seemless integration with simulation gui
        Houtcomes = testingGUI_realtimeloop(handles);
    else
%         profile off
%         profile on -history

        
        chirpnoise=load('chirp.mat');
        [results, Houtcomes] = main_RealTimeLoop(handles,hObject);    %%%%%%% MAIN LOOP %%%%%%%%%%%%%%
        sound(chirpnoise.y);
%         profile viewer
    end
    
    %stop saving neural data to file
    if handles.nspConnected
        cbmex('fileconfig', [handles.dataSavePath '\' filename], '', 0)
    end    
    
    %stop saving neural data from nsp2 to file
    if handles.nsp2Connected
        cbmex('fileconfig', [handles.dataSavePath '\' filename '_nsp2'], '', 0, 'instance',1)
    end
    
    %Block results placed in the workspace
    assignin('base', Houtcomes.blockInfo.resultsName , results)
    
    %organize block info to set session table data
    currentTableData = get(handles.sessionTable, 'data');
    currentTableData{handles.sessionInfo.currentRow, 1} = datestr(Houtcomes.sessionInfo.startTime);
    currentTableData{handles.sessionInfo.currentRow, 2} = round(Houtcomes.sessionInfo.duration ./60, 2);
    currentTableData{handles.sessionInfo.currentRow, 3} = Houtcomes.blockInfo.task;
    currentTableData{handles.sessionInfo.currentRow, 4} = Houtcomes.blockInfo.feedbackString;
    currentTableData{handles.sessionInfo.currentRow, 5} = Houtcomes.blockInfo.nmStim;
    currentTableData{handles.sessionInfo.currentRow, 6} = Houtcomes.blockInfo.decoderSelectedNumber;
    currentTableData{handles.sessionInfo.currentRow, 7} = Houtcomes.blockInfo.encoderSelected;
    currentTableData{handles.sessionInfo.currentRow, 8} = Houtcomes.blockInfo.artifactRejectionSelected;
    set(handles.sessionTable, 'data', currentTableData)
    
    
    handles.sessionInfo.currentRow = handles.sessionInfo.currentRow +1;
    set(handles.startButton, 'enable','on')
else
    fprintf('did not go real-time\n')
end

guidata(hObject, handles)

% --- Executes on button press in stopButton.
function stopButton_Callback(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global UpdateAnimationTimer
set(handles.startButton, 'UserData', false);
stop(UpdateAnimationTimer);
set(handles.startButton, 'enable','on');
guidata(hObject, handles)

% --- Executes on button press in openLoopButton.
function openLoopButton_Callback(hObject, eventdata, handles)
% hObject    handle to openLoopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of openLoopButton
handles.blockInfo.openLoop = get(hObject,'Value');
if handles.blockInfo.openLoop
    %turn all closed-loop options OFF
    handles.blockInfo.feedbackString = 'OL';
    handles.blockInfo.closedLoop = 0;
    set(handles.closedLoopButton, 'Value', 0);
    set(handles.NMstimCheckBox, 'enable', 'on');
    set(handles.NMstimCheckBox, 'Value', 0);
    set(handles.SensoryStimCheckBox, 'enable', 'off');
    set(handles.SensoryStimCheckBox, 'Value', 0);
    set(handles.visualCheckBox, 'enable', 'off');
    set(handles.visualCheckBox, 'Value', 0);
    set(findall(handles.FilterGroup, '-property', 'enable'), 'enable', 'off');
    set(handles.decoderCheckBox, 'Value', 0);
    set(handles.encoderCheckBox, 'Value', 0);
    set(handles.artifactRejection, 'Value', 0);
end
guidata(hObject, handles)


% --- Executes on button press in closedLoopButton.
function closedLoopButton_Callback(hObject, eventdata, handles)
% hObject    handle to closedLoopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of closedLoopButton
handles.blockInfo.closedLoop = get(hObject,'Value');
if handles.blockInfo.closedLoop
    handles.blockInfo.feedbackString = 'CL';
    handles.blockInfo.openLoop = 0;
    set(handles.openLoopButton, 'Value', 0);
    set(handles.NMstimCheckBox, 'enable', 'on');
    set(handles.SensoryStimCheckBox, 'enable', 'on');
    set(handles.visualCheckBox, 'enable', 'on');
    set(handles.visualCheckBox, 'Value', 1);
    set(findall(handles.FilterGroup, '-property', 'enable'), 'enable', 'on');
end
guidata(hObject, handles)

% --- Executes on button press in NMstimCheckBox.
function NMstimCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to NMstimCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NMstimCheckBox
handles.blockInfo.nmStim = get(hObject, 'Value');
guidata(hObject, handles)

% --- Executes on button press in SensoryStimCheckBox.
function SensoryStimCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to SensoryStimCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of SensoryStimCheckBox
handles.blockInfo.sensoryStim = get(hObject, 'Value');
guidata(hObject, handles)


% --- Executes on button press in visualCheckBox.
function visualCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to visualCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of visualCheckBox
handles.blockInfo.visualFeedback = get(hObject, 'Value');
guidata(hObject, handles)


% --- Executes on button press in loadButton.
function loadButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% [filename, path] = uigetfile('*.txt');
% [~,taskName,~] = fileparts(filename);
% 
% fid = fopen(strcat(path, filename), 'r');
% if contains(lower(filename),'regress')
%     %NOTE: filename must have Regress within it
%     handles.blockInfo.loadedtask = textscan(fid, '%s %f %f');
%     cues  = handles.blockInfo.loadedtask{1};
%     times = num2str(handles.blockInfo.loadedtask{2});
%     regLabel = num2str(handles.blockInfo.loadedtask{3});
%     cuesTimes = strcat(cues, {' '}, times, {' '}, regLabel);
%     
%     %for RTS
%     handles.blockInfo.regressionLabel = regLabel;
% else
%     % classification task
%     handles.blockInfo.loadedtask = textscan(fid, '%s %f');
%     cues  = handles.blockInfo.loadedtask{1};
%     times = num2str(handles.blockInfo.loadedtask{2});
%     cuesTimes = strcat(cues, {' '}, times);
%     
%     %remove regressionLabel field
%     if isfield(handles.blockInfo, 'regressionLabel')
%         handles.blockInfo = rmfield(handles.blockInfo, 'regressionLabel');
%     end
% end
% fclose(fid);

[filename, path] = uigetfile('*.json');
[~,taskName,~] = fileparts(filename);

fid = fopen(strcat(path, filename), 'r');
tasktxt = fread(fid,'*char');
task = jsondecode(tasktxt');

if task.regressionTask
    repDesign = [];
    for irep = 1:task.numreps
        tempDesign = [repmat(task.cueNames,length(task.regressLevels),1) num2cell(repmat(task.cueTime,length(task.cueNames)*length(task.regressLevels),1)) num2cell(reshape(repmat(task.regressLevels',length(task.cueNames),1),[],1))];        
        % complete randomization
%         tempDesign = tempDesign(randperm(size(tempDesign,1)),:);
%         repDesign = [repDesign;tempDesign];

        % all regression levels of a particular cue occur together
        tempids = randperm(size(task.cueNames,1));
        for icue = 1:length(task.cueNames)
            repDesign = [repDesign; tempDesign(tempids(icue) + (randperm(length(task.regressLevels))-1).*size(task.cueNames,1),:)];
        end
    end
    startstopDelay = {'delay' task.prepostrestdur 0};
    delayRow = {'delay' task.cueDelay 0};
    
else    
    repDesign = [];
    for irep = 1:task.numreps
        tempDesign = [task.cueNames, num2cell(repmat(task.cueTime,length(task.cueNames),1))];        
        % complete randomization
        tempDesign = tempDesign(randperm(size(tempDesign,1)),:);
        repDesign = [repDesign;tempDesign];
    end
    startstopDelay = {'delay' task.prepostrestdur};
    delayRow = {'delay' task.cueDelay};    
end
delayDesign = repmat(delayRow,size(repDesign,1),1);
taskDesign = [startstopDelay; reshape([delayDesign(:) repDesign(:)]',2*size(repDesign,1),[]); delayRow; startstopDelay];
% taskDesign(2,:) = []; % removing an extra intercue delay;
if task.regressionTask
    handles.blockInfo.loadedtask = {taskDesign(:,1) cell2mat(taskDesign(:,2)) cell2mat(taskDesign(:,3))};
else
    handles.blockInfo.loadedtask = {taskDesign(:,1) cell2mat(taskDesign(:,2))};
end

cues  = handles.blockInfo.loadedtask{1};
times = num2str(handles.blockInfo.loadedtask{2});
if task.regressionTask    
    regLabel = num2str(handles.blockInfo.loadedtask{3});
    cuesTimes = strcat(cues, {' '}, times, {' '}, regLabel);
    
    %for RTS
    handles.blockInfo.regressionLabel = regLabel;
else
    cuesTimes = strcat(cues, {' '}, times);
    
    %remove regressionLabel field
    if isfield(handles.blockInfo, 'regressionLabel')
        handles.blockInfo = rmfield(handles.blockInfo, 'regressionLabel');
    end
end
fclose(fid);

% Save the task progression
[filename, path] = uiputfile('Trial*.txt');
fid = fopen(strcat(path, filename), 'w');
fprintf(fid,'%s\n',cuesTimes{:});
fclose(fid);

% update listbox to display the task info
set(handles.taskDisplayBox,'String', cuesTimes)

% set task name to display next to loaded task button
set(handles.taskNameLoadedText,'String', taskName)

%save task info for RTS
handles.blockInfo.cue = cues;
handles.blockInfo.cueTime = times;
handles.blockInfo.task = taskName;

guidata(hObject, handles)


% --- Executes on selection change in taskDisplayBox.
function taskDisplayBox_Callback(hObject, eventdata, handles)
% hObject    handle to taskDisplayBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns taskDisplayBox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from taskDisplayBox


% --- Executes during object creation, after setting all properties.
function taskDisplayBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to taskDisplayBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in decoderDropDown.
function decoderDropDown_Callback(hObject, eventdata, handles)
% hObject    handle to decoderDropDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns decoderDropDown contents as cell array
%        contents{get(hObject,'Value')} returns selected item from decoderDropDown
handles.blockInfo.decoderSelectedNumber = get(hObject,'Value');
contents = cellstr(get(hObject,'String'));
handles.blockInfo.decoderSelected = contents{handles.blockInfo.decoderSelectedNumber};
guidata(hObject, handles)

% --- Executes during object creation, after setting all properties.
function decoderDropDown_CreateFcn(hObject, eventdata, handles)
% hObject    handle to decoderDropDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in decoderCheckBox.
function decoderCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to decoderCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of decoderCheckBox
handles.blockInfo.decoder = get(hObject,'Value');
guidata(hObject, handles)

% --- Executes on selection change in encoderDropDown.
function encoderDropDown_Callback(hObject, eventdata, handles)
% hObject    handle to encoderDropDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns encoderDropDown contents as cell array
%        contents{get(hObject,'Value')} returns selected item from encoderDropDown
contents = cellstr(get(hObject,'String'));
handles.blockInfo.encoderSelected = contents{get(hObject,'Value')};
guidata(hObject, handles)

% --- Executes during object creation, after setting all properties.
function encoderDropDown_CreateFcn(hObject, eventdata, handles)
% hObject    handle to encoderDropDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in encoderCheckBox.
function encoderCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to encoderCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of encoderCheckBox
handles.blockInfo.encoder = get(hObject,'Value');
guidata(hObject, handles)

% --------------------------------------------------------------------
function file_menu_Callback(hObject, eventdata, handles)
% hObject    handle to file_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function connectNSP_Callback(hObject, eventdata, handles)
% hObject    handle to connectNSP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if (~handles.nspConnected)    
% If NSP is already not connected via cbmex UDP communication, then connect
% otherwise disconnect
    connection = -1;
    try
        connection = cbmex('open');
    catch
        fprintf('Could not connect to cbmex\n')
        cbmex('close')
    end

    %if connection == 2
    if connection == 1
        %link functor to cbmex
        handles.functor = @cbmex;

        disp('Connected via to NSP1 via UDP!')

        % record from all channels
        cbmex('mask', 0, 1);                

        %Neural data recorded at 30 kHz
        for chan = 1:128
            cbmex('config', chan, 'smpgroup', 4); % smpgroup 5 = 30kHz, 4 = 10kHz
        end

        %sensor recordings at 1kHz
        for ain=1:8
            cbmex('config', 128+ain, 'smpgroup', 4);
        end

        % Power OpAmps for sensors
        cbmex('analogout', 145, 'sequence',[30000 32767]) %ch1 should be +5V 
        cbmex('analogout', 146, 'sequence',[30000 -32767]) %ch2 should be -5V

        handles.nspConnected = true;

        %disply check next to menu item
        set(hObject,'Checked','on');
    else
        handles.nspConnected = false;

        set(hObject,'Checked','off');
        error('NSP NOT connected')
    end
else
    %Disconnect NSP
    cbmex('close');
    handles.nspConnected = false;    
    warndlg('Disconnected NSP');
    set(hObject,'Checked','off');
end
guidata(hObject, handles)

% --------------------------------------------------------------------
function ConnectNSP2_Callback(hObject, eventdata, handles)
% hObject    handle to ConnectNSP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if (~handles.nsp2Connected)
    %connect NSP via cbmex UDP communication
    connection2 = -1;
    try
        connection2 = cbmex('open', 2, 'central-addr', '192.168.137.017', 'instance', 1);
    catch
        fprintf('Could not connect to cbmex for NSP2\n')
        cbmex('close')
    end

    if connection2 == 1
        %link functor to cbmex
        %handles.functor = @cbmex; %already initialized

        disp('Connected via to NPS2 via UDP!')

        % record from all channels
        cbmex('mask', 0, 1, 'instance',1);

        %Neural data recorded at 30 kHz
        for chan = 1:128
            cbmex('config', chan, 'smpgroup', 4, 'instance', 1); % smpgroup 5 = 30kHz, 4 = 10kHz
        end

        %sensor recordings at 1kHz
        for ain=1:16
            cbmex('config', 128+ain, 'smpgroup', 2,'instance', 1);
        end

        % Power OpAmps for sensors
    %     cbmex('analogout', 145, 'sequence',[30000 32767]) %ch1 should be +5V 
    %     cbmex('analogout', 146, 'sequence',[30000 -32767]) %ch2 should be -5V

        handles.nsp2Connected = true;

        %disply check next to menu item
        set(hObject,'Checked','on');
    else
        handles.nsp2Connected = false;

        set(hObject,'Checked','off');
        error('NSP2 NOT connected')
    end
else    
    %Disconnect NSP2
    cbmex('close','instance',1);
    handles.nsp2Connected = false;
    warndlg('Disconnected NSP2');
    set(hObject,'Checked','off');
end
guidata(hObject, handles)

% --------------------------------------------------------------------
function connectAO_Callback(hObject, eventdata, handles)
% hObject    handle to connectAO (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% CONNECT to AO
% DspMac='54:4A:16:7E:9B:2D';  % written on the back of the Rackmount.
% PCMac='aa:bb:cc:dd:ee:ff';
% AdpaterIndex=-1;
% connectedAO = AO_startConnection(DspMac,PCMac,AdpaterIndex);  % connect to SnR
% if connectedAO == 2
%     fprintf('AO Connected\n')
%     handles.AOConnected = true;
%     
%     %disply check next to menu item
%     set(hObject,'Checked','on');
% else
%     fprintf('AO NOT Connected\n')
%     handles.AOConnected = false;
%     
%     set(hObject,'Checked','off');
% end
%Initialize Serial port for sampling bend sensors - Nikunj 05-03-2019

if (~handles.FocalStimConnected)
    handles.serialFocalStim = Connect_FocalStim('COM9'); % Teensy port
    if (handles.serialFocalStim == -1)
        handles.FocalStimConnected = false; 
        set(hObject,'Checked','off');
    else
        handles.FocalStimConnected = true;
        %disply check next to menu item
        set(hObject,'Checked','on');
        disp(' ');
        disp('Connected to FocalStim'); 
    end
else
    % Close existing serial com port
    fclose(handles.serialFocalStim);
    delete(handles.serialFocalStim);
    handles.FocalStimConnected = false;
    set(hObject,'Checked','off');
    warndlg('Disconnected FocalStim');    
end    
guidata(hObject, handles)


% --------------------------------------------------------------------
function Untitled_4_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in trainFilterPushButton.
function trainFilterPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to trainFilterPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

train = 1;
if isempty(handles.filterBuild.trainingBlocks) || ~isfield(handles.filterBuild, 'trainingBlocks')
    uiwait(errordlg('Please enter the Blocks you''d like to train on'))
    train=0;
end

%load up data and pass to decoder
% Checks for valid blocks in workspace and loads the variables to pass on
blocksSelected = handles.filterBuild.trainingBlocks;

BaseWorkspace = evalin('base','whos');
BaseWorkspaceCell = struct2cell(BaseWorkspace);
BaseWorkspaceNames = {BaseWorkspaceCell{1,:}};

check1 = contains(BaseWorkspaceNames, 'Block');
check2 = contains(BaseWorkspaceNames, 'results');
subsetnames = {BaseWorkspaceNames{check1 & check2}};
availableblocks = cell2mat(cellfun(@str2double, (regexp(subsetnames,'\d*','Match')), 'un',0));
validblocks = ismember(availableblocks ,blocksSelected);
selectedblocks = {subsetnames{validblocks}};

for jj=1:numel(blocksSelected)
    blockname{1,jj} = strcat('Block_',num2str(blocksSelected(jj)),'_results');
end
validateblocks = strcmp(blockname, selectedblocks);
% assert(sum(validateblocks) == numel(blocksSelected),'Selected blocks are not available in the workspace');

%place all data into struct for training decoder
for block=1:numel(blocksSelected)
    trainingData(block).selblocks = evalin('base',blockname{1,block});
    %     eval(sprintf('trainingData(%d).selblocks = %s',block, blockname{1,block}));
end


if train
    fprintf('Training... ')
    switch lower(handles.filterBuild.filterChoice)
        case 'baggedensemble'
            [mdl, filtInfo] = trainClassificationBaggedEnsemble(trainingData, handles);
            
            handles.filtInfo.minval{handles.filterInfo.currentRow} = filtInfo.minval;
            handles.filtInfo.range{handles.filterInfo.currentRow} = filtInfo.range;
            handles.filtInfo.meanbaseline{handles.filterInfo.currentRow} = filtInfo.meanbaseline;
            handles.filtInfo.stdbaseline{handles.filterInfo.currentRow} = filtInfo.stdbaseline;
            %             handles.filtInfo.pcatransformation = filtInfo.pcatransformation;
            handles.filtInfo.filterName = 'baggedensemble';
            
        case 'boostedregressiontree'
            [mdl, filtInfo] = trainBoostedRegressionTree(trainingData, handles);
            
            handles.filtInfo.minval{handles.filterInfo.currentRow} = filtInfo.minval;
            handles.filtInfo.range{handles.filterInfo.currentRow} = filtInfo.range;
            handles.filtInfo.meanbaseline{handles.filterInfo.currentRow} = filtInfo.meanbaseline;
            handles.filtInfo.stdbaseline{handles.filterInfo.currentRow} = filtInfo.stdbaseline;
            handles.filtInfo.pcatransformation = filtInfo.pcatransformation;
            handles.filtInfo.filterName = 'boostedregressiontree';
            
        case 'svm'
            [mdl, filtInfo] = trainSVM(trainingData, handles);
            
            
            handles.filtInfo.minval{handles.filterInfo.currentRow} = filtInfo.minval;
            handles.filtInfo.range{handles.filterInfo.currentRow} = filtInfo.range;
            handles.filtInfo.meanbaseline{handles.filterInfo.currentRow} = filtInfo.meanbaseline;
            handles.filtInfo.stdbaseline{handles.filterInfo.currentRow} = filtInfo.stdbaseline;
            %             handles.filtInfo.pcatransformation{handles.filterInfo.currentRow} = filtInfo.pcatransformation;
            handles.filtInfo.filterName = 'svm';
            
            
        case 'boostedregressionensemble'
            [mdl, filtInfo] = trainBoostedRegressionEnsemble(trainingData, handles);
            
            
            handles.filtInfo.minval{handles.filterInfo.currentRow} = filtInfo.minval;
            handles.filtInfo.range{handles.filterInfo.currentRow} = filtInfo.range;
            handles.filtInfo.meanbaseline{handles.filterInfo.currentRow} = filtInfo.meanbaseline;
            handles.filtInfo.stdbaseline{handles.filterInfo.currentRow} = filtInfo.stdbaseline;
            %             handles.filtInfo.pcatransformation{handles.filterInfo.currentRow} = filtInfo.pcatransformation;
            handles.filtInfo.filterName = 'boostedregressionensemble';
            
            
        case 'regressiontree'
            [mdl, filtInfo] = trainRegressionTree(trainingData, handles);
            
            
            handles.filtInfo.minval{handles.filterInfo.currentRow} = filtInfo.minval;
            handles.filtInfo.range{handles.filterInfo.currentRow} = filtInfo.range;
            handles.filtInfo.meanbaseline{handles.filterInfo.currentRow} = filtInfo.meanbaseline;
            handles.filtInfo.stdbaseline{handles.filterInfo.currentRow} = filtInfo.stdbaseline;
            %             handles.filtInfo.pcatransformation{handles.filterInfo.currentRow} = filtInfo.pcatransformation;
            handles.filtInfo.filterName = 'regressiontree';
            
            
            %New decoder goes here
            
        otherwise
            uiwait(errordlg('AH!, the selected Filter doesn''t have a training scheme in place'));
    end
    
    
    
    if ~isempty(mdl)
        %place built model into filter Table
        currentFilterTableData = get(handles.filterTable, 'data');
        currentFilterTableData{handles.filterInfo.currentRow, 1} = filtInfo.filterName;
        currentFilterTableData{handles.filterInfo.currentRow, 2} = mat2str(filtInfo.trainingBlocks);
        currentFilterTableData{handles.filterInfo.currentRow, 3} = filtInfo.featureCount;
        currentFilterTableData{handles.filterInfo.currentRow, 4} = filtInfo.sensorFeedback;
        currentFilterTableData{handles.filterInfo.currentRow, 5} = datestr(filtInfo.startTime);  %format to 'HH:MM:SS'?
        
        set(handles.filterTable, 'data', currentFilterTableData)
        
        %store learned params into sFilters
        handles.filterInfo.sFilters{handles.filterInfo.currentRow} = mdl;
        handles.filterInfo.filterName = filtInfo.filterName;
        
        %index for following trained decoder
        handles.filterInfo.currentRow = handles.filterInfo.currentRow +1;
        
        %Fill decoder drop down with trained decoders
        if isfield(handles.params, 'decoder')
            validDecoders = ~cellfun(@isempty, handles.filterTable.Data(:,1));
            decoderBuildList = cell(1,sum(validDecoders));
            for dd = 1:sum(validDecoders)
                ddNum = sprintf('(%d)  ', dd);
                decoderBuildList(dd) = strcat(ddNum,{' '},handles.filterTable.Data(dd,1));
            end
            set(handles.decoderDropDown,'String',decoderBuildList);
        end
        
        fprintf('...Done \n')
    end
else
    fprintf('No Filter was trained\n')
end

guidata(hObject, handles)

% --- Executes on selection change in filterListbox.
function filterListbox_Callback(hObject, eventdata, handles)
% hObject    handle to filterListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns filterListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filterListbox
contents = cellstr(get(hObject,'String'));
handles.filterBuild.filterChoice = contents{get(hObject,'Value')};
guidata(hObject, handles)

% --- Executes during object creation, after setting all properties.
function filterListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filterListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function trainingBlocks_Callback(hObject, eventdata, handles)
% hObject    handle to trainingBlocks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of trainingBlocks as text
%        str2double(get(hObject,'String')) returns contents of trainingBlocks as a double
%make NAN to empty
tblocks = strsplit(get(hObject, 'String'));
blankSpaces = strcmp(tblocks,'');
tblocks(blankSpaces) = [];
handles.filterBuild.trainingBlocks = str2double(tblocks);
guidata(hObject, handles)


% --- Executes during object creation, after setting all properties.
function trainingBlocks_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trainingBlocks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function excludeChan_Callback(hObject, eventdata, handles)
% hObject    handle to excludeChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of excludeChan as text
%        str2double(get(hObject,'String')) returns contents of excludeChan as a double
handles.filterBuild.excludeChan = get(hObject,'Value');
guidata(hObject, handles)

% --- Executes during object creation, after setting all properties.
function excludeChan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to excludeChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function taskName_Callback(hObject, eventdata, handles)
% hObject    handle to taskName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of taskName as text
%        str2double(get(hObject,'String')) returns contents of taskName as a double
handles.tasksetup.taskname = get(hObject,'String');
guidata(hObject, handles)



% --- Executes during object creation, after setting all properties.
function taskName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to taskName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function numReps_Callback(hObject, eventdata, handles)
% hObject    handle to numReps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numReps as text
%        str2double(get(hObject,'String')) returns contents of numReps as a double
handles.tasksetup.numreps = str2double(get(hObject,'String'));
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function numReps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numReps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cueDelay_Callback(hObject, eventdata, handles)
% hObject    handle to cueDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cueDelay as text
%        str2double(get(hObject,'String')) returns contents of cueDelay as a double
handles.tasksetup.cueDelay = str2double(get(hObject,'String'));
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function cueDelay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cueDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in savePushButton.
function savePushButton_Callback(hObject, eventdata, handles)
% hObject    handle to savePushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% check required fields before saving
task = handles.tasksetup;
taskCues = handles.taskcues;
if isempty(task.numreps) || task.numreps == 0
    h = msgbox('Please enter the repetition number','Error','error');
end
if isempty(task.taskname)
    h = msgbox('Please enter a NAME for the overall task','Error','error');
end
if isempty(task.cueTime) || task.cueTime == 0
    h = msgbox('Please enter a valid cue time','Error','error');
end
if isempty(task.cueDelay) || task.cueDelay == 0
    h = msgbox('Are you sure you don''t want a delay between cues??','Warning','warn');
end
if isfield(task,"regressLevels") && isempty(task.regressLevels) && task.regressionTask
    h = msgbox('Please enter levels to regress upon!', 'Error', 'error');
end

%determine which Cues are checked
cuefields = fieldnames(taskCues);
checkedCues = structfun(@(x) isequal(x,1),taskCues); %% Santosh 08/02/2019
checkedCueNames = cuefields(checkedCues);

%NOW save the file :)
filename = strcat(handles.tasksetup.taskname);
[filename, pathname] = uiputfile([filename '.json'], 'Save as');

% write task selections into text file for Unity environment to interpret
fid = fopen(fullfile(pathname,filename), 'w');
delayTime = task.cueDelay;
cueTime = task.cueTime;

task.cueNames = checkedCueNames;
tasktxt = jsonencode(task);
fwrite(fid,tasktxt,'char');
% task = rmfield(task,'cueNames');

% if task.regressionTask
%     %begin with a delay
%     fprintf(fid, 'delay %g %g\n', delayTime, 0);
%     
%     %regression task
%     regressionLevels = [task.regressLevels fliplr(task.regressLevels)];
%     
%     for k = 1:repRow
%         for kk = 1:repCol
%             tempTask = randTask(k,kk);
%             for j = 1:length(regressionLevels)
%                 % third column is the force level
%                 fprintf(fid, '%s %g %g\n', char(tempTask), cueTime, regressionLevels(j));
%             end
%             fprintf(fid, 'delay %g %g\n', delayTime, 0);
%         end
%     end
%     
% else %classification task
%     %begin with delay
%     % HARDCODING THE REST PERIOD AT THE BEGINNING
%     fprintf(fid, 'delay %g\n', handles.tasksetup.prepostrestdur);
%     
%     for k = 1:repRow
%         for kk = 1:repCol
%             tempTask = randTask(k,kk);
%             fprintf(fid,'%s %g\n', char(tempTask), cueTime);
%             fprintf(fid, 'delay %g\n', delayTime);
%         end
%     end
%     % HARDCODING THE REST PERIOD AT THE END
%     fprintf(fid, 'delay %g\n', handles.tasksetup.prepostrestdur);
% end

fclose(fid);
guidata(hObject,handles)



% --- Executes on button press in indexFlexionCheckBox.
function indexFlexionCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to indexFlexionCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of indexFlexionCheckBox
handles.taskcues.indexFlexion = get(hObject,'Value');
guidata(hObject,handles)

% --- Executes on button press in indexExtCheckBox.
function indexExtCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to indexExtCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of indexExtCheckBox
handles.taskcues.indexExtension = get(hObject,'Value');
guidata(hObject,handles)

% --- Executes on button press in middleFlexionCheckBox.
function middleFlexionCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to middleFlexionCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of middleFlexionCheckBox
handles.taskcues.middleFlexion  = get(hObject,'Value');
guidata(hObject,handles)

% --- Executes on button press in middleExtCheckBox.
function middleExtCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to middleExtCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of middleExtCheckBox
handles.taskcues.middleExtension = get(hObject,'Value');
guidata(hObject,handles)

% --- Executes on button press in ringFlexionCheckBox.
function ringFlexionCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to ringFlexionCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ringFlexionCheckBox
handles.taskcues.ringFlexion = get(hObject,'Value');
guidata(hObject,handles)

% --- Executes on button press in ringExtCheckBox.
function ringExtCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to ringExtCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ringExtCheckBox
handles.taskcues.ringExtension = get(hObject,'Value');
guidata(hObject,handles)

% --- Executes on button press in pinkyFlexionCheckBox.
function pinkyFlexionCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to pinkyFlexionCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pinkyFlexionCheckBox
handles.taskcues.pinkyFlexion = get(hObject,'Value');
guidata(hObject,handles)

% --- Executes on button press in pinkyExtCheckBox.
function pinkyExtCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to pinkyExtCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pinkyExtCheckBox
handles.taskcues.pinkyExtension = get(hObject,'Value');
guidata(hObject,handles)

% --- Executes on button press in thumbFlexionCheckBox.
function thumbFlexionCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to thumbFlexionCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of thumbFlexionCheckBox
handles.taskcues.thumbFlexion = get(hObject,'Value');
guidata(hObject, handles)

% --- Executes on button press in thumbExtCheckBox.
function thumbExtCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to thumbExtCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of thumbExtCheckBox
handles.taskcues.thumbExtension = get(hObject,'Value');
guidata(hObject, handles)

% --- Executes on button press in wristFlexionCheckBox.
function wristFlexionCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to wristFlexionCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of wristFlexionCheckBox
handles.taskcues.wristFlexion = get(hObject,'Value');
guidata(hObject, handles)

% --- Executes on button press in wristExtCheckBox.
function wristExtCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to wristExtCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of wristExtCheckBox
handles.taskcues.wristExtension = get(hObject,'Value');
guidata(hObject, handles)


% --- Executes on button press in pinchCheckBox.
function pinchCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to pinchCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pinchCheckBox
handles.taskcues.pinch = get(hObject,'Value');
guidata(hObject, handles)


% --- Executes on button press in powerCheckBox.
function powerCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to powerCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of powerCheckBox
handles.taskcues.power = get(hObject,'Value');
guidata(hObject, handles)

% --- Executes on button press in keyCheckBox.
function keyCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to keyCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of keyCheckBox
handles.taskcues.key = get(hObject,'Value');
guidata(hObject, handles)


% --- Executes on button press in openCheckBox.
function openCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to openCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of openCheckBox
handles.taskcues.open = get(hObject,'Value');
guidata(hObject, handles)

% --- Executes on button press in closeCheckBox.
function closeCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to closeCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of closeCheckBox
handles.taskcues.close = get(hObject,'Value');
guidata(hObject, handles)

% --- Executes on button press in wristPronationCheckbox.
function wristPronationCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to wristPronationCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of wristPronationCheckbox
handles.taskcues.wristPronation = get(hObject,'Value');
guidata(hObject, handles)

% --- Executes on button press in wristSupinationCheckbox.
function wristSupinationCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to wristSupinationCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of wristSupinationCheckbox
handles.taskcues.wristSupination = get(hObject,'Value');
guidata(hObject, handles)



function cueTime_Callback(hObject, eventdata, handles)
% hObject    handle to cueTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cueTime as text
%        str2double(get(hObject,'String')) returns contents of cueTime as a double
handles.tasksetup.cueTime = str2double(get(hObject,'String'));
guidata(hObject, handles)

% --- Executes during object creation, after setting all properties.
function cueTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cueTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in artifactRejection.
function artifactRejection_Callback(hObject, eventdata, handles)
% hObject    handle to artifactRejection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of artifactRejection
handles.blockInfo.artifactRejection = get(hObject, 'Value');
guidata(hObject, handles)

% --- Executes on selection change in artifactRejectionDropDown.
function artifactRejectionDropDown_Callback(hObject, eventdata, handles)
% hObject    handle to artifactRejectionDropDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns artifactRejectionDropDown contents as cell array
%        contents{get(hObject,'Value')} returns selected item from artifactRejectionDropDown
contents = cellstr(get(hObject,'String'));
handles.blockInfo.artifactRejectionSelected = contents{get(hObject,'Value')};
guidata(hObject, handles)

% --- Executes during object creation, after setting all properties.
function artifactRejectionDropDown_CreateFcn(hObject, eventdata, handles)
% hObject    handle to artifactRejectionDropDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ulnarDeviationCheckbox.
function ulnarDeviationCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to ulnarDeviationCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ulnarDeviationCheckbox
handles.taskcues.ulnarDeviation = get(hObject,'Value');
guidata(hObject, handles)



% --- Executes on button press in radialDeviationCheckbox.
function radialDeviationCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to radialDeviationCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radialDeviationCheckbox
handles.taskcues.radialDeviation = get(hObject,'Value');
guidata(hObject, handles)


% --- Executes when entered data in editable cell(s) in sessionTable.
function sessionTable_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to sessionTable (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
% data = get(hObject, 'data');

% fprintf('Table comments have been edited\n')
guidata(hObject, handles)

% --- Executes when entered data in editable cell(s) in filterTable.
function filterTable_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to filterTable (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
guidata(hObject, handles)


% --- Executes on button press in saveAsButton.
function saveAsButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveAsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%TODO: make this functional
[FileName,PathName]  = uiputfile('*.mat');
Summary = struct;
if isfield(handles,'blockInfo')
    Summary.blockInfo = handles.blockInfo;
end
if isfield(handles,'sessionInfo')
    Summary.sessionInfo = handles.sessionInfo;
end
if isfield(handles,'sessionTable')
    Summary.sessionTable = handles.sessionTable;
end
if isfield(handles,'filterTable')
    Summary.filterTable = handles.filterTable;
end
if isfield(handles, 'filterInfo')
    Summary.filterInfo = handles.filterInfo;
end

%save all data in the workspace
BaseWorkspace = evalin('base','whos');
BaseWorkspaceCell = struct2cell(BaseWorkspace);

BaseWorkspaceNames = {BaseWorkspaceCell{1,:}};
check1 = contains(BaseWorkspaceNames, 'Block');
check2 = contains(BaseWorkspaceNames, 'results');
subsetnames = {BaseWorkspaceNames{check1 & check2}};
availableblocks = cell2mat(cellfun(@str2double, (regexp(subsetnames,'\d*','Match')), 'un',0));
for jj=1:numel(availableblocks)
    blockname{1,jj} = strcat('Block_',num2str(availableblocks(jj)),'_results');
end
for block=1:numel(availableblocks)
    Data{block} = evalin('base',blockname{1,block});
end
Summary.Data = Data;

save([PathName, FileName], 'Summary')


% --- Executes on button press in LoadPushButton.
function LoadPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


[FileName,PathName] = uigetfile('*.mat');
loadedData = load([PathName, FileName]);
%is this better than replacing all handles with loadedData?
%(handles = loadedData)

%Currently only sets both table data
set(handles.sessionTable, 'Data', loadedData.Summary.sessionTable.Data)
set(handles.filterTable, 'Data', loadedData.Summary.filterTable.Data)

%Add filter info to handles struct
if isfield(handles, 'filterInfo')
    handles.filterInfo = loadedData.Summary.filterInfo;
end

if isfield(handles, 'sessionInfo')
    handles.sessionInfo = loadedData.Summary.sessionInfo;
end


guidata(hObject, handles);


% --- Executes on button press in LoadFiltersPushButton.
function LoadFiltersPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadFiltersPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName,PathName] = uigetfile('*.mat');
loadedData = load([PathName, FileName]);

%Only set filter table data
set(handles.filterTable, 'Data', loadedData.Summary.filterTable.Data)

%TODO: set decoder drop down upon loading filters
%         if isfield(handles.params, 'decoder')
%             validDecoders = ~cellfun(@isempty, handles.filterTable.Data(:,1));
%             decoderBuildList = cell(1,sum(validDecoders));
%             for dd = 1:sum(validDecoders)
%                 ddNum = sprintf('(%d)  ', dd);
%                 decoderBuildList(dd) = strcat(ddNum,{' '},handles.filterTable.Data(dd,1));
%             end
%             set(handles.decoderDropDown,'String',decoderBuildList);
% set(handles.decoderDropDown,'String',decoderBuilds);

%Add filter info to handles struct
if isfield(handles, 'filterInfo')
    handles.filterInfo = loadedData.Summary.filterInfo;
end


guidata(hObject, handles);


% --- Executes on button press in calibrateForceSensorsPushButton.
function calibrateForceSensorsPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to calibrateForceSensorsPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in calibrateIMUpushButton.
function calibrateIMUpushButton_Callback(hObject, eventdata, handles)
% hObject    handle to calibrateIMUpushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on button press in LoadFES.
function LoadFES_Callback(hObject, eventdata, handles)
% hObject    handle to LoadFES (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, path] = uigetfile('*.mat');
load([path, filename]);


% organize loaded outcomes into FES table
numOutcomes = size(outcomes,2);
if outcomes{1}.version == 1
    for m = 1:numOutcomes
        nmstimTable{m, 1} = outcomes{m}.annotation;
        %this was the stim for that "outcome",
        %is this safe to assume as the max stim amplitude? for now, yes
        nmstimTable{m, 2} = outcomes{m}.electrodes{1}.waveform_parameters.amp_mA_2;
        
        nmstimTable{m, 3} = 16;
        %         nmstimTable{m, 3} = outcomes{m}.electrodes{1}.eid;
        %         nmstimTable{m, 4} = outcomes{m}.electrodes{1}.rid;
        %       Stim channels should be between 10128 to 10255
        nmstimTable{m, 4} = outcomes{m}.electrodes{1}.channel - 10255; %that magic number :-)
        nmstimTable{m, 5} = outcomes{m}.electrodes{1}.return_channel - 10255;
    end
end

handles.FESinfo.movements = outcomes;
set(handles.fesTable, 'data', nmstimTable)

listofOutcomes = {nmstimTable{:, 1}};
set(handles.stimOptionsDropDown, 'String', listofOutcomes)

% display loaded outcomes name
set(handles.stimFileName,'String', filename)
handles.FESinfo.outcomesFile = filename;

guidata(hObject, handles)


% --- Executes on button press in NMstimButton.
function NMstimButton_Callback(hObject, eventdata, handles)
% hObject    handle to NMstimButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

stimTestOutcomesIndex = handles.FESinfo.SelectedFESTest;
stimTestMovement = handles.FESinfo.movements{:, stimTestOutcomesIndex};

%lets deactivate button press
set(handles.NMstimButton, 'enable','off');

%FES stim set up
FirstPhaseDelay_mS  = 0;   %the delay of the first phase
FirstPhaseWidth_mS  = 0.5; %the width of the first phase
SecondPhaseDelay_mS = 0; %the delay of the second phase
SecondPhaseWidth_mS = FirstPhaseWidth_mS; %the width of the second phase
Freq_hZ             = 50;  %the frequency of the stimulation
duration            = 2;

stimulationChannel = stimTestMovement.electrodes{1}.channel ;

%check if reciprocal used
if stimTestMovement.electrodes{1}.reciprocal
    reciprocalChannel  = stimTestMovement.electrodes{1}.return_channel;
    returnChannel      = -1;
    allStimChannels    = [stimulationChannel reciprocalChannel];
    stimAMP = stimTestMovement.electrodes{1}.waveform_parameters.amp_mA_2;
else
    returnChannel      = stimTestMovement.electrodes{1}.return_channel;
    allStimChannels    = [stimulationChannel];
end

%set stim params
if stimTestMovement.electrodes{1}.reciprocal
    [~]   = AO_SetStimulationParameters(FirstPhaseDelay_mS,   -stimAMP, FirstPhaseWidth_mS,...
        SecondPhaseDelay_mS,  stimAMP, SecondPhaseWidth_mS,...
        Freq_hZ, duration,  returnChannel, stimulationChannel);
    pause(.01)
    [~]   = AO_SetStimulationParameters(FirstPhaseDelay_mS,   stimAMP, FirstPhaseWidth_mS,...
        SecondPhaseDelay_mS,  -stimAMP, SecondPhaseWidth_mS,...
        Freq_hZ, duration,  returnChannel, reciprocalChannel);
    pause(.01)
    
    %stim!
    for jj = 1:length(allStimChannels)
        [~] = AO_StartStimulation(allStimChannels(jj));
    end
    
else
    
    [~]   = AO_SetStimulationParameters(FirstPhaseDelay_mS,   stimAMP, FirstPhaseWidth_mS,...
        SecondPhaseDelay_mS,  -stimAMP, SecondPhaseWidth_mS,...
        Freq_hZ, duration,  returnChannel, stimulationChannel);
    pause(.01)
    
    [~] = AO_StartStimulation(allStimChannels);
end

%lets reactivate button
set(handles.NMstimButton, 'enable','on');
guidata(hObject, handles)


% --- Executes on button press in fesTemplatePushButton.
function fesTemplatePushButton_Callback(hObject, eventdata, handles)
% hObject    handle to fesTemplatePushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

fesTable = get(handles.fesTable, 'data');
[row, col] = size(fesTable);
%movement labels in a cell matrix
for i = 1:row
    handles.FESinfo.movementLabels{i} = fesTable{i,1};
end

%for reference in Real time system
handles.FESinfo.template = fesTable;
guidata(hObject, handles)


% --- Executes on selection change in stimOptionsDropDown.
function stimOptionsDropDown_Callback(hObject, eventdata, handles)
% hObject    handle to stimOptionsDropDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns stimOptionsDropDown contents as cell array
%        contents{get(hObject,'Value')} returns selected item from stimOptionsDropDown

contents = cellstr(get(hObject,'String'));
handles.FESinfo.SelectedFESTest = get(hObject,'Value');
handles.FESinfo.SelectedFESTestLabel = contents{get(hObject,'Value')};
guidata(hObject, handles)


% --- Executes during object creation, after setting all properties.
function stimOptionsDropDown_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stimOptionsDropDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in stimMonitorCheckBox.
function stimMonitorCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to stimMonitorCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of stimMonitorCheckBox
handles.blockInfo.stimMonitor = get(hObject, 'Value');
guidata(hObject, handles)


% --------------------------------------------------------------------
function simulationMode_Callback(hObject, eventdata, handles)
% hObject    handle to simulationMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if strcmp(get(hObject,'Checked'),'on')
    %close mockcbmex
    if ~isempty(handles.functor)
        handles.functor('close')
    end
    
    handles.mockcbmex.open = false;
    set(hObject,'Checked','off');
    handles.functor = @cbmex;
else
    set(hObject,'Checked','on');
    handles.functor = @mockcbmex;
    
    if ~handles.mockcbmex.open
        handles.functor('open');
        handles.mockcbmex.open = true;
    else
        fprintf('mock cbmex already open\n')
    end
end
guidata(hObject, handles)


% --- Executes on button press in regressionCheckBox.
function regressionCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to regressionCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of regressionCheckBox
handles.tasksetup.regressionTask = get(hObject, 'Value');
guidata(hObject, handles)


function regressLevels_Callback(hObject, eventdata, handles)
% hObject    handle to regressLevels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of regressLevels as text
%        str2double(get(hObject,'String')) returns contents of regressLevels as a double

handles.tasksetup.regressLevels = str2num(get(hObject,'String')) ;
guidata(hObject, handles)


% --- Executes during object creation, after setting all properties.
function regressLevels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to regressLevels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% handles.taskcues.regressLevels = [0 0.25 0.5 0.75 1];
% set(hObject, 'String', num2str(handles.taskcues.regressLevels))
guidata(hObject, handles)



function prepostrestdur_Callback(hObject, eventdata, handles)
% hObject    handle to prepostrestdur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of prepostrestdur as text
%        str2double(get(hObject,'String')) returns contents of prepostrestdur as a double
handles.tasksetup.prepostrestdur = str2double(get(hObject,'String'));
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function prepostrestdur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prepostrestdur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function connectBend_Callback(hObject, eventdata, handles)
% hObject    handle to connectBend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Initialize Serial port for sampling bend sensors - Nikunj 05-03-2019
if (~handles.bendConnected)
    handles.serialBend = setupSerial('COM5'); % Teensy port
    if (handles.serialBend == -1)
        handles.bendConnected = false; 
        set(hObject,'Checked','off');
    else
        handles.bendConnected = true;
        %disply check next to menu item
        set(hObject,'Checked','on');
        disp(' ');
        disp('Connected to Bend Sensors'); 
    end
else
    % Close existing serial com port
    fclose(handles.serialBend);
    delete(handles.serialBend);
    handles.bendConnected = false;
    set(hObject,'Checked','off');
    warndlg('Disconnected Bend Sensors');    
end    
guidata(hObject, handles)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over stopButton.
function stopButton_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
