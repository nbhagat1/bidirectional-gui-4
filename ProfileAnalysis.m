% profile analysis - Nikunj

results = profile('info');

Total_time_vector = [];
for i = 1:44
    if (results.FunctionTable(i).NumCalls == 1742)
        Total_time_vector = [Total_time_vector, results.FunctionTable(i).TotalTime];
    end
end

%ignore the first reading because its for main_RealTimeLoop
%ignore the second reading because its cell.ismember , which is already accounted for in getPreprocessNeuralsignal 
%ignore the seventh reading because its daubcqf , which is already accounted for in getNeuralFeatures 
%...
% Better to manually calculate it, because there are a lot of children
% functions

latency = sum([40.859 + 44.76 + 4.677])/842

latency = sum([29.827 + 27 + 3.206])/572